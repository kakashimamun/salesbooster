-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `delegates`
--

DROP TABLE IF EXISTS `delegates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delegates` (
  `Client` bigint(20) NOT NULL,
  `delegates` varchar(255) NOT NULL,
  KEY `FK_lo0we3cubs1tr227ddo5af3r1` (`delegates`),
  KEY `FK_50fyaulhjkq6bouhe8shmcx2x` (`Client`),
  CONSTRAINT `FK_50fyaulhjkq6bouhe8shmcx2x` FOREIGN KEY (`Client`) REFERENCES `client` (`id`),
  CONSTRAINT `FK_lo0we3cubs1tr227ddo5af3r1` FOREIGN KEY (`delegates`) REFERENCES `user_account` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delegates`
--

LOCK TABLES `delegates` WRITE;
/*!40000 ALTER TABLE `delegates` DISABLE KEYS */;
INSERT INTO `delegates` VALUES (1,'SA1'),(1,'SA4'),(22,'SA3'),(22,'SA5'),(28,'SA5'),(28,'SA2'),(4,'SA4'),(4,'SA3'),(6,'SA2'),(6,'sa6'),(2,'SA3'),(2,'SA2'),(12,'SA4'),(12,'SA2'),(14,'SA5'),(14,'SA2'),(3,'SA3'),(3,'SA5'),(5,'SA1'),(5,'SA3'),(5,'SA5'),(9,'SA5'),(9,'SA2'),(10,'SA4'),(10,'SA5'),(15,'SA5'),(15,'SA2'),(20,'SA5'),(20,'SA2'),(13,'SA3'),(13,'SA5'),(13,'sa6'),(16,'SA4'),(16,'SA2'),(25,'SA5'),(25,'SA2'),(26,'SA3'),(26,'SA5'),(45,'SA4'),(45,'SA3'),(46,'SA3'),(46,'SA2');
/*!40000 ALTER TABLE `delegates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:02
