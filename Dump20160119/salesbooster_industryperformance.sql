-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `industryperformance`
--

DROP TABLE IF EXISTS `industryperformance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `industryperformance` (
  `industry` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `noofdeals` bigint(20) NOT NULL,
  `amount` bigint(20) NOT NULL,
  PRIMARY KEY (`industry`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `industryperformance`
--

LOCK TABLES `industryperformance` WRITE;
/*!40000 ALTER TABLE `industryperformance` DISABLE KEYS */;
INSERT INTO `industryperformance` VALUES ('bank','SA1',10,994247),('bank','SA2',4,309207),('bank','SA3',10,668612),('bank','SA4',5,585948),('bank','SA5',6,502335),('bar','SA1',9,101387),('bar','SA2',6,651006),('bar','SA3',1,583943),('bar','SA4',6,930407),('bar','SA5',5,851586),('church','SA1',1,709775),('church','SA2',5,211075),('church','SA3',3,110552),('church','SA4',4,378828),('church','SA5',9,706225),('hospital','SA1',10,318599),('hospital','SA2',8,394940),('hospital','SA3',1,231151),('hospital','SA4',7,599620),('hospital','SA5',7,504710),('lodging','SA1',3,901533),('lodging','SA2',6,497011),('lodging','SA3',6,225929),('lodging','SA4',10,35231),('lodging','SA5',4,879717),('museum','SA1',8,393327),('museum','SA2',3,415511),('museum','SA3',4,783281),('museum','SA4',5,354016),('museum','SA5',9,594487),('museum','sa6',1,0),('park','SA1',2,942506),('park','SA2',3,679627),('park','SA3',10,396953),('park','SA4',5,319738),('park','SA5',9,534860),('real_estate_agency','SA1',7,248996),('real_estate_agency','SA2',1,403193),('real_estate_agency','SA3',7,518073),('real_estate_agency','SA4',3,748883),('real_estate_agency','SA5',5,268876),('restaurant','SA1',11,530191),('restaurant','SA2',4,845681),('restaurant','SA3',8,391876),('restaurant','SA4',1,122267),('restaurant','SA5',5,340321),('restaurant','sa6',1,0),('school','SA1',9,371024),('school','SA2',10,403976),('school','SA3',8,356973),('school','SA4',5,538871),('school','SA5',12,793944),('shopping_mall','SA1',3,304736),('shopping_mall','SA2',9,439270),('shopping_mall','SA3',1,194001),('shopping_mall','SA4',7,721645),('shopping_mall','SA5',3,168624);
/*!40000 ALTER TABLE `industryperformance` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:03
