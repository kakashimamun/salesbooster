-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `opportunity`
--

DROP TABLE IF EXISTS `opportunity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `area` float NOT NULL,
  `cost` float NOT NULL,
  `perdaycost` float NOT NULL,
  `required` bit(1) NOT NULL,
  `day` bit(1) NOT NULL,
  `night` bit(1) NOT NULL,
  `timesinshift` int(11) NOT NULL,
  `friday` bit(1) NOT NULL,
  `monday` bit(1) NOT NULL,
  `saturday` bit(1) NOT NULL,
  `sunday` bit(1) NOT NULL,
  `thursday` bit(1) NOT NULL,
  `tuesday` bit(1) NOT NULL,
  `wednesday` bit(1) NOT NULL,
  `product` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8maopjl5d5j7js8k1knly8ark` (`product`),
  CONSTRAINT `FK_8maopjl5d5j7js8k1knly8ark` FOREIGN KEY (`product`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunity`
--

LOCK TABLES `opportunity` WRITE;
/*!40000 ALTER TABLE `opportunity` DISABLE KEYS */;
INSERT INTO `opportunity` VALUES (1,'2016-01-19 09:39:43',NULL,0,200,384,24,'','','\0',1,'\0','\0','\0','','\0','\0','\0',6),(2,'2016-01-19 09:39:43',NULL,0,100,1664,52,'','','\0',1,'\0','\0','','','\0','\0','\0',4),(3,'2016-01-19 09:42:45',NULL,0,200,11340,135,'','','',1,'','','','','','','',12),(4,'2016-01-19 09:42:45','2016-01-19 09:42:55',0,20,4216.8,50.2,'\0','','\0',1,'','','','','','','',13),(5,'2016-01-19 09:45:05',NULL,0,100,624,52,'\0','','\0',1,'\0','\0','\0','','\0','\0','\0',4),(6,'2016-01-19 09:48:26',NULL,0,25,410,51.25,'','','\0',1,'\0','\0','\0','','\0','\0','\0',14),(7,'2016-01-19 09:48:26',NULL,0,100,1320,55,'\0','','\0',1,'\0','\0','','','\0','\0','',12),(8,'2016-01-19 09:50:13',NULL,0,100,176,22,'\0','','\0',1,'\0','\0','\0','','\0','\0','\0',6),(9,'2016-01-19 09:50:13',NULL,0,25,1800,45,'','','\0',1,'','','\0','\0','','','',7),(10,'2016-01-19 09:53:17',NULL,0,100,1248,52,'\0','','\0',1,'\0','','\0','\0','','\0','',3),(11,'2016-01-19 09:53:59',NULL,0,200,192,24,'','','\0',1,'\0','\0','\0','','\0','\0','\0',6),(13,'2016-01-19 10:46:40',NULL,0,50,2626,50.5,'','','\0',1,'\0','','\0','','\0','\0','\0',13),(14,'2016-01-19 10:49:58',NULL,0,200,624,24,'','','\0',1,'\0','\0','\0','','\0','\0','\0',6),(15,'2016-01-19 10:49:58',NULL,0,100,2704,52,'','','\0',1,'\0','\0','','','\0','\0','\0',4);
/*!40000 ALTER TABLE `opportunity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:03
