-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `fixed` decimal(10,2) DEFAULT '0.00',
  `surface` varchar(255) NOT NULL,
  `tags` varchar(1023) DEFAULT NULL,
  `task` varchar(255) NOT NULL,
  `var` decimal(10,2) DEFAULT '0.00',
  `relatedservice` varchar(255) DEFAULT NULL,
  `priority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_6xg0s3sdrxhmbjo3k1ybo7852` (`surface`,`task`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (3,NULL,'2016-01-15 09:40:05',0,50.00,'Floor:Mosaic','funeral_home,furniture_store,gas_station,embassy,electronics_store,courthouse,hospital,hindu_temple,hotel,gym,library,laundry,health,food,book_store,bar,bakery,art_gallery,atm,bank,beauty_salon,car_rental,campground,bus_station,bicycle_store,clothing_store,church,casino,car_repair,cafe,amusement_park,bowling_alley,liquor_store,jewelry_store,hardware_store,grocery_or_supermarket,doctor,dentist,cemetery,car_dealer,car_wash,airport,city_hall,convenience_store,accounting,finance,department_store,movie_theater,meal_takeaway,lodging,insurance_agency,hair_care,fire_station,police,physiotherapist,pet_store,painter,night_club,movie_rental,locksmith,local_government_office,moving_company,mosque,museum,meal_delivery,office,park,plumber,post_office,restaurant,real_estate_agency','Wipe',0.02,NULL,'LOW'),(4,NULL,'2016-01-15 09:39:52',0,50.00,'Floor:Carpet','bank,art_gallery,bakery,university,zoo,taxi_stand,store,shopping_mall,school,spa,stadium,office,mosque,lodging,library,hospital,casino','Vacuum',0.02,NULL,'MEDIUM'),(5,NULL,'2016-01-15 09:41:09',0,50.00,'Table:Glass','funeral_home,fire_station,food,furniture_store,finance,embassy,electronics_store,department_store,courthouse,health,hair_care,grocery_or_supermarket,doctor,dentist,clothing_store,laundry,insurance_agency,hardware_store,gym,convenience_store,movie_theater,meal_takeaway,locksmith,jewelry_store,gas_station,casino,mosque,local_government_office,hindu_temple,cemetery,car_dealer,bowling_alley','Wipe',0.04,NULL,'LOW'),(6,NULL,'2016-01-15 09:39:35',0,20.00,'Floor:Carpet','amusement_park,art_gallery,bank,bar,casino,department_store,convenience_store,food,gym,hotel,hospital,laundry,lodging,mosque,office,museum,school,spa,shopping_mall,zoo,university,store,stadium,restaurant','Remove Spot',0.02,NULL,'HIGH'),(7,NULL,'2016-01-14 13:05:25',0,40.00,'Restroom','amusement_park,airport,bank,bakery,art_gallery,beauty_salon,bicycle_store,book_store,bowling_alley,bus_station,cafe,car_dealer,campground,car_rental,liquor_store,library,laundry,insurance_agency,hospital,health,hotel,meal_takeaway,lodging,local_government_office,hardware_store,gas_station,furniture_store,food,fire_station,finance,night_club,moving_company,mosque,jewelry_store','Maintanance',0.20,NULL,'MEDIUM'),(8,NULL,'2016-01-15 09:40:25',0,50.00,'Kitchen','airport,art_gallery,bakery,bank,atm,bar,bicycle_store,bowling_alley,beauty_salon,amusement_park,bus_station,cafe,car_dealer,car_rental,car_repair,campground,book_store,car_wash,church,clothing_store,city_hall,convenience_store','Maintanance',0.03,NULL,'MEDIUM'),(9,NULL,'2016-01-15 09:40:17',0,50.00,'Furniture','amusement_park,art_gallery,bakery,bank,bar,bicycle_store,police,post_office,real_estate_agency,plumber,pharmacy,parking,painter,office,museum,movie_theater,mosque,meal_delivery,locksmith,liquor_store,jewelry_store,hindu_temple,hair_care,gas_station,fire_station,doctor,convenience_store,university,travel_agency,taxi_stand,subway_station','Wipe',0.03,NULL,'MEDIUM'),(11,NULL,'2016-01-15 09:40:40',0,50.00,'Parking','amusement_park,atm,bakery,bank,gas_station,grocery_or_supermarket,furniture_store,food,finance,physiotherapist,pharmacy,parking,painter,office,museum,stadium,spa,shoe_store,restaurant,post_office,pet_store,moving_company','Clean',0.02,NULL,'LOW'),(12,NULL,'2016-01-15 09:41:17',0,50.00,'Window','airport,amusement_park,atm,bakery,art_gallery,bank,beauty_salon,bar,bicycle_store,book_store,bowling_alley,cafe,campground,bus_station,car_dealer,accounting,car_wash,casino,cemetery,church,city_hall,clothing_store,convenience_store,physiotherapist,pet_store,painter,zoo,courthouse,car_repair','Wipe',0.05,NULL,'LOW'),(13,NULL,'2016-01-15 09:59:04',0,50.00,'Restroom','art_gallery,amusement_park,atm,bank,bakery,bar,beauty_salon,bicycle_store,bowling_alley,book_store,cafe,campground,bus_station,car_dealer,car_rental,car_repair,casino,car_wash,cemetery,church,airport,clothing_store,convenience_store,city_hall,courthouse,accounting,dentist,hair_care,gas_station,funeral_home,finance,doctor,electronics_store,insurance_agency,hindu_temple,hardware_store,gym,food,embassy','Clean',0.01,NULL,'HIGH'),(14,'2016-01-15 09:43:49',NULL,0,50.00,'IT Instruments','school,office,university,accounting,bank,casino,embassy,finance,hotel,insurance_agency,local_government_office,lodging','Sanitize',0.05,NULL,'MEDIUM');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:01
