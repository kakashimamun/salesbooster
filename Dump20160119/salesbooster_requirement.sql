-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `requirement`
--

DROP TABLE IF EXISTS `requirement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requirement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `area` float NOT NULL,
  `required` bit(1) NOT NULL,
  `day` bit(1) NOT NULL,
  `night` bit(1) NOT NULL,
  `timesinshift` int(11) NOT NULL,
  `friday` bit(1) NOT NULL,
  `monday` bit(1) NOT NULL,
  `saturday` bit(1) NOT NULL,
  `sunday` bit(1) NOT NULL,
  `thursday` bit(1) NOT NULL,
  `tuesday` bit(1) NOT NULL,
  `wednesday` bit(1) NOT NULL,
  `client` bigint(20) NOT NULL,
  `product` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hbh3307wq6r9658yqbj8e4pfj` (`client`),
  KEY `FK_fy6yglcjn8h5mytqgobn26o9f` (`product`),
  CONSTRAINT `FK_fy6yglcjn8h5mytqgobn26o9f` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_hbh3307wq6r9658yqbj8e4pfj` FOREIGN KEY (`client`) REFERENCES `client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requirement`
--

LOCK TABLES `requirement` WRITE;
/*!40000 ALTER TABLE `requirement` DISABLE KEYS */;
INSERT INTO `requirement` VALUES (1,'2016-01-19 09:38:52',NULL,0,200,'','','\0',1,'\0','\0','\0','','\0','\0','\0',1,6),(2,'2016-01-19 09:39:05',NULL,0,100,'','','\0',1,'\0','\0','','','\0','\0','\0',1,4),(3,'2016-01-19 09:42:12',NULL,0,200,'','','',1,'','','','','','','',12,12),(4,'2016-01-19 09:42:37',NULL,0,20,'\0','','\0',1,'','','','','','','',12,13),(5,'2016-01-19 09:44:57',NULL,0,100,'\0','','\0',1,'\0','\0','\0','','\0','\0','\0',22,4),(6,'2016-01-19 09:46:00',NULL,0,100,'\0','','\0',1,'\0','\0','\0','','\0','\0','\0',26,6),(8,'2016-01-19 09:48:02',NULL,0,25,'','','\0',1,'\0','\0','\0','','\0','\0','\0',4,14),(9,'2016-01-19 09:48:21',NULL,0,100,'\0','','\0',1,'\0','\0','','','\0','\0','',4,12),(10,'2016-01-19 09:49:49',NULL,0,100,'\0','','\0',1,'\0','\0','\0','','\0','\0','\0',10,6),(11,'2016-01-19 09:50:08',NULL,0,25,'','','\0',1,'','','\0','\0','','','',10,7),(12,'2016-01-19 09:51:56',NULL,0,100,'','\0','',1,'\0','','\0','\0','\0','\0','\0',3,6),(13,'2016-01-19 09:53:13',NULL,0,100,'\0','','\0',1,'\0','','\0','\0','','\0','',2,3),(14,'2016-01-19 09:53:55',NULL,0,200,'','','\0',1,'\0','\0','\0','','\0','\0','\0',9,6),(15,'2016-01-19 10:44:59',NULL,0,50,'','','\0',1,'\0','','\0','','\0','\0','\0',45,13);
/*!40000 ALTER TABLE `requirement` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:02
