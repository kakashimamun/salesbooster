-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: salesbooster
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `version` bigint(20) NOT NULL,
  `clientid` bigint(20) NOT NULL,
  `clientstatus` varchar(255) DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `missed` bit(1) NOT NULL,
  `srepresentative` varchar(255) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `teammeeting` bit(1) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (1,'2016-01-19 09:30:15','2016-01-19 09:39:08',0,1,'PROSPECT','2016-01-19 17:30:15','\0','SA5','2016-01-19 13:30:15','COMPLETED','\0','Collect Requirments of Poison Ivy Bistro','MEET'),(2,'2016-01-19 09:30:25','2016-01-19 09:44:31',0,22,'PROSPECT','2016-01-19 17:30:25','\0','SA1','2016-01-19 13:30:25','COMPLETED','\0','Collect Requirments of Pei Hwa Secondary School','MEET'),(3,'2016-01-19 09:30:35','2016-01-19 09:52:18',0,28,'PROSPECT','2016-01-20 22:00:00','\0','SA4','2016-01-20 18:00:00','DUE','\0','Collect Requirments of Nanyang Technological University','MEET'),(4,'2016-01-19 09:30:43','2016-01-19 09:47:26',0,4,'PROSPECT','2016-01-20 17:30:00','\0','SA2','2016-01-20 13:30:00','COMPLETED','\0','Collect Requirments of Pasir Ris Crest Secondary School','MEET'),(5,'2016-01-19 09:30:56','2016-01-19 09:34:58',0,6,'PROSPECT','2016-01-20 17:30:00','\0','SA3','2016-01-20 13:30:00','DUE','\0','Collect Requirments of Dunman Secondary School','MEET'),(6,'2016-01-19 09:31:05','2016-01-19 09:52:54',0,2,'PROSPECT','2016-01-21 17:31:00','\0','sa6','2016-01-21 13:31:00','COMPLETED','\0','Collect Requirments of Bollywood Veggies','MEET'),(7,'2016-01-19 09:31:15','2016-01-19 09:42:41',0,12,'PROSPECT','2016-01-21 17:31:00','\0','SA5','2016-01-21 13:31:00','COMPLETED','\0','Collect Requirments of Parkway East Hospital','MEET'),(8,'2016-01-19 09:31:23','2016-01-19 09:34:48',0,14,'PROSPECT','2016-01-29 17:31:00','\0','SA1','2016-01-29 13:31:00','DUE','\0','Collect Requirments of The Fragrance Hotel','MEET'),(9,'2016-01-19 09:31:31','2016-01-19 09:51:30',0,3,'PROSPECT','2016-01-19 17:31:31','\0','SA4','2016-01-19 13:31:31','COMPLETED','\0','Collect Requirments of D\'Kranji Farm Resort','MEET'),(10,'2016-01-19 09:31:38','2016-01-19 09:34:35',0,5,'PROSPECT','2016-01-20 17:31:00','\0','SA2','2016-01-20 13:31:00','DUE','\0','Collect Requirments of Ngee Ann Secondary School','MEET'),(11,'2016-01-19 09:31:47','2016-01-19 09:53:29',0,9,'PROSPECT','2016-01-22 17:31:00','\0','sa6','2016-01-22 13:31:00','COMPLETED','\0','Collect Requirments of The Changi Museum','MEET'),(12,'2016-01-19 09:31:54','2016-01-19 09:49:23',0,10,'PROSPECT','2016-01-19 17:31:54','\0','SA3','2016-01-19 13:31:54','COMPLETED','\0','Collect Requirments of Loyang Primary School','MEET'),(13,'2016-01-19 09:32:04','2016-01-19 09:34:51',0,15,'PROSPECT','2016-01-23 17:32:00','\0','sa6','2016-01-23 13:32:00','DUE','\0','Collect Requirments of Santa Grand Hotel East Coast','MEET'),(14,'2016-01-19 09:32:15','2016-01-19 09:34:50',0,20,'PROSPECT','2016-01-28 17:32:00','\0','SA3','2016-01-28 13:32:00','DUE','\0','Collect Requirments of Spizza','MEET'),(15,'2016-01-19 09:32:28','2016-01-19 09:46:25',0,13,'PROSPECT','2016-01-19 17:32:00','\0','SA2','2016-01-19 13:32:00','COMPLETED','\0','Collect Requirments of Venue Hotel The Lily','MEET'),(16,'2016-01-19 09:33:47','2016-01-19 11:00:00',0,16,'PROSPECT','2016-01-13 17:33:00','','SA5','2016-01-13 13:33:00','DUE','\0','Collect Requirments of Hotel 81 Tristar','MEET'),(17,'2016-01-19 09:33:56','2016-01-19 09:34:54',0,25,'PROSPECT','2016-01-27 17:33:00','\0','SA4','2016-01-27 13:33:00','DUE','\0','Collect Requirments of Jurong West Primary School','MEET'),(18,'2016-01-19 09:34:03','2016-01-19 09:45:32',0,26,'PROSPECT','2016-01-19 17:34:03','\0','SA1','2016-01-19 13:34:03','COMPLETED','\0','Collect Requirments of S\'pore Discovery Centre','MEET'),(19,'2016-01-19 09:39:08','2016-01-19 09:40:28',0,1,'PROFILED','2016-01-20 17:39:00','\0','SA5','2016-01-20 13:39:00','COMPLETED','\0','Propose cleaning porposal to Poison Ivy Bistro','PROPOSE'),(20,'2016-01-19 09:40:28','2016-01-19 09:40:35',0,1,'PROFILED','2016-01-19 17:40:28','\0','SA5','2016-01-19 13:40:28','COMPLETED','\0','Negotiate the price with Poison Ivy Bistro','NEGOTIATE'),(21,'2016-01-19 09:40:36',NULL,0,1,'ACTIVE','2016-04-12 13:40:36','\0','SA5','2016-05-10 09:40:36','DUE','\0','Service is Expiring, Follow Up and Offer Another Proposal to Poison Ivy Bistro','FOLLOWUP'),(22,'2016-01-19 09:42:41','2016-01-19 09:43:35',0,12,'PROFILED','2016-01-19 17:42:41','\0','SA5','2016-01-19 13:42:41','COMPLETED','\0','Propose cleaning porposal to Parkway East Hospital','PROPOSE'),(23,'2016-01-19 09:43:35','2016-01-19 09:43:42',0,12,'PROFILED','2016-01-19 17:43:35','\0','SA5','2016-01-19 13:43:35','COMPLETED','\0','Negotiate the price with Parkway East Hospital','NEGOTIATE'),(24,'2016-01-19 09:43:42','2016-01-19 09:43:53',0,12,'PROFILED','2016-01-19 17:43:42','\0','SA5','2016-01-19 13:43:42','COMPLETED','\0','Follow Up and Offer another proposal to Parkway East Hospital','FOLLOWUP'),(25,'2016-01-19 09:43:53',NULL,0,12,'PROPOSED','2016-01-19 17:43:53','\0','SA5','2016-01-19 13:43:53','DUE','\0','Negotiate the price with Parkway East Hospital','CLOSE'),(26,'2016-01-19 09:44:31','2016-01-19 09:45:01',0,22,'PROFILED','2016-01-19 17:44:31','\0','SA1','2016-01-19 13:44:31','COMPLETED','\0','Propose cleaning porposal to Pei Hwa Secondary School','PROPOSE'),(27,'2016-01-19 09:45:01','2016-01-19 10:06:10',0,22,'PROPOSED','2016-01-19 17:45:00','\0','SA1','2016-01-19 13:45:00','COMPLETED','\0','Negotiate the price with Pei Hwa Secondary School','CLOSE'),(28,'2016-01-19 09:45:08','2016-01-19 09:45:21',0,22,'PROPOSED','2016-01-19 17:45:08','\0','SA1','2016-01-19 13:45:08','COMPLETED','\0','Negotiate the price with Pei Hwa Secondary School','NEGOTIATE'),(29,'2016-01-19 09:45:21',NULL,0,22,'ACTIVE','2016-03-15 13:45:21','\0','SA1','2016-04-12 09:45:21','DUE','\0','Service is Expiring, Follow Up and Offer Another Proposal to Pei Hwa Secondary School','FOLLOWUP'),(30,'2016-01-19 09:45:32',NULL,0,26,'PROFILED','2016-01-19 17:45:32','\0','SA1','2016-01-19 13:45:32','DUE','\0','Propose cleaning porposal to S\'pore Discovery Centre','PROPOSE'),(31,'2016-01-19 09:46:25',NULL,0,13,'PROFILED','2016-01-19 17:46:25','\0','SA2','2016-01-19 13:46:25','DUE','\0','Propose cleaning porposal to Venue Hotel The Lily','PROPOSE'),(32,'2016-01-19 09:47:26','2016-01-19 09:52:14',0,4,'PROPOSED','2016-01-21 17:47:00','\0','SA2','2016-01-21 13:47:00','DUE','\0','Negotiate the price with Pasir Ris Crest Secondary School','CLOSE'),(33,'2016-01-19 09:49:02','2016-01-19 09:49:05',0,4,'PROPOSED','2016-01-19 17:49:02','\0','SA2','2016-01-19 13:49:02','COMPLETED','\0','Negotiate the price with Pasir Ris Crest Secondary School','NEGOTIATE'),(34,'2016-01-19 09:49:05',NULL,0,4,'ACTIVE','2016-02-16 13:49:05','\0','SA2','2016-03-15 09:49:05','DUE','\0','Service is Expiring, Follow Up and Offer Another Proposal to Pasir Ris Crest Secondary School','FOLLOWUP'),(35,'2016-01-19 09:49:23','2016-01-19 09:52:16',0,10,'PROPOSED','2016-01-20 17:49:00','\0','SA3','2016-01-20 13:49:00','DUE','\0','Negotiate the price with Loyang Primary School','CLOSE'),(36,'2016-01-19 09:51:09','2016-01-19 09:52:17',0,10,'PROPOSED','2016-01-21 17:51:00','\0','SA3','2016-01-21 13:51:00','DUE','\0','Negotiate the price with Loyang Primary School','NEGOTIATE'),(37,'2016-01-19 09:51:30',NULL,0,3,'PROFILED','2016-01-19 17:51:30','\0','SA4','2016-01-19 13:51:30','DUE','\0','Propose cleaning porposal to D\'Kranji Farm Resort','PROPOSE'),(38,'2016-01-19 09:52:54',NULL,0,2,'PROPOSED','2016-01-19 17:52:54','\0','sa6','2016-01-19 13:52:54','DUE','\0','Negotiate the price with Bollywood Veggies','CLOSE'),(39,'2016-01-19 09:53:20','2016-01-19 09:53:22',0,2,'PROPOSED','2016-01-19 17:53:20','\0','sa6','2016-01-19 13:53:20','COMPLETED','\0','Negotiate the price with Bollywood Veggies','NEGOTIATE'),(40,'2016-01-19 09:53:22',NULL,0,2,'ACTIVE','2016-02-16 13:53:22','\0','sa6','2016-03-15 09:53:22','DUE','\0','Service is Expiring, Follow Up and Offer Another Proposal to Bollywood Veggies','FOLLOWUP'),(41,'2016-01-19 09:53:29',NULL,0,9,'PROPOSED','2016-01-19 17:53:29','\0','sa6','2016-01-19 13:53:29','DUE','\0','Negotiate the price with The Changi Museum','CLOSE'),(42,'2016-01-19 09:54:15','2016-01-19 09:54:21',0,9,'PROPOSED','2016-01-19 17:54:15','\0','sa6','2016-01-19 13:54:15','COMPLETED','\0','Negotiate the price with The Changi Museum','NEGOTIATE'),(43,'2016-01-19 09:54:21',NULL,0,9,'ACTIVE','2016-02-16 13:54:21','\0','sa6','2016-03-15 09:54:21','DUE','\0','Service is Expiring, Follow Up and Offer Another Proposal to The Changi Museum','FOLLOWUP'),(44,'2016-01-19 10:15:25',NULL,0,0,NULL,'2016-01-22 11:30:00','\0','Manager','2016-01-22 07:30:00','DUE','','Team meeting on calling','GENERAL'),(45,'2016-01-19 10:41:56','2016-01-19 10:46:15',0,45,'PROSPECT','2016-01-19 18:41:56','\0','SA5','2016-01-19 14:41:56','COMPLETED','\0','Collect Requirments of Mac\'s Cafe and bar','MEET'),(46,'2016-01-19 10:42:53',NULL,0,46,'PROSPECT','2016-01-19 18:42:53','\0','SA1','2016-01-19 14:42:53','DUE','\0','Collect Requirments of Pietrasanta The Italian Restaurant','MEET'),(47,'2016-01-19 10:46:15','2016-01-19 10:47:40',0,45,'PROFILED','2016-01-19 18:46:15','\0','SA5','2016-01-19 14:46:15','COMPLETED','\0','Propose cleaning porposal to Mac\'s Cafe and bar','PROPOSE'),(48,'2016-01-19 10:47:40','2016-01-19 10:47:49',0,45,'PROFILED','2016-01-19 18:47:40','\0','SA5','2016-01-19 14:47:40','COMPLETED','\0','Negotiate the price with Mac\'s Cafe and bar','NEGOTIATE'),(49,'2016-01-19 10:47:49',NULL,0,45,'ACTIVE','2016-06-21 14:47:49','\0','SA5','2016-07-19 10:47:49','DUE','\0','Service is Expiring, Follow Up and Offer Another Proposal to Mac\'s Cafe and bar','FOLLOWUP'),(50,'2016-01-19 10:50:42',NULL,0,0,NULL,'2016-01-26 16:00:00','\0','Manager','2016-01-26 10:30:00','DUE','','team meeting on negotiation ','GENERAL');
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-19 11:53:03
