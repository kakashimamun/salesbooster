package com.worksap.stm.sb;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@PropertySource("classpath:mail.properties")
public class MailConfiguration {

    @Value("${mail.protocol}")
    private String protocol;
    @Value("${mail.host}")
    private String host;
    @Value("${mail.port}")
    private int port;
    @Value("${mail.smtp.auth}")
    private boolean auth;
    @Value("${mail.smtp.starttls.enable}")
    private boolean starttls;
    @Value("${mail.debug}")
    private boolean debug;

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        
        Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", auth);
        mailProperties.put("mail.smtp.starttls.enable", starttls);
        mailProperties.put("mail.debug", debug);
        mailSender.setJavaMailProperties(mailProperties);
        
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setProtocol(protocol);
        
        return mailSender;
    }
    
//    @Bean
//    public JavaMailSender mailSender() throws IOException {
//        Properties properties = configProperties();
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost(properties.getProperty("mail.server.host"));
//        mailSender.setPort(Integer.parseInt(properties.getProperty("mail.server.port")));
//        mailSender.setProtocol(properties.getProperty("mail.server.protocol"));
//        mailSender.setUsername(properties.getProperty("mail.server.username"));
//        mailSender.setPassword(properties.getProperty("mail.server.password"));
//        mailSender.setJavaMailProperties(javaMailProperties());
//        return mailSender;
//    }
//     
//    private Properties configProperties() throws IOException {
//        Properties properties = new Properties();
//        properties.load(new ClassPathResource("configuration.properties").getInputStream());
//        return properties;
//    }
//     
//    private Properties javaMailProperties() throws IOException {
//        Properties properties = new Properties();
//        properties.load(new ClassPathResource("javamail.properties").getInputStream());
//        return properties;
//    }
}