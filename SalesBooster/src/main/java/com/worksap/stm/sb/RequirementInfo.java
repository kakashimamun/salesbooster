package com.worksap.stm.sb;

import com.worksap.stm.sb.domain.BaseEntity;
import com.worksap.stm.sb.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by mamun on 14/1/2016.
 */


@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@Data
@Entity
public class RequirementInfo extends BaseEntity {


    private long clientid;

    private long weeks;


    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private LocalDate start;

}
