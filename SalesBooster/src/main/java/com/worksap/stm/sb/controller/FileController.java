package com.worksap.stm.sb.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import lombok.AllArgsConstructor;
import lombok.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.worksap.stm.sb.domain.File;
import com.worksap.stm.sb.service.FileService;
import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class FileController
{

	@Autowired
	private FileService fileService;

	@RequestMapping(value =
	{ "/fileuploader" })
	public String welcome(HttpServletRequest request, Model model,
			HttpSession session)
	{

		return SbLibrary.getPageFragment(request, "fileuploader");
	}

	@AllArgsConstructor
	@Data
	class CkEditorImageResponse
	{

		private int uploaded;
		private String fileName;
		private String url;
		private String error;

		public void setError(String msg)
		{
			this.error = "'error': {" + "'message':" + msg + "}";
		}

	}

	@RequestMapping(value = "/ckeditorUplaod", method = RequestMethod.POST)
	public @ResponseBody CkEditorImageResponse singleSave(
			@RequestParam("upload") MultipartFile file,
			HttpServletRequest request)
	{

		File fileEntity = null;

		if (!file.isEmpty())
		{
			try
			{

				fileEntity = fileService.saveFileToFolder(file, request);

			} catch (Exception e)
			{

				// TODO Auto-generated catch block
				e.printStackTrace();

				return new CkEditorImageResponse(0, "", "",
						"Failed to save in folder");
			}

			try
			{
				fileService.saveFileToDB(fileEntity);
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();

				return new CkEditorImageResponse(0, "", "",
						"Unable to save in database.");
			}

			return new CkEditorImageResponse(1, fileEntity.getName(),
					fileService.getLink(fileEntity, request), "");

		} else
		{

			return new CkEditorImageResponse(0, "", "",
					"Unable to upload. File is empty.");
		}

	}

	@RequestMapping(value = "/singleSave", method = RequestMethod.POST)
	public @ResponseBody String ckeditorUplaod(
			@RequestParam("file") MultipartFile file,
			@RequestParam("desc") String desc, HttpServletRequest request)
	{

		File fileEntity = null;

		if (!file.isEmpty())
		{
			try
			{

				fileEntity = fileService.saveFileToFolder(file, request);

			} catch (Exception e)
			{

				// TODO Auto-generated catch block
				e.printStackTrace();
				return "Failed to save file in the folder";
			}

			try
			{
				fileService.saveFileToDB(fileEntity);
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();

				return "Failed to save in DB";
			}

			return fileService.getLink(fileEntity, request);

		} else
		{
			return "Unable to upload. File is empty.";
		}

	}

	// @RequestMapping(value="/multipleUpload")
	// public String multiUpload(){
	// return "multipleUpload";
	// }
	//

	// @RequestMapping(value="/multipleSave", method=RequestMethod.POST )
	// public @ResponseBody String multipleSave(@RequestParam("file")
	// MultipartFile[] files){
	// String fileName = null;
	// String msg = "";
	// if (files != null && files.length >0) {
	// for(int i =0 ;i< files.length; i++){
	// try {
	// fileName = files[i].getOriginalFilename();
	// byte[] bytes = files[i].getBytes();
	// BufferedOutputStream buffStream =
	// new BufferedOutputStream(new FileOutputStream(new File("F:/cp/" +
	// fileName)));
	// buffStream.write(bytes);
	// buffStream.close();
	// msg += "You have successfully uploaded " + fileName +"<br/>";
	// } catch (Exception e) {
	// return "You failed to upload " + fileName + ": " + e.getMessage()
	// +"<br/>";
	// }
	// }
	// return msg;
	// } else {
	// return "Unable to upload. File is empty.";
	// }
	// }
}