package com.worksap.stm.sb.controller;



import javax.servlet.http.HttpServletRequest;

import com.worksap.stm.sb.domain.SalesRecord;
import com.worksap.stm.sb.service.NotificationService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import com.worksap.stm.sb.domain.ProductConfig;
import com.worksap.stm.sb.service.ProductConfigService;
import com.worksap.stm.sb.service.SalesRecordService;
import com.worksap.stm.sb.utils.SbLibrary;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Controller
public class ProductConfigController
{
	
	@Autowired
	ProductConfigService productConfigService;
	
	@Autowired
	SalesRecordService salesTargetService;

	@Autowired
	NotificationService notificationService;

	@RequestMapping(value = "/serviceSettings", method = RequestMethod.GET)
	public String serviceSettingsGet(HttpServletRequest request, Model model)
	{
		
 		Iterable<ProductConfig> productConfigs = productConfigService.getAllConfig();
 		
		
		model.addAttribute("configs",productConfigs);
		
		return SbLibrary.getPageFragment(request, "service-setting");
	}


	
	@RequestMapping(value = "/salesTarget", method = RequestMethod.GET)
	public String salesTarget(HttpServletRequest request, Model model)
	{
		JSONArray morrisChartMonthObj = salesTargetService.getMorrisMonthData();


		model.addAttribute("morrisChartMonthObj",morrisChartMonthObj);


		JSONArray morrisChartYearObj = salesTargetService.getMorrisYearData();


		model.addAttribute("morrisChartYearObj",morrisChartYearObj);


		SalesRecord thisMonth = salesTargetService.updateSalesRecordOfTheMonth();

        model.addAttribute("thisMonth",thisMonth);

		return SbLibrary.getPageFragment(request, "sales-target");
	}
	
	
	@RequestMapping(value = "/serviceSettings", method = RequestMethod.POST)
	public @ResponseBody JSONObject  serviceSettingsPost(@RequestBody JSONObject postData, HttpServletRequest request, Model model)
	{




		Set<String> keys = postData.keySet();

		List<ProductConfig> configs = new ArrayList<ProductConfig>();


		for(String configKey:keys)
		{
//			System.out.println(configKey);
//			System.out.println(postData.get(configKey));
//
			configs.add(new ProductConfig(configKey, (String) postData.get(configKey)));
		}



//		notificationService.notifyAllSalesRep("Profit Margin Changed!","There has been some changes made in the profit margin.Please Refresh all Open proposals",0,"");
//

		configs = (List<ProductConfig>) productConfigService.update(configs);


		return SbLibrary.getResponseObj(true, "Service Configs Updated",configs);
	
	}

	@RequestMapping(value = "/updateSalesTarget", method = RequestMethod.POST)
	public @ResponseBody JSONObject  updateSalesTarget(@RequestBody JSONObject postData, HttpServletRequest request, Model model)
	{
		long target = Long.parseLong((String) postData.get("target"));

		return salesTargetService.setTarget(target);

	}

}
