package com.worksap.stm.sb.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.service.*;
import org.joda.time.LocalDate;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.Proposal;
import com.worksap.stm.sb.type.ProposalStatus;
import com.worksap.stm.sb.utils.SbLibrary;


@Controller
public class ProposalController
{

	@Autowired
	RequirementService requirementService;
	
	@Autowired
	ProductConfigService productConfigService;
	
	
	@Autowired
	OpportunityService opportunityService;
	
	@Autowired
	ProposalService proposalService;
	
	@Autowired
	ClientService clientService;

    @Autowired
    NoteService notesService;
    @Autowired
    TimelineService timelineService;
    @Autowired
    TaskService taskService;
    @Autowired
    EmailService emailService;

    @RequestMapping(value="/newProposal/{clientid}",method=RequestMethod.GET)
	public @ResponseBody JSONObject generateNewProposal(@PathVariable("clientid") long clientid)
	{
            return proposalService.createNew(clientid);
	}

	
	@RequestMapping(value="/hasOpenProposal/{clientid}",method=RequestMethod.GET)
	public @ResponseBody List<Proposal> hasOpenProposal(@PathVariable("clientid") long clientid)
	{
		
		return proposalService.getOpenAndUnExpiredProposals(clientid);
		
	}
	
	
	@RequestMapping(value="/detailProposal/{id}",method=RequestMethod.GET)
	public String editProposalGet(@PathVariable("id") long id, HttpServletRequest request,Model model)
	{
		Proposal proposal =  proposalService.getProposalById(id);
		
		model.addAttribute("proposal",proposal);


		
		long clientid = proposal.getClientid();
		
		JSONObject clientObj = clientService.getClientById(clientid);
		
		Client client = (Client) clientObj.get("data");

		model.addAttribute("data",client);

		model.addAttribute("clientid",clientid);


        JSONObject notesObj = notesService.getClientNotes(clientid);

        if (notesObj.get("data") != null)
        {
            model.addAttribute("notes", notesObj.get("data"));

        }

        JSONObject tEventsObj = timelineService.getTimelineEvents(clientid);

        if (tEventsObj.get("data") != null)
        {
            model.addAttribute("tEvents", tEventsObj.get("data"));

        }

        JSONObject taskObject = taskService.getTasksByClient(clientid);

        if (taskObject.get("data") != null)
        {
            model.addAttribute("tasks", taskObject.get("data"));

        }

        JSONObject reqObj = requirementService.getRequirementsByClient(clientid);


        if (reqObj.get("data") != null)
        {
            model.addAttribute("requirements", reqObj.get("data"));

        }

        JSONObject reqInfoObj = requirementService.getRequirementsInfoByClient(clientid);


        if (reqInfoObj.get("data") != null)
        {
            model.addAttribute("reqInfos", reqInfoObj.get("data"));

        }



        JSONObject propObj = proposalService.getProposalByClient(clientid);


        if (propObj.get("data") != null)
        {
            model.addAttribute("proposals", propObj.get("data"));

        }

        JSONObject emailSentObj = emailService.getSentEmailByCLientid(clientid);


        if (emailSentObj.get("data") != null)
        {
            model.addAttribute("sentmails", emailSentObj.get("data"));

        }

        model.addAttribute("backurl", SbLibrary.getUrl("/getClientDetails/", clientid));
		
		return SbLibrary.getPageFragment(request, "edit-proposal");
	}
	
	@RequestMapping(value="/updateProposal/{id}",method=RequestMethod.POST)
	public @ResponseBody JSONObject updateProposalPrice(@PathVariable("id") long id, @RequestBody JSONObject postData, HttpServletRequest request,Model model)
	{

//		System.out.println(postData);



        LocalDate start = LocalDate.parse((String) postData.get("start"));
        long duration = Long.parseLong((String) postData.get("validfor"));
        float discount = Float.parseFloat((String) postData.get("givendiscount"));
        long cleaners = Long.parseLong((String) postData.get("numberofcleaners"));

        discount /=100;

//        System.out.println(start);
//        System.out.println(duration);
//        System.out.println(discount);
//        System.out.println(cleaners);
//

        return proposalService.updateProposal(id,start,duration,discount,cleaners);

	}
	
	
	@RequestMapping(value="/deleteProposal",method=RequestMethod.POST)
	public @ResponseBody JSONObject deleteProposal(@RequestBody JSONObject postData, HttpServletRequest request, Model model, BindingResult bindingResult)
	{
		
		JSONObject retObj = new JSONObject();
		
		if (bindingResult.hasErrors())
		{
			retObj.put("success", false);
			retObj.put("msg", "Failed to Delete");
			
		} else
		{
			// Create in DB and return
			
			
			String idStr = (String) postData.get("id");
			
			long id = Long.parseLong(idStr);
			
			retObj = proposalService.delete(id);
			
		}
		
		return retObj;
	}
	
	
	
	@RequestMapping(value="/updateProposalStatus/{id}/{status}",method=RequestMethod.GET)
	public  String updateProposalStatus(@PathVariable long id, @PathVariable ProposalStatus status)
	{

		Proposal proposal = proposalService.updateStatus(id,status);
	
		return SbLibrary.getRedirectUrl("/detailProposal/", proposal.getId());
		
	}

	@RequestMapping(value="/removeOpp/{id}/{oppid}",method=RequestMethod.GET)
	public  @ResponseBody JSONObject updateProposalStatus(@PathVariable long id, @PathVariable long oppid)
	{

		return proposalService.removeOpp(id, oppid);

	}
	@RequestMapping(value="/refreshPrices/{id}",method=RequestMethod.GET)
	public  String refreshPrices(@PathVariable long id)
	{

		Proposal proposal = proposalService.refreshPrices(id);

		return SbLibrary.getRedirectUrl("/detailProposal/", proposal.getId());

	}

//	@RequestMapping(value="/mailProposal/{id}",method=RequestMethod.GET)
//	public  String mailProposal(@PathVariable long id, RedirectAttributes redAttr)
//	{
//		Proposal proposal = proposalService.getProposalById(id);
//
//		redAttr.addAttribute("mailtext", proposalService.formattedProposalForMail(id));
//
//		redAttr.addAttribute("backurl", SbLibrary.getUrl("/detailProposal/", id));
//
//		return SbLibrary.getRedirectUrl("/sendEmail/", proposal.getClientid());
//
//	}


	@RequestMapping(value="/negotiatingDeals",method=RequestMethod.GET)
	public  String negotiatingDeals(HttpServletRequest request, Model model)
	{

		List<Proposal> proposals = (List<Proposal>) proposalService.getProposedDeals();

		model.addAttribute("proposals",proposals);

		return SbLibrary.getPageFragment(request,"current-proposals");
	}

	

	
}
