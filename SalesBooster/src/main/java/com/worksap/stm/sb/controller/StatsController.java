package com.worksap.stm.sb.controller;

import com.worksap.stm.sb.domain.SalesRecord;
import com.worksap.stm.sb.domain.embedded.SalesStats;
import com.worksap.stm.sb.service.SalesRecordService;
import com.worksap.stm.sb.service.StatsService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by mamun on 10/1/2016.
 */


@Controller
public class StatsController {


    @Autowired
    StatsService statsService;
    @Autowired
    SalesRecordService salesRecordService;


    @RequestMapping(value = "/getCurrentSales",method = RequestMethod.GET)
    public @ResponseBody JSONObject getCurrentSales()
    {

        SalesRecord curSalesRecord = salesRecordService.updateSalesRecordOfTheMonth();

        List<SalesStats> won = statsService.getLastNWonProposal();
        List<SalesStats> lost = statsService.getLastNLostProposal();
        List<SalesStats> current = statsService.getLastNProposedProposal();
        List<SalesStats> latest = statsService.getLatestNDeals();


        JSONObject latestDeals = new JSONObject();

        latestDeals.put("won",won);
        latestDeals.put("lost",lost);
        latestDeals.put("current",current);
        latestDeals.put("latest",latest);

        JSONObject retObj = new JSONObject();

        retObj.put("sales",curSalesRecord);
        retObj.put("deals",latestDeals);


        return retObj;
    }

}
