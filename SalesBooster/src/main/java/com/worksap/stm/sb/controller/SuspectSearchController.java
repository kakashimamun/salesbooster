package com.worksap.stm.sb.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.worksap.stm.sb.service.SuspectSearchService;
import com.worksap.stm.sb.utils.SbLibrary;

@Controller
public class SuspectSearchController
{

	@Autowired
	private SuspectSearchService ssService;

	@RequestMapping(value = "/searchSD", method = RequestMethod.GET)
	public String sdSearch(HttpServletRequest request, Model model,
			HttpSession session)
	{

		return SbLibrary.getPageFragment(request, "search-sd");
	}

	@RequestMapping(value = "/searchSD", method = RequestMethod.POST)
	public @ResponseBody ArrayList<HashMap<String, String>> getResults(
			@RequestParam("url") String url, HttpServletRequest request)
	{

		return ssService.getCompanies(url);

	}
	
	
	@RequestMapping("/searchGMap")
	public String loadPlacesForm(HttpServletRequest request)
	{
		
		return SbLibrary.getPageFragment(request, "search-gmap");
	}

}
