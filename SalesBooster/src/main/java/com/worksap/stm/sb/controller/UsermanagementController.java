package com.worksap.stm.sb.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.service.UserService;
import com.worksap.stm.sb.utils.PasswordHash;
import com.worksap.stm.sb.utils.SbLibrary;


@Controller
public class UsermanagementController {
	
	
	@Autowired
	private UserService userService;
	

	
	
	@PreAuthorize("hasAuthority('SYS_ADMIN')")
	@RequestMapping(value = "/usermanagement", method = RequestMethod.GET)
	public String getAllUsers(HttpServletRequest request,ModelMap model) {
		
		
		
		List<User> users = null;
		
		try {
			users = userService.getUsers();

			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 model.addAttribute("users",users);
		 
		 
		 

		
		 return SbLibrary.getPageFragment(request, "usermanagement");
	}
	
	
	@PreAuthorize("hasAuthority('SYS_ADMIN')")
	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public String createUser(@ModelAttribute User user, HttpServletRequest request,ModelMap model,BindingResult bindingResult) 
	{
		
		System.err.println(user);
		
		user.setEmail("stm.sales.booster@gmail.com");
		user.setEmailPass("stmsb196");
		
		user.setPassword(new PasswordHash().encode(user.getPassword()));
		
		userService.createuser(user);
		
		return SbLibrary.getRedirectUrl("/usermanagement", "");
	}

//	@PreAuthorize("hasAuthority('BM')")
//	@RequestMapping(value = "/getSalesReps", method = RequestMethod.GET)
//	public String getSalesReps(HttpServletRequest request,Model model )
//	{
//
//
//
//
//
//	}
//

	
	
	
	
	
	
	

}
