package com.worksap.stm.sb.crons;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.EventStats;
import com.worksap.stm.sb.domain.IndustryPerformance;
import com.worksap.stm.sb.domain.embededid.IndustryPerformanceId;
import com.worksap.stm.sb.repo.ClientRepo;
import com.worksap.stm.sb.repo.EventStatsRepo;
import com.worksap.stm.sb.repo.IndsutryPerformanceRepo;
import com.worksap.stm.sb.service.*;
import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.type.StatsType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks
{



	@Autowired
	TaskService taskService;

	@Autowired
	ClientService clientService;

	@Autowired
	ProposalService proposalService;

	@Autowired
	EmailService emailService;

	@Autowired
	SalesRecordService salesRecordService;

	@Autowired
	StatsService statsService;

	@Autowired
	ClientRepo clientRepo;

	@Autowired
	EventStatsRepo eventStatsRepo;

	@Autowired
    IndsutryPerformanceRepo indsutryPerformanceRepo;
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(cron = "0 */5 * * * *")
	public void reportCurrentTime()
	{
		//System.out.println("The time is now " + dateFormat.format(new Date()));
	}

	@Scheduled(cron = "0 */5 * * * *")
	public void removeCompletedTasks()
	{
		taskService.deleteCompletedtasks();

	}
	
	
	@Scheduled(cron = "0 */5 * * * *")
	public void deleteRemovedClient()
	{

		clientService.deleteRemovedClient();

	}
	
	
	@Scheduled(cron = "0 */5 * * * *")
	public void setProposalExpiry()
	{
		proposalService.setExpiredProposals();
		
	}
	
	
	@Scheduled(cron = "0 */5 * * * *")
	public void sendEmail() throws IOException
	{
		emailService.sendUnsentEmails();
		
	}

	@Scheduled(cron = "0 0 * * * *")
	public void setMissedTask() throws IOException
	{

		taskService.setMissedTasks();

	}

	@Scheduled(cron = "0 */5 * * * *")
	public void setCompletedteamMeetings() throws IOException
	{

		taskService.setTeamMeetingCompleted();

	}

	@Scheduled(cron = "*/15 * * * * *")
	public void generateSalesStats() throws IOException
	{

//		statsService.generateSalesStats();
//		statsService.generateEventStats();


//		System.out.println("Called!!");
//
//		List<Client> clients = (List<Client>) clientRepo.findAll();
//
//
//		for(Client client: clients)
//		{
//			if(client.getSrepresentative() != null)
//			{
//
//				EventStats eventStats = new EventStats();
//
//				eventStats.setClientid(client.getId());
//				eventStats.setSalesrep(client.getSrepresentative());
//
//
//				switch(client.getStatus())
//				{
//					case EXCLIENT:
//						eventStats.setType(StatsType.EXCLIENT);
//						eventStats.setPrestatus(ClientStatusType.ACTIVE);
//						eventStatsRepo.save(eventStats);
//
//						System.out.println(eventStats);
//
//					case ACTIVE:
//						eventStats.setType(StatsType.ACTIVE);
//						eventStats.setPrestatus(ClientStatusType.PROPOSED);
//						eventStatsRepo.save(eventStats);
//						System.out.println(eventStats);
//					case PROSPECT:
//
//						eventStats.setType(StatsType.PROSPECT);
//						eventStats.setPrestatus(ClientStatusType.SUSPECT);
//						eventStatsRepo.save(eventStats);
//						System.out.println(eventStats);
//					case SUSPECT:
//						eventStats.setType(StatsType.SUSPECT);
//						eventStats.setPrestatus(null);
//						eventStatsRepo.save(eventStats);
//						System.out.println(eventStats);
//						break;
//
//
//					case REMOVED:
//						eventStats.setType(StatsType.REMOVED);
//						eventStats.setPrestatus(ClientStatusType.SUSPECT);
//						eventStatsRepo.save(eventStats);
//						System.out.println(eventStats);
//						break;
//
//					default:
//						eventStats.setType(StatsType.PROSPECT);
//						eventStats.setPrestatus(ClientStatusType.SUSPECT);
//						eventStatsRepo.save(eventStats);
//						System.out.println(eventStats);
//
//				}
//
//
//
//			}
//		}
//
//		System.out.println("Finished!!");


//		statsService.getRevenueByProduct();




	}
//
//    @Scheduled(cron = "* * * * * *")
//    public void generateIndustryPerformance() throws IOException {
//
//
//
//
//       IndustryPerformanceId id = new IndustryPerformanceId();
//
//
//       id.setIndustry("real_estate_agency");
//
//        int saId = 1 + (int) (Math.random() * ((5 - 1) + 1));
//
//        String srep = "SA"+saId;
//
//        id.setUsername(srep);
//
//
//        IndustryPerformance performance = new IndustryPerformance();
//
//        performance.setId(id);
//
//        long revenue = 1 + (int) (Math.random() * ((10 - 1) + 1));
//        performance.setAmount(revenue);
//
//        long deal = 10000 + (int) (Math.random() * ((1000000 - 10000) + 1));
//        performance.setNoofdeals(deal);
//
//
//        indsutryPerformanceRepo.save(performance);
//
//    }
}