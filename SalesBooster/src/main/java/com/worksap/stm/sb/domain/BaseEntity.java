package com.worksap.stm.sb.domain;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.joda.time.LocalDateTime;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class BaseEntity
{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true)
	private long id;

	@DateTimeFormat(pattern = "dd/MM/yyyy-HH:mm")
	private LocalDateTime created;

	@DateTimeFormat(pattern = "dd/MM/yyyy-HH:mm")
	private LocalDateTime updated;
	
	
	@Version
	private long version;

	@PrePersist
	public void setCreationDate()
	{
		this.created = LocalDateTime.now();
	}

	@PreUpdate
	public void setChangeDate()
	{
		this.updated = LocalDateTime.now();
	}

}
