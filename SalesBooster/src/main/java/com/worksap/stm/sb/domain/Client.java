package com.worksap.stm.sb.domain;


import javax.persistence.*;


import com.sun.scenario.effect.impl.sw.sse.SSEBlend_LIGHTENPeer;
import com.worksap.stm.sb.domain.embedded.Geolocation;
import com.worksap.stm.sb.entity.CurrentUser;
import com.worksap.stm.sb.utils.SbLibrary;
import org.hibernate.validator.constraints.NotBlank;

import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.type.Priority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
@Data
@Entity
@Table(name = "Client")

public class Client extends BaseEntity
{


	@NotBlank
	private String name;

	@NotBlank
	private String address;

	private String phone;

	private String contactname;

	private String contacttitle;

	private String contactno;

	private String website;

	private String email;

	private String industry;

	private long annualrevenue = 0;

	private long employeesize = 0;

	@Embedded
	private Geolocation geolocation;
	
	private String decisiontime;

	@Column(unique=true)
	private String placeid;

	@Column(unique=true)
	private String gmapid;

	@Enumerated(EnumType.STRING)
	@Column(nullable=false)
	private ClientStatusType status;

	@NotBlank
	private String sadder;
	private String srepresentative;

	@Enumerated(EnumType.STRING)
	@Column(nullable=false)
	private Priority priority;


	private long loyalty = 0;


	private long givenleadscore = 0;

	private long leadscore = 0;


	@ManyToMany(cascade = CascadeType.DETACH,fetch = FetchType.EAGER)
	private List<User> delegates = new LinkedList<User>();


	@Transient
	private boolean delegated;

	public boolean isDelegated()
	{
		User cuUser = null;

		try {
			 cuUser = SbLibrary.getCurrentUser().getUser();
		}catch (Exception ex)
		{
			System.err.println(ex.getMessage());
		}

		if(cuUser !=null) {
			if (this.srepresentative != null)
				if (this.srepresentative.equals(cuUser.getUsername())) return false;

			for (User user : this.getDelegates()) {
				if (user.getUsername().equals(cuUser.getUsername())) return true;
			}
		}

		return false;
	}

	private boolean candelegate = false;

}
