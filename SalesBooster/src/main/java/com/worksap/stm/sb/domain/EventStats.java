package com.worksap.stm.sb.domain;


import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.worksap.stm.sb.type.ClientStatusType;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper = true)
@Data
@Entity  
 
public class EventStats extends StatsBase
{

	@Enumerated(EnumType.STRING)
	private ClientStatusType prestatus;

}
