package com.worksap.stm.sb.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;


	
	
	
	@Data
	@Entity
	@Table(name = "file")
	public class File implements Serializable{

	
	private static final long serialVersionUID = 7566754529985407983L;

	  @Id
	  @Column(name="id")
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private long id;
	  
	  @NotNull
	  @Column(name="link")
	  private String link;
	  
	  @NotNull
	  @Column(name="ext")
	  private String ext;
	  
	  @NotNull
	  @Column(name="name")
	  private String name;
	  
	  @NotNull
	  @Column(name="uploader")
	  private String uploader;
	  
	  @NotNull
//	  @Column(name="timestamp", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	  private Timestamp timestamp;
	  

	
	}
	
	
	
	
	
	

