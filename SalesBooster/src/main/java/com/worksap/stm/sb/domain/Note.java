package com.worksap.stm.sb.domain;

import javax.persistence.*;


import com.worksap.stm.sb.type.NoteType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringEscapeUtils;
import org.hibernate.annotations.Type;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Note extends BaseEntity
{

	public String heading;

	@Type(type = "text")
	public String text;

	public String creator;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	public NoteType type;

	@Transient
	public String clientname = "For All";

	@Column(updatable = false, nullable = false)
	public long clientid = 0;


	@PostLoad
	public void postload()
	{
		this.text = StringEscapeUtils.unescapeHtml4(this.getText());
	}


//	public String getText()
//	{
//		return StringEscapeUtils.unescapeHtml4(this.getText());
//	}
}
