package com.worksap.stm.sb.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class ProductConfig
{

	@NotNull
	@Column(unique=true)
	@Id
	private String name;
	
	@NotNull
	private String value;
	
}
