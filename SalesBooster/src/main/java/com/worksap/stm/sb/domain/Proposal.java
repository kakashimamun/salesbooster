package com.worksap.stm.sb.domain;

import java.text.DecimalFormat;
import java.util.List;

import javax.persistence.*;

import com.worksap.stm.sb.type.ProposalStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@Data
@Entity
public class Proposal extends BaseEntity
{



	
	@OneToMany(fetch = FetchType.EAGER ,cascade=CascadeType.ALL,orphanRemoval = true)
	List<Opportunity> opportunities;
	
	private float price;

	private float discountedprice;
	
	private float profit;
	
	private float cost;

	private float perdaycost;

	private long numberofcleaners;

	private long validfor;

	private float discount;

	private float givendiscount = 0;

	private float loyaltydis;

	private String salesrep;
	
	private long clientid;
	
	@Enumerated(EnumType.STRING)
	private ProposalStatus status = ProposalStatus.OPEN;
	
	private boolean expired = false;
	
	private boolean closed = false;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate start;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate end;


	@Transient
	private float loyaltyDisStr;

	public float getLoyaltyDisStr()
	{
		float value = this.loyaltydis*100;

		DecimalFormat df = new DecimalFormat("#.##");

		return Float.parseFloat(df.format(value));
	}

	@Transient
	private float givenDisStr;

	public float getGivenDisStr()
	{
		float value = this.givendiscount*100;

		DecimalFormat df = new DecimalFormat("#.##");

		return Float.parseFloat(df.format(value));
	}
	@Transient
	private float disStr;

	public float getDisStr()
	{
		float value = this.discount*100;

		DecimalFormat df = new DecimalFormat("#.##");

		return Float.parseFloat(df.format(value));
	}

}
