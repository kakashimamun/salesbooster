package com.worksap.stm.sb.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.type.TaskStatus;
import com.worksap.stm.sb.type.TaskType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Task extends BaseEntity
{

	private String title;

	private long clientid;

	@Enumerated(EnumType.STRING)
	private ClientStatusType clientstatus;

	private String srepresentative;

	@DateTimeFormat(pattern = "dd/MM/yyyy-HH:mm")
	private LocalDateTime start;

	@DateTimeFormat(pattern = "dd/MM/yyyy-HH:mm")
	private LocalDateTime end;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private TaskType type;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private TaskStatus status = TaskStatus.DUE;



	private boolean teammeeting = false;
	private boolean missed = false;

}
