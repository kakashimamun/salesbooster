package com.worksap.stm.sb.domain;


import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
@Data
@Entity
public class Timeline extends BaseEntity
{

	
	@NotNull
	private long clientid;
	
	
	@NotBlank
	private String srepresentative;
	
	
	@NotBlank
	private String heading;
	
	
	@NotBlank
	@Type(type="text")
	private String msg;
	

	
}
