package com.worksap.stm.sb.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.mapping.Map;
import org.hibernate.mapping.Set;

import lombok.Data;



	@Data
	@Entity
	@Table(name = "user_account",uniqueConstraints = {
			@UniqueConstraint(columnNames = "username")})
	public class User implements Serializable{

	
	private static final long serialVersionUID = 7566754529985407983L;
//
//	  @Id
//	  @Column(name="id")
//	  @GeneratedValue(strategy = GenerationType.AUTO)
//	  private long id;
//
//
		@Id
	  @NotNull
	  @Column(name="username")
	  private String username;
	  
	  @NotNull
	  @Column(name="password")
	  private String password;
	  
	  @NotNull
	  @Column(name="name")
	  private String name;
	  
	  
	  @Column(name="email")
	  private String email;
	  
	  
	  @Column(name="email_pass")
	  private String emailPass;
	  
	  
	  @NotNull
	  @Column(name="roles")
	  private String roles;
	  
	  
	  
	  


}