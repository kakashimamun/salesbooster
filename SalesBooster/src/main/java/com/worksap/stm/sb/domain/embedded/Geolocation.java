package com.worksap.stm.sb.domain.embedded;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class Geolocation
{

	public String lat;
	public String lng;
	

	public Geolocation(Geolocation geolocation)
	{
		this.lat = geolocation.getLat();
		this.lng = geolocation.getLng();
	}

}


