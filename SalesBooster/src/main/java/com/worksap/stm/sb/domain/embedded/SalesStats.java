package com.worksap.stm.sb.domain.embedded;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.worksap.stm.sb.domain.StatsBase;
import com.worksap.stm.sb.type.ProposalStatus;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper = true)
@Data
@Entity  
 
public class SalesStats extends StatsBase
{

	
	@Column(columnDefinition = "Decimal(10,2) default '00.00'")
	private float revenue;

	@Column(columnDefinition = "Decimal(10,2) default '00.00'")
	private float profit;

	private long proposalid;

	private String industry;

	@Enumerated(EnumType.STRING)
	private ProposalStatus proposalstatus;
	
	
}
