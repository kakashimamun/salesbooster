package com.worksap.stm.sb.domain.embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Created by mamun on 14/1/2016.
 */


@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class Week {


    @NotNull
    private boolean sunday = false;

    @NotNull
    private boolean monday = false;

    @NotNull
    private boolean tuesday = false;

    @NotNull
    private boolean wednesday = false;

    @NotNull
    private boolean thursday = false;

    @NotNull
    private boolean friday = false;

    @NotNull
    private boolean saturday = false;


    
    
    public String toString()
    {
        StringBuilder sb = new StringBuilder("");

        if(this.isSunday())
        {
            sb.append("Sunday, ");
        }
        if(this .isMonday())
        {
            sb.append("Monday, ");
        }

        if(this .isTuesday())
        {
            sb.append("Tuesday, ");
        }
        if(this .isWednesday())
        {
            sb.append("Wednesday, ");
        }

        if(this .isThursday())
        {
            sb.append("Thursday, ");
        }

        if(this .isFriday())
        {
            sb.append("Friday, ");
        }

        if(this .isSaturday())
        {
            sb.append("Saturday, ");
        }

        String retStr = sb.toString();

        retStr = retStr.substring(0, retStr.lastIndexOf(","));

        return retStr;


    }
}
