package com.worksap.stm.sb.domain.embedded;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Created by mamun on 14/1/2016.
 */


@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class WorkingShift {



    @NotNull
    private boolean day;

    @NotNull
    private boolean night;

}
