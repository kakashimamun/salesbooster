package com.worksap.stm.sb.domain.embededid;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Embeddable

public class SalesRecordId implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1655078135435418358L;
	
	
	int monthOfYear;
	int yearOfCentury;
}
