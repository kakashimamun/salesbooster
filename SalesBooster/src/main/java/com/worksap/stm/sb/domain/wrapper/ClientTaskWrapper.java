package com.worksap.stm.sb.domain.wrapper;

import com.worksap.stm.sb.controller.LoginController;
import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.Task;
import com.worksap.stm.sb.type.TaskStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDateTime;
import org.joda.time.Minutes;

/**
 * Created by mamun on 18/1/2016.
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClientTaskWrapper {



    private Client client;
    private Minutes gone;
    private Minutes remained;
    private String srep;
    private TaskStatus status;

    private String taskstatus;
    private String taskTitle;

    private String cssClass;

    private String text;


    public ClientTaskWrapper(Client c,Task t)
    {
        this.client = c;

        if(t == null)
        {
            this.gone = Minutes.MAX_VALUE;
            this.remained = Minutes.MIN_VALUE;
            this.srep = "Not assigned";
            this.status = TaskStatus.COMPLETED;
            this.taskstatus = "No Task";
            this.text = "Cleint has no task assigned";
            this.taskTitle = "No Task assigned";
            this.cssClass = "";
        }
        else {
            this.gone = Minutes.minutesBetween(t.getStart(), LocalDateTime.now());
            this.remained = Minutes.minutesBetween(LocalDateTime.now(), t.getEnd());


            if (gone.getMinutes() > 0 && remained.getMinutes() < 0) {
                this.taskstatus = "Missed";
                this.cssClass = "missed";

                this.text = "Task ended " + remained.getMinutes() + " minutes ago";

            } else if (gone.getMinutes() > 0 && remained.getMinutes() > 0) {
                this.taskstatus = "Running";
                this.cssClass = "current";

                this.text = "Task Started " + gone.getMinutes() + " minutes ago. " + remained.getMinutes() + " minutes left to end.";

            } else if (gone.getMinutes() < 0 && remained.getMinutes() > 0) {
                this.taskstatus = "Upcoming";
                this.cssClass = "upcoming";

                this.text = remained.getMinutes() + " minutes left to start";
            }


            this.status = t.getStatus();

            if (t.getStatus().equals(TaskStatus.COMPLETED)) {
                this.cssClass = "completed";

                this.text = "Task Completed. Needs to Create new task";
            }

            this.srep = t.getSrepresentative();

            this.taskTitle = t.getTitle();
        }

    }


    public String getGone()
    {
        return this.gone.getMinutes()+"m";
    }

    public String getRemained()
    {
        return this.remained.getMinutes()+"m";
    }








}
