package com.worksap.stm.sb.domain.wrapper;


import javax.validation.constraints.NotNull;


import com.worksap.stm.sb.domain.embedded.Week;
import com.worksap.stm.sb.domain.embedded.WorkingShift;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RequirementWrapper
{


	@NotNull
	private Week week ;
	
	@NotNull
	private int timesinshift;

	@NotNull
	private float area;

	@NotNull
	private String surface;
	
	@NotNull
	private String task;

	@NotNull
	private WorkingShift shift ;
	
	private boolean required = false;
}
