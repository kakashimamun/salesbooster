package com.worksap.stm.sb.dto;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;




@AllArgsConstructor
@NoArgsConstructor
@Data
public class FileDto {

	
	private int id;
	private String link;
	private String name;
	private String ext;
	private String uplaoder;
	private Timestamp timestamp;
}
