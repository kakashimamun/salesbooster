package com.worksap.stm.sb.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

	
	
	@AllArgsConstructor
	@NoArgsConstructor
	@Data
	public class TeamDto {
		private String name;
	}
