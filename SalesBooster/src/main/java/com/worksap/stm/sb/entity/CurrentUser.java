package com.worksap.stm.sb.entity;


import org.springframework.security.core.authority.AuthorityUtils;

import com.worksap.stm.sb.domain.User;


public class CurrentUser extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = -4568931000872570880L;

	private User user;

	public CurrentUser(User user) {
		super(user.getUsername(), user.getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRoles()));
		this.user = user;
	}

	public User getUser() {
		return user;
	}

}
