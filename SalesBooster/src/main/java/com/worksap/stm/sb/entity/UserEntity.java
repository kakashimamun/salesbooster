package com.worksap.stm.sb.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.worksap.stm.sb.dto.RoleDto;
import com.worksap.stm.sb.dto.TeamDto;
import com.worksap.stm.sb.dto.UserDto;
import com.worksap.stm.sb.utils.SbLibrary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserEntity  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1494771364008787353L;


	public UserEntity(UserDto user, List<RoleDto> roles, List<TeamDto> teams) {
		// TODO Auto-generated constructor stub
		this.id = user.getId();
		this.username = user.getUsername();
		this.name = user.getName();
		this.password = user.getPassword();
		this.email = user.getEmail();
		this.email_pass = user.getEmail_pass();
		this.roles = roles;
		this.teams = teams;
		
	}
	
	private String id;
	private String username;
	private String name;
	private String password;
	private String email;
	private String email_pass;
	private List<RoleDto> roles;
	private List<TeamDto> teams;

	
	public String getRoles()
	{
		StringBuilder sb   = new StringBuilder();
		
		for(RoleDto role:this.roles)
		{
			sb.append(role.getName());
			sb.append(",");
		}
		
		return  SbLibrary.trimRegex(sb.toString());
	}
	
	
	public String getTeams()
	{
		StringBuilder sb   = new StringBuilder();
		
		for(TeamDto team:this.teams)
		{
			sb.append(team.getName());
			sb.append(",");
		}
		
		return SbLibrary.trimRegex(sb.toString());
	}
	
	
	
	
	public void setRoles(String roles)
	{
		String[] rolez = roles.split(",");
		
		
		this.roles = new ArrayList<RoleDto>();
		for(String s:rolez)
		{
			this.roles.add(new RoleDto(s));
		}
	}
	public void setTeams(String teams)
	{
		String[] teamz = teams.split(",");
		
		
		this.teams = new ArrayList<TeamDto>();
		for(String s:teamz)
		{
			this.teams.add(new TeamDto(s));
		}
	}
}
