package com.worksap.stm.sb.repo;

import java.math.BigInteger;
import java.util.List;

import com.worksap.stm.sb.domain.Task;

import org.json.simple.JSONObject;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.type.ClientStatusType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;




public interface ClientRepo extends PagingAndSortingRepository<Client, Long>
{


	List<Client> findByStatusAndSrepresentativeIsNull(ClientStatusType status);

	List<Client> findByStatus(ClientStatusType status);
	
	List<Client> findBySrepresentativeIsNotNull();


	long countByStatusNotAndSrepresentativeIsNull(ClientStatusType prospect);

	long countByStatusAndSrepresentativeIsNull(ClientStatusType status);


	@Query(value="SELECT * FROM client c WHERE c.srepresentative = ?1 ORDER BY c.name", nativeQuery=true)
	List<Client> findBySrepresentativeOrderByName(String srepresentative);

	
	@Query(value="SELECT * FROM client c  WHERE c.status = 'REMOVED'",nativeQuery=true)
	List<Client> findRemovedClients();

	
	@Query(value="SELECT * FROM client c WHERE c.srepresentative = ?1 AND c.status = ?2 ORDER BY c.name", nativeQuery=true)
	List<Client> findBySrepAndStatusOrderByName(String srepresentative, String status);


	@Query(value = "select c.gmapid from client c",nativeQuery = true)
	List<String> findAllGmapIds();


	List<Client> findByStatusNotAndSrepresentativeIsNull(ClientStatusType suspect);

	long countBySrepresentative(String username);



	long countByStatusAndSadderAndSrepresentativeIsNull( ClientStatusType suspect,String username);

	List<Client> findByStatusAndSadderAndSrepresentativeIsNull(ClientStatusType suspect, String username);


	@Query(value = "SELECT * from client c where  c.srepresentative = ?1 ORDER BY name",nativeQuery = true)
	List<Client> findBySadderAndSrepresentativeOrderByName(String username);




	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "Update client c set c.srepresentative = ?2 where c.id = ?1",nativeQuery = true)
	void addSalesRepresentative(long clientid, String salesrep);


	@Query(value = "select distinct client.id from  client left join delegates on " +
			"client.id = delegates.Client where client.srepresentative = ?1 or delegates.delegates = ?1",nativeQuery = true)
	List<BigInteger> findAllMyClientIds(String username);


	@Query(value = "select task.clientid from task where task.clientid in (\n" +
			"(select distinct client.id from  client left join delegates on client.id = delegates.Client" +
			" where client.srepresentative = ?1 or delegates.delegates = ?1)) " +
			"and task.status = 'DUE' group by task.clientid order by (now() - task.start) desc,(now() - task.end) ",nativeQuery = true)
	List<BigInteger> findMyClientIdsSortedByNearestDuetask(String username);




	@Query(value = "select * from task\n" +
			" where task.clientid = ?1 order by (now() - task.start) desc,(task.end - now()) limit 1",nativeQuery = true)
    Task closestTask(long clientid);



	@Query(value = "Select distinct client.id from client",nativeQuery = true)
	List<BigInteger> findAllIds();


	@Query(value = "select task.clientid from task where task.status = 'DUE' " +
			"group by task.clientid order by (now() - task.start) desc,(now() - task.end) ",nativeQuery = true)
	List<BigInteger> findAllIdsSortedByNearestDuetask();


}
