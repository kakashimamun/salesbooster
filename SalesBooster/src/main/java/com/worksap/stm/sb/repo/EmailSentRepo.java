package com.worksap.stm.sb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.EmailSent;
import com.worksap.stm.sb.domain.Timeline;

public interface EmailSentRepo extends PagingAndSortingRepository<EmailSent, Long>
{

	@Query(value="SELECT * FROM emailsent e WHERE e.sent = false AND NOW() > e.sendtime ",nativeQuery=true)
	List<EmailSent> findEmailsToSend();

	
	
	
	@Query(value="SELECT * FROM emailsent e WHERE e.clientid= ?1 ORDER BY e.sendtime DESC", nativeQuery=true)
	
	List<EmailSent> findByClientid(Long id);

}
