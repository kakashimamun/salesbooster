package com.worksap.stm.sb.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.EventStats;

import java.util.List;

public interface EventStatsRepo extends PagingAndSortingRepository<EventStats, Long>
{

    List<EventStats> findBySalesrep(String username);
}
