package com.worksap.stm.sb.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import com.worksap.stm.sb.domain.File;

public interface FileRepo extends Repository<File, Integer>
{

	File save(File f);

	@Query(value = "SELECT link FROM file t WHERE t.id = ?1", nativeQuery = true)
	String findLinkById(long id);

}
