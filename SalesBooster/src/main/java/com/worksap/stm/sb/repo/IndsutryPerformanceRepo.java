package com.worksap.stm.sb.repo;

import com.worksap.stm.sb.domain.IndustryPerformance;
import com.worksap.stm.sb.domain.embededid.IndustryPerformanceId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by mamun on 13/1/2016.
 */
public interface IndsutryPerformanceRepo extends PagingAndSortingRepository<IndustryPerformance,IndustryPerformanceId> {


    @Query(value = "SELECT * from IndustryPerformance where industry=?1",nativeQuery = true)
    List<IndustryPerformance> findByIndustry(String industry);
}
