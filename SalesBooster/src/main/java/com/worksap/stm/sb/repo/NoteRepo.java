package com.worksap.stm.sb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.Note;

public interface NoteRepo extends PagingAndSortingRepository<Note, Long>
{

	
	@Query(value = "SELECT * FROM note n where n.clientid = 0 OR n.clientid = ?1", nativeQuery=true)
	List<Note> findByClientidAndAll(long clientid);


	List<Note> findByClientid(long i);
}
