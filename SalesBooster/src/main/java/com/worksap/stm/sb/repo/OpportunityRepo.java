package com.worksap.stm.sb.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.Opportunity;



public interface OpportunityRepo extends PagingAndSortingRepository<Opportunity	, Long>
{

	
	
}
