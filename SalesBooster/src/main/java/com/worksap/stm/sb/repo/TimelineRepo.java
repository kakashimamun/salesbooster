package com.worksap.stm.sb.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.worksap.stm.sb.domain.Timeline;

public interface TimelineRepo extends PagingAndSortingRepository<Timeline, Long>
{

	
	@Query(value="SELECT * from timeline t where t.clientid= ?1 ORDER BY t.created DESC", nativeQuery=true)
	
	List<Timeline> findByClientidOrderByCreated(long clientid);
	
}
