package com.worksap.stm.sb.repo;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.worksap.stm.sb.domain.User;

public interface UserRepo extends Repository<User, Integer>
{

	User findByUsername(String username);

	List<User> findAll();

	User save(User persisted);
	
	List<User> findByRoles(String roles);

}
