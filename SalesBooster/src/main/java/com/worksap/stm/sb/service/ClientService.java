package com.worksap.stm.sb.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.google.common.cache.CacheLoader;
import com.google.common.math.LongMath;
import com.sun.jmx.snmp.SnmpVarBindList;
import com.worksap.stm.sb.domain.Task;
import com.worksap.stm.sb.domain.wrapper.ClientTaskWrapper;
import com.worksap.stm.sb.type.StatsType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.repo.ClientRepo;
import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.type.Priority;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class ClientService
{

	@Autowired
	ClientRepo clientRepo;

	@Autowired
	TimelineService timelineService;

	@Autowired
	TaskService taskService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	StatsService statsService;

//	private void afterNewSuspectAdded(Client newClient)
//	{
//		User curUser = SbLibrary.getCurrentUser().getUser();
//
//		timelineService.create(newClient.getId(), "Added!!", "Added to the pipeline by " + curUser.getName());
//
//		notificationService.create("New Suspect!!", "A suspect have been added by "+newClient.getSadder()+". Please assign it to a Sales Associate", newClient.getId(),
//				"Manager", "/unassignedSuspects");
//
//		notificationService.create("Suspect Submitted!!", "You have added a new suspect. Please wait for the manager's approval", newClient.getId(),
//				curUser.getUsername(), "/");
//	}
//
//	private Client beforeNewSuspectAdded(Client newClient)
//	{
//
//		User curUser = SbLibrary.getCurrentUser().getUser();
//
//		if (newClient.getStatus() == null)
//		{
//			newClient.setStatus(ClientStatusType.SUSPECT);
//		}
//
//		if (newClient.getSadder() == null)
//		{
//			newClient.setSadder(curUser.getUsername());
//
////			if(!curUser.getRoles().equals("BM"))
////			{
////				newClient.setSadder("BM");
////			}
//		}
//
//		if (newClient.getPriority() == null)
//		{
//			newClient.setPriority(Priority.LOW);
//		}
//
//		return newClient;
//	}
//
//	public JSONObject addSuspect(Client newClient)
//	{
//		Client addedClient = null;
//
//		newClient = beforeNewSuspectAdded(newClient);
//
//		try
//		{
//			addedClient = clientRepo.save(newClient);
//
//		} catch (Exception daEx)
//		{
//
//			return SbLibrary.getResponseObj(false, daEx.getMessage());
//		}
//
//		afterNewSuspectAdded(addedClient);
//
//		return SbLibrary.getResponseObj(true, "New Suspect Submitted", addedClient);
//
//	}

	public JSONObject getAllUnassigenedSuspect()
	{

		String username = SbLibrary.getCurrentUser().getUsername();

		List<Client> suspects = null;
		try
		{
			suspects = clientRepo.findByStatusAndSrepresentativeIsNull(ClientStatusType.SUSPECT);

		} catch (DataAccessException daEx)
		{
			return SbLibrary.getResponseObj(false, daEx.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Unassigned Suspects of " + username, suspects);
	}

	public JSONObject getAllAssignedClients()
	{
		String username = SbLibrary.getCurrentUser().getUsername();
		List<Client> clients = null;
		try
		{
			clients = clientRepo.findBySrepresentativeIsNotNull();

		} catch (DataAccessException daEx)
		{
			return SbLibrary.getResponseObj(false, daEx.getMessage());
		}

		return SbLibrary.getResponseObj(true, "All clients contacts of added by " + username, clients);
	}

	public JSONObject getMyClients()
	{
		User user = SbLibrary.getCurrentUser().getUser();

		List<Client> clients = null;
		try
		{
			if (user.getRoles().equals("BM"))
			{
				clients = (List<Client>) clientRepo.findAll();

			} else
			{
				clients = clientRepo.findBySadderAndSrepresentativeOrderByName(user.getUsername());
			}

		} catch (DataAccessException daEx)
		{
			return SbLibrary.getResponseObj(false, daEx.getMessage());
		}

		return SbLibrary.getResponseObj(true, "All Clients of " + user.getUsername(), clients);
	}

	public JSONObject getClientById(Long id)
	{
		Client clientData = null;

		try
		{
			clientData = clientRepo.findOne(id);
		} catch (Exception ex)
		{
			// TODO: handle exception
			return SbLibrary.getResponseObj(false, ex.getMessage());

		}

		if (clientData != null)
			return SbLibrary.getResponseObj(true, "Client Data for id:" + id, clientData);

		return SbLibrary.getResponseObj(false, "Error Happend fetching data from Database");

	}

	private void statusUpdated(Client oldClient, Client updatedClient)
	{
		User curUser = SbLibrary.getCurrentUser().getUser();

		timelineService.create(updatedClient.getId(), "Status changed!!",
				"Changed status to " + updatedClient.getStatus() + " from " + oldClient.getStatus() + " by " + curUser.getName());

		taskService.setTaskCompleted(updatedClient.getId(), oldClient.getStatus());

		statsService.clientStatusChangeEvent(oldClient, updatedClient);

		if(updatedClient.getStatus().equals(ClientStatusType.PROSPECT) && oldClient.getStatus().equals(ClientStatusType.SUSPECT))
		{
			updatedClient.setSrepresentative(null);

			clientRepo.save(updatedClient);

			notificationService.create("New Prospect","A new Prospect has been added to the pipeline.Please assign to proper sales reresentative",
					updatedClient.getId(),"Manager","/unassignedProspects");


			notificationService.create("Prospect Submitted","The client that you upgraded as prospect is submitted to the pipeline.",
					updatedClient.getId(),updatedClient.getSrepresentative(),"");

		}else
		{
			taskService.createAutomatedTask(updatedClient);
		}

	}

	private void srepChanged(Client oldClient, Client updatedClient)
	{
		User curUser = SbLibrary.getCurrentUser().getUser();

		if (oldClient.getSrepresentative() != null)
		{
			notificationService.changeUnseenNotificationsSrep(updatedClient.getId(), updatedClient.getSrepresentative(),
					oldClient.getSrepresentative());

			taskService.changeSrepOfDueTasks(updatedClient.getId(), updatedClient.getSrepresentative());

			timelineService.create(updatedClient.getId(), "Change Sales Representative", "Assigned to " + updatedClient.getSrepresentative()
					+ " from " + oldClient.getSrepresentative() + " by " + curUser.getName());

			notificationService.create("New " + updatedClient.getStatus() + "!!", "You have assigned " + updatedClient.getName()
					+ " and Associated Tasks.Please Schedule the tasks at your prefereble time and visit the Link to See details",
					updatedClient.getId(), updatedClient.getSrepresentative(), "/getClientDetails/" + updatedClient.getId());

			notificationService.create("Client Withdrawn!!", updatedClient.getName()+" is assigned to "+updatedClient.getSrepresentative(), updatedClient.getId(),
					oldClient.getSrepresentative(), "");


		} else
		{

			notificationService.create("New Client!!", "You have assigned a new client. please visit the Link to See details", updatedClient.getId(),
					updatedClient.getSrepresentative(), "/getClientDetails/" + updatedClient.getId());
			if(!updatedClient.getSadder().equals(updatedClient.getSrepresentative()) &&
                    !(updatedClient.getSadder().equals("Manager") | updatedClient.getSadder().equals("Website")))
			{
				notificationService.create("Suspect Assigned!!", "The suspect you submitted has been asigned to "+updatedClient.getSrepresentative(), updatedClient.getId(),
						updatedClient.getSadder(), "");
			}


			taskService.createAutomatedTask(updatedClient);

			timelineService.create(updatedClient.getId(), "Assigned New Representative", "Assigned to " + updatedClient.getSrepresentative() + " by "
					+ curUser.getName());



		}
	}

	private void afterUpdate(Client oldClient, Client update)
	{
		User curUser = SbLibrary.getCurrentUser().getUser();

		timelineService.create(oldClient.getId(), "Information Updated!!", "Updated information by " + curUser.getName());

	}

	public JSONObject updateClient(Client update)
	{
		Client client = clientRepo.findOne(update.getId());

		Client oldClientTrack = new Client();

		// flags to track down changes
		boolean statusChanged = false;
		boolean srepChanged = false;

		oldClientTrack.setId(client.getId());

		oldClientTrack.setName(client.getName());
		if (update.getName() != null && !update.getName().equals(""))
		{
			client.setName(update.getName());
		}

		oldClientTrack.setPriority(client.getPriority());
		if (update.getPriority() != null && !update.getPriority().toString().equals(""))
		{
			client.setPriority(update.getPriority());
		}

		oldClientTrack.setStatus(client.getStatus());
		if (update.getStatus() != null && !update.getStatus().toString().equals(""))
		{
			if (!client.getStatus().toString().equalsIgnoreCase(update.getStatus().toString()))
			{
				statusChanged = true;
			}
			client.setStatus(update.getStatus());
		}

		oldClientTrack.setContactname(client.getContactname());
		if (update.getContactname() != null && !update.getContactname().equals(""))
		{
			client.setContactname(update.getContactname());
		}

		oldClientTrack.setContacttitle(client.getContacttitle());
		if (update.getContacttitle() != null && !update.getContacttitle().equals(""))
		{
			client.setContacttitle(update.getContacttitle());
		}

		oldClientTrack.setContactno(client.getContactno());
		if (update.getContactno() != null && !update.getContactno().equals(""))
		{
			client.setContactno(update.getContactno());
		}

		oldClientTrack.setEmail(client.getEmail());
		if (update.getEmail() != null && !update.getEmail().equals(""))
		{
			client.setEmail(update.getEmail());
		}

		oldClientTrack.setAddress(client.getAddress());
		if (update.getAddress() != null && !update.getAddress().equals(""))
		{
			client.setAddress(update.getAddress());
		}

		oldClientTrack.setPhone(client.getPhone());
		if (update.getPhone() != null && !update.getPhone().equals(""))
		{
			client.setPhone(update.getPhone());
		}

		oldClientTrack.setWebsite(client.getWebsite());
		if (update.getWebsite() != null && !update.getWebsite().equals(""))
		{
			client.setWebsite(update.getWebsite());
		}

		oldClientTrack.setGeolocation(client.getGeolocation());
		if (update.getGeolocation() != null && !update.getGeolocation().toString().equals(""))
		{
			client.setGeolocation(update.getGeolocation());
		}

		oldClientTrack.setIndustry(client.getIndustry());
		if (update.getIndustry() != null && !update.getIndustry().equals(""))
		{
			client.setIndustry(update.getIndustry());
		}

		oldClientTrack.setAnnualrevenue(client.getAnnualrevenue());
		if (update.getAnnualrevenue() != 0)
		{
			client.setAnnualrevenue(update.getAnnualrevenue());
		}

		oldClientTrack.setEmployeesize(client.getEmployeesize());
		if (update.getEmployeesize() != 0 )
		{
			client.setEmployeesize(update.getEmployeesize());
		}

		oldClientTrack.setDecisiontime(client.getDecisiontime());
		if (update.getDecisiontime() != null && !update.getDecisiontime().equals(""))
		{
			client.setDecisiontime(update.getDecisiontime());
		}

		oldClientTrack.setLoyalty(client.getLoyalty());
		if (update.getLoyalty() != 0)
		{
			client.setLoyalty(update.getLoyalty());
		}

		oldClientTrack.setSrepresentative(client.getSrepresentative());
		if (update.getSrepresentative() != null && !update.getSrepresentative().equals(""))
		{
			if (client.getSrepresentative() == null || !client.getSrepresentative().equals(update.getSrepresentative()))
			{
				srepChanged = true;
			}

			client.setSrepresentative(update.getSrepresentative());
		}

		oldClientTrack.setLeadscore(client.getLeadscore());

		client.setLeadscore(calculateLeadScore(client));



//		 System.err.println(update);
//
//		 System.err.println(client);
//
//		 System.err.println(oldClientTrack);
		//
		// System.err.println(srepChanged);
		//
		// System.err.println(statusChanged);

		Client updatedClient = null;

		try
		{
			updatedClient = clientRepo.save(client);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		if (updatedClient != null)
		{

			if (statusChanged)
			{
				statusUpdated(oldClientTrack, updatedClient);
			}

			if (srepChanged)
			{
				srepChanged(oldClientTrack, updatedClient);
			}

			if (!srepChanged && !statusChanged)
				afterUpdate(oldClientTrack, updatedClient);

			return SbLibrary.getResponseObj(true,
					"Updated Client Data for client id:" + updatedClient.getId() + "and name:" + updatedClient.getName(), updatedClient);
		}

		return SbLibrary.getResponseObj(false, "Error Happend Editing CLient Please Refresh page");
	}



    public JSONObject getMyClientsByStatus(ClientStatusType status)
	{
		User user = SbLibrary.getCurrentUser().getUser();
		List<Client> clients = null;
		try
		{
			if (user.getRoles().equals("BM"))
			{
				clients = clientRepo.findByStatus(status);

			} else
			{
				clients = clientRepo.findBySrepAndStatusOrderByName(user.getUsername(), status.toString());

			}

		} catch (DataAccessException daEx)
		{
			return SbLibrary.getResponseObj(false, daEx.getMessage());
		}

		return SbLibrary.getResponseObj(true, "All " + status.toString() + " Clients of " + user.getUsername(), clients);
	}

	public JSONObject assignSalesrep(long clientid, String salesrep) {

        Client client = clientRepo.findOne(clientid);

        String oldSrep = client.getSrepresentative();
        String newSrep = salesrep;

        client.setSrepresentative(salesrep);

        List<User> delegateList = client.getDelegates();

        if(delegateList != null | delegateList.size() == 0) {
            for (User deUser : delegateList) {
                if (deUser.getUsername().equals(salesrep)) {
                    delegateList.remove(deUser);
                }
            }
        }

        client.setDelegates(delegateList);

        Client updatedClient = clientRepo.save(client);

//		this.srepChanged(oldClient,updated);

        User curUser = SbLibrary.getCurrentUser().getUser();

        if (oldSrep != null) {
            notificationService.changeUnseenNotificationsSrep(updatedClient.getId(), updatedClient.getSrepresentative(),
                    oldSrep);

            taskService.changeSrepOfDueTasks(updatedClient.getId(), updatedClient.getSrepresentative());

            timelineService.create(updatedClient.getId(), "Change Sales Representative", "Assigned to " + updatedClient.getSrepresentative()
                    + " from " + oldSrep + " by " + curUser.getName());

            notificationService.create("New " + updatedClient.getStatus() + "!!", "You have assigned " + updatedClient.getName()
                            + " and Associated Tasks.Please Schedule the tasks at your prefereble time and visit the Link to See details",
                    updatedClient.getId(), updatedClient.getSrepresentative(), "/getClientDetails/" + updatedClient.getId());

            notificationService.create("Client Withdrawn!!", updatedClient.getName() + " is assigned to " + updatedClient.getSrepresentative(),
                    updatedClient.getId(), oldSrep, "");


        } else {

            notificationService.create("New Client!!", "You have assigned a new client. please visit the Link to See details", updatedClient.getId(),
                    updatedClient.getSrepresentative(), "/getClientDetails/" + updatedClient.getId());

            if (!updatedClient.getSadder().equals(updatedClient.getSrepresentative()) && !updatedClient.getSadder().equals("Manager")) {
                notificationService.create("Suspect Assigned!!", "The suspect you submitted has been asigned to " + updatedClient.getSrepresentative(), updatedClient.getId(),
                        updatedClient.getSadder(), "");
            }


            taskService.createAutomatedTask(updatedClient);

            timelineService.create(updatedClient.getId(), "Assigned New Representative", "Assigned to " + updatedClient.getSrepresentative() + " by "
                    + curUser.getName());


        }

        return SbLibrary.getResponseObj(true, "Client assigned to " + salesrep, updatedClient);


    }

	public void deleteRemovedClient() {

		List<Client> clients = clientRepo.findRemovedClients();

		clientRepo.delete(clients);
	}

	public JSONObject getAllUnassigenedProspects() {


		String username = SbLibrary.getCurrentUser().getUsername();

		List<Client> prospects = null;
		try
		{
			prospects = clientRepo.findByStatusAndSrepresentativeIsNull(ClientStatusType.PROSPECT);

		} catch (DataAccessException daEx)
		{
			return SbLibrary.getResponseObj(false, daEx.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Unassigned clients of " + username, prospects);
	}


	public JSONObject countMySuspects(String username) {

		long count = 0;

		try
		{
			count = clientRepo.countByStatusAndSadderAndSrepresentativeIsNull(ClientStatusType.SUSPECT,username);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Count of unassigned suspects", count);
	}

	public JSONObject countUnassignedClients(String username) {

		long count = 0;

		try
		{
			count = clientRepo.countByStatusAndSrepresentativeIsNull(ClientStatusType.PROSPECT );

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Count of unassigned clients", count);
	}


    private JSONObject getClientStatsFromList(List<Client> clients)
    {
        int suspects = 0;
        int prospects = 0;
        int profiled = 0;
        int proposed = 0;
        int active = 0;
        int exclient = 0;


        for(Client client:clients)
        {
            if(client.getStatus().equals(ClientStatusType.SUSPECT))
            {
                suspects++;
            }
            else if(client.getStatus().equals(ClientStatusType.PROSPECT))
            {
                prospects++;
            }
            else if(client.getStatus().equals(ClientStatusType.PROFILED))
            {
                profiled++;
            }
            else if(client.getStatus().equals(ClientStatusType.PROPOSED))
            {
                proposed++;
            }
            else if(client.getStatus().equals(ClientStatusType.ACTIVE))
            {
                active++;
            }
            else  if(client.getStatus().equals(ClientStatusType.EXCLIENT))
            {
                exclient++;
            }
        }


        JSONObject retObj = new JSONObject();


        retObj.put("suspects",suspects);
        retObj.put("prospects",prospects);
        retObj.put("profiled",profiled);
        retObj.put("proposed",proposed);
        retObj.put("active",active);
        retObj.put("exclient",exclient);

        return retObj;

    }


    public JSONObject getClientStats(String username) {

        List<Client> clients = clientRepo.findBySrepresentativeOrderByName(username);


        return getClientStatsFromList(clients);

    }

    public JSONObject getClientStats( ) {

        List<Client> clients = (List<Client>) clientRepo.findAll();


        return getClientStatsFromList(clients);

    }



	private long calculateLeadScore(Client client)
    {

		int given = 0;

		if(client.getAnnualrevenue() > 100000) given+=2;
			else if(client.getAnnualrevenue() > 10000) given +=1;


		if(client.getEmployeesize() > 1000) given+=2;
			else if(client.getEmployeesize() > 500) given +=1;


		given += client.getGivenleadscore();

        return given > 5? 5:given;
    }



	public JSONObject addContactUs(Client client) {

		client.setPriority(Priority.MEDIUM);
		client.setStatus(ClientStatusType.PROSPECT);
		client.setSadder("Website");


        client.setGivenleadscore(4);
        client.setLeadscore(calculateLeadScore(client));


		Client addedClient = null;

//        System.out.println(client);

		try
		{
			addedClient = clientRepo.save(client);

		} catch (Exception daEx)
		{
			System.err.println(daEx.getMessage());
            daEx.printStackTrace();

			return SbLibrary.getResponseObj(false, daEx.getMessage());
		}


//        System.out.println(addedClient);

		timelineService.create(addedClient.getId(),null, "Added!!", "Added to the pipeline by Website");

		notificationService.create("New Client!!", "A Potential Client has been added to the pipeline by our website. Please assign to proper sales representative",
				addedClient.getId(),"Manager", "/unassignedProspects");


		return SbLibrary.getResponseObj(true, "New Prospect added", addedClient);


	}


	public JSONObject addBySalesRep(Client client) {

		client.setPriority(Priority.LOW);
		client.setStatus(ClientStatusType.SUSPECT);

        User cuUser = SbLibrary.getCurrentUser().getUser();

		client.setSadder(cuUser.getUsername());

        client.setLeadscore(calculateLeadScore(client));

		Client addedClient = null;

		try
		{

			addedClient = clientRepo.save(client);

		} catch (Exception daEx)
		{
			System.err.println(daEx.getMessage());

			return SbLibrary.getResponseObj(false, daEx.getMessage());
		}


        timelineService.create(addedClient.getId(), "Added!!", "Added to the pipeline by " + cuUser.getName());


        notificationService.create("Suspect Submitted!!", "You have added a new suspect. Please collect information and update him as prospect for manager's approval", addedClient.getId(),
                cuUser.getUsername(), "/");

		return SbLibrary.getResponseObj(true, "New Client added", addedClient);

	}

    public JSONObject addByManager(Client client) {

		client.setPriority(Priority.MEDIUM);
		client.setStatus(ClientStatusType.PROSPECT);

        User cuUser = SbLibrary.getCurrentUser().getUser();
		client.setSadder(cuUser.getUsername());


        client.setGivenleadscore(3);
        client.setLeadscore(calculateLeadScore(client));

		Client addedClient = null;

		try
		{
			addedClient = clientRepo.save(client);

		} catch (Exception daEx)
		{
			System.err.println(daEx.getMessage());

			return SbLibrary.getResponseObj(false, daEx.getMessage());
		}

        timelineService.create(addedClient.getId(), "Added!!", "Added to the pipeline by " + cuUser.getName());

        notificationService.create("New Prospect!!", "A client have been added . Please assign it to a Sales Associate", addedClient.getId(),
                "Manager", "/unassignedProspects");


		return SbLibrary.getResponseObj(true, "New Client added", addedClient);

	}


    public JSONObject getAllGmapId()
    {
        List<String> gmapids = clientRepo.findAllGmapIds();

        JSONArray jsonArray = new JSONArray();

        for(String str:gmapids)
        {
            jsonArray.add(str);
        }


        return SbLibrary.getResponseObj(true,"All gmapids",jsonArray);
    }

    public long getNumberOfClients(String username) {


        return clientRepo.countBySrepresentative(username);
    }

    public List<Client> getMySuspects(String username) {

        return clientRepo.findByStatusAndSadderAndSrepresentativeIsNull(ClientStatusType.SUSPECT,username);
    }

    public JSONObject getPipelineClients() {


        User curUser = SbLibrary.getCurrentUser().getUser();

        List<ClientTaskWrapper> clientTaskWrappers = null;
        try
        {
            if (curUser.getRoles().equals("BM"))
            {

                List<BigInteger> ids = clientRepo.findAllIds();
                List<BigInteger> idsSortedBytask = clientRepo.findAllIdsSortedByNearestDuetask();

                List<Long> clientIds = new ArrayList<>();

                for(BigInteger x:idsSortedBytask)
                {
                    clientIds.add(x.longValue());
                }

                for (BigInteger x : ids){
                    if (!idsSortedBytask.contains(x))
                        clientIds.add(x.longValue());
                }

				clientTaskWrappers = new ArrayList<>();

				for(Long id : clientIds)
				{
					Client c = clientRepo.findOne(id);
					Task t = null;
					try {
						t = taskService.taskRepo.closestTask(c.getId());
					}catch(Exception ex)
					{
						System.err.println(ex.getMessage());
					}


					if(c != null && t!=null)
					{
						clientTaskWrappers.add(new ClientTaskWrapper(c,t));
					}
				}
            } else
            {

                List<BigInteger> allClientIds = clientRepo.findAllMyClientIds(curUser.getUsername());
                List<BigInteger> clientIdsSortedByTask = clientRepo.findMyClientIdsSortedByNearestDuetask(curUser.getUsername());

                List<Long> clientIds = new ArrayList<>();

                for(BigInteger x:clientIdsSortedByTask)
                {
                    clientIds.add(x.longValue());
                }

                for (BigInteger x : allClientIds){
                    if (!clientIdsSortedByTask.contains(x))
                        clientIds.add(x.longValue());
                }


                clientTaskWrappers = new ArrayList<>();

				for(Long id : clientIds)
				{
					Client c = clientRepo.findOne(id);
					Task t = taskService.taskRepo.closestTask(c.getId());

//					clientTaskWrappers.add(new ClientTaskWrapper(c,t));


					if(c != null && t!=null)
					{
						clientTaskWrappers.add(new ClientTaskWrapper(c,t));
					}
				}

//                System.out.println(clients);

//                for(Client client:clients )
//                {
//                    if(!client.isCandelegate()) {
//                        clients.remove(client);
//                    }
//                }



            }

        } catch (DataAccessException daEx)
        {
            return SbLibrary.getResponseObj(false, daEx.getMessage());
        }



        return SbLibrary.getResponseObj(true, "All Clients of " + curUser.getUsername(), clientTaskWrappers);

    }
}
