package com.worksap.stm.sb.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.User;
import com.worksap.stm.sb.entity.CurrentUser;
import com.worksap.stm.sb.entity.UserEntity;
import com.worksap.stm.sb.service.UserService;

@Service
public class CurrentUserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public CurrentUserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    public CurrentUser loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = null;
		try {
			user = userService.getUserByUsername(username);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if (user == null) {
        	throw new UsernameNotFoundException(String.format("User with id=%s was not found", username));
        }
       
   
        return new CurrentUser(user);
    }

}
