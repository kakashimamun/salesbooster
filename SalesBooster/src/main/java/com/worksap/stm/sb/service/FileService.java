package com.worksap.stm.sb.service;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.worksap.stm.sb.domain.File;
import com.worksap.stm.sb.repo.FileRepo;
import com.worksap.stm.sb.utils.SbLibrary;



@Service
public class FileService {

	@Value("${file.upload.path}")
	private String RELATIVE_FILE_PATH;
	
	
	
	@Autowired
	FileRepo fileRepo;
	
	
	public File saveFileToFolder(MultipartFile file,HttpServletRequest request) throws IOException
	{
		String fileName = file.getOriginalFilename();
        
        String fileExt = FilenameUtils.getExtension(fileName);    
        String fileBaseName = FilenameUtils.getBaseName(fileName);           
        String realFilePath = request.getServletContext().getRealPath(RELATIVE_FILE_PATH);
        

        String filePath = realFilePath+fileName;
        
        
        byte[] bytes = file.getBytes();

        BufferedOutputStream buffStream = 
                new BufferedOutputStream(new FileOutputStream(new java.io.File(filePath)));
        buffStream.write(bytes);
        buffStream.close();              
                    

        File fileEntity = new File();
        
        fileEntity.setExt(fileExt);
        fileEntity.setLink(RELATIVE_FILE_PATH+fileName);
        fileEntity.setName(fileBaseName);
        fileEntity.setTimestamp(new Timestamp(System.currentTimeMillis()));
        fileEntity.setUploader((String)request.getSession().getAttribute("username"));
        
        
        
        return fileEntity;
	}
	
	
	public int saveFileToDB(File file) throws IOException
	{
		File savedFile =  fileRepo.save(file);
		
		if(savedFile == null)
			return 0;
		
		return (int) savedFile.getId();
	}
	
	
	
	public String getLink(File f,HttpServletRequest request)
	{
		
//		System.err.println(f.getId());
//		
//		System.err.println(fileRepo.findLinkById((long)f.getId()));
		
		return SbLibrary.getURLWithContextPath(request)+fileRepo.findLinkById(f.getId());
		
	
	}
	
	
}
