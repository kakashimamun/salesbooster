package com.worksap.stm.sb.service;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.IndustryPerformance;
import com.worksap.stm.sb.domain.Proposal;
import com.worksap.stm.sb.domain.embededid.IndustryPerformanceId;
import com.worksap.stm.sb.repo.IndsutryPerformanceRepo;
import com.worksap.stm.sb.type.ClientStatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mamun on 13/1/2016.
 */


@Service
public class IndustryPerformanceService {

    @Autowired
    IndsutryPerformanceRepo industryPerformanceRepo;


    @Autowired
    ClientService clientService;


    public List<IndustryPerformance> getPerformanceByIndustry(String industry)
    {
        List<IndustryPerformance> industryPerformances = industryPerformanceRepo.findByIndustry(industry);


        return industryPerformances;


    }

    public void proposalWon(Proposal proposal)
    {

        Client client = (Client) clientService.getClientById(proposal.getClientid()).get("data");

        IndustryPerformanceId id = new IndustryPerformanceId(proposal.getSalesrep(),client.getIndustry());

        IndustryPerformance ip = industryPerformanceRepo.findOne(id);


        if(ip!=null)
        {        ip.setNoofdeals(ip.getNoofdeals()+1);
                ip.setAmount((long) (ip.getAmount()+proposal.getDiscount()));
        }else
        {
            ip = new IndustryPerformance();
            ip.setId(id);
            ip.setNoofdeals(1);
            ip.setAmount((long) proposal.getDiscount());
        }


        industryPerformanceRepo.save(ip);
    }


}
