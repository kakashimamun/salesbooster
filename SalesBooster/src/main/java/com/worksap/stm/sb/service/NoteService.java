package com.worksap.stm.sb.service;

import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.Note;
import com.worksap.stm.sb.repo.ClientRepo;
import com.worksap.stm.sb.repo.NoteRepo;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class NoteService
{

	@Autowired
	NoteRepo notesRepo;

	@Autowired
	ClientRepo clientRepo;

	@Autowired
	NotificationService notificationService;

	private void assignClientName(List<Note> notes)
	{
		for (Note note : notes)
		{
			if (note.getClientid() != 0)
			{
				Client client = clientRepo.findOne(note.getClientid());
				note.clientname = client.getName();
			}
		}
	}

	public JSONObject getTutorials()
	{
		JSONObject obj = null;
		List<Note> notes = null;

		try
		{
			notes = (List<Note>) notesRepo.findByClientid(0);

		} catch (Exception ex)
		{
			obj = SbLibrary.getResponseObj(false, "Couldn't fetch notes.");
		}

		assignClientName(notes);

		obj = SbLibrary.getResponseObj(true, "All available notes", notes);

		return obj;
	}

	public JSONObject getClientNotes(long clientid)
	{
		JSONObject obj = null;
		List<Note> notes = null;

		try
		{
			notes = (List<Note>) notesRepo.findByClientid(clientid);

		} catch (Exception ex)
		{
			obj = SbLibrary.getResponseObj(false, "Couldn't fetch notes.");
		}

		if (notes != null)
			assignClientName(notes);

		obj = SbLibrary.getResponseObj(true, "All available notes", notes);

		return obj;
	}

	public JSONObject createNote(Note note)
	{
		Note addedNote = null;

		try
		{
			note.setText(StringEscapeUtils.escapeHtml4(note.getText()));

			addedNote = notesRepo.save(note);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}


		if(addedNote.getClientid() == 0)
		{

			notificationService.notifyAllSalesRep("New Tutorials!!","A new Tutorials has been created titled "+addedNote.getHeading(),0,"/tutorials");
		}

		return SbLibrary.getResponseObj(true, "A new note has been created", addedNote);
	}

	public JSONObject deleteNote(long noteid)
	{
		notesRepo.delete(noteid);

		return SbLibrary.getResponseObj(true, "Note " + noteid + " deleted");
	}

}
