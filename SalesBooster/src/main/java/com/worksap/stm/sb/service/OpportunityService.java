package com.worksap.stm.sb.service;

import java.util.List;

import com.worksap.stm.sb.RequirementInfo;
import com.worksap.stm.sb.domain.Client;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.Opportunity;
import com.worksap.stm.sb.domain.Product;
import com.worksap.stm.sb.domain.Requirement;
import com.worksap.stm.sb.repo.OpportunityRepo;

@Service
public class OpportunityService {

    @Autowired
    OpportunityRepo opportunityRepo;

    @Autowired
    ProductConfigService productConfigService;


    public Opportunity create(Requirement req, RequirementInfo requirementInfo)
    {

        JSONObject cfg = productConfigService.getConfigurations();

        float pm = (float) cfg.get("Profit Margin");
        float nightExtra = (float)cfg.get("Night Shift Extra");
        float maxDis = (float) cfg.get("Maximum Discount");
        long discount = (long) cfg.get("Cleaners");
        long perCostCleaner = (long) cfg.get("Per Cost Cleaner");


        Opportunity op = new Opportunity();

        op.setProduct(req.getProduct());

        op.setRequired(req.isRequired());

        op.setShift(req.getShift());

        op.setArea(req.getArea());
        op.setWeek(req.getWeek());

        op.setTimesinshift(req.getTimesinshift());


        float perCleanCost = req.getArea()*req.getProduct().getVar()+req.getProduct().getFixed();

        float perShiftCost = req.getTimesinshift()*perCleanCost;


        float perDayCost = 0;

        if(req.getShift().isDay())
        {
            perDayCost += perShiftCost;
        }

        if(req.getShift().isNight())
        {
            perDayCost += perShiftCost+perShiftCost*nightExtra;
        }


        float perWeekCost = 0;

        if(req.getWeek().isSunday())
        {
            perWeekCost += perDayCost;
        }
        if(req.getWeek().isMonday())
        {
            perWeekCost += perDayCost;
        }

        if(req.getWeek().isTuesday())
        {
            perWeekCost += perDayCost;
        }
        if(req.getWeek().isWednesday())
        {
            perWeekCost += perDayCost;
        }

        if(req.getWeek().isThursday())
        {
            perWeekCost += perDayCost;
        }

        if(req.getWeek().isFriday())
        {
            perWeekCost += perDayCost;
        }

        if(req.getWeek().isSaturday())
        {
            perWeekCost += perDayCost;
        }

        float totalCost = perWeekCost*requirementInfo.getWeeks();

        op.setCost(totalCost);

        op.setPerdaycost(perDayCost);


        return opportunityRepo.save(op);
    }

    public Opportunity updateOpp(Opportunity op, long duration)
    {

        JSONObject cfg = productConfigService.getConfigurations();

        float pm = (float) cfg.get("Profit Margin");
        float nightExtra = (float)cfg.get("Night Shift Extra");
        float maxDis = (float) cfg.get("Maximum Discount");
        long discount = (long) cfg.get("Cleaners");
        long perCostCleaner = (long) cfg.get("Per Cost Cleaner");


        float perCleanCost = op.getArea()*op.getProduct().getVar()+op.getProduct().getFixed();

        float perShiftCost = op.getTimesinshift()*perCleanCost;


        float perDayCost = 0;

        if(op.getShift().isDay())
        {
            perDayCost += perShiftCost;
        }

        if(op.getShift().isNight())
        {
            perDayCost += perShiftCost+perShiftCost*nightExtra;
        }


        float perWeekCost = 0;

        if(op.getWeek().isSunday())
        {
            perWeekCost += perDayCost;
        }
        if(op.getWeek().isMonday())
        {
            perWeekCost += perDayCost;
        }

        if(op.getWeek().isTuesday())
        {
            perWeekCost += perDayCost;
        }
        if(op.getWeek().isWednesday())
        {
            perWeekCost += perDayCost;
        }

        if(op.getWeek().isThursday())
        {
            perWeekCost += perDayCost;
        }

        if(op.getWeek().isFriday())
        {
            perWeekCost += perDayCost;
        }

        if(op.getWeek().isSaturday())
        {
            perWeekCost += perDayCost;
        }

        float totalCost = perWeekCost*duration;

        op.setCost(totalCost);

        return opportunityRepo.save(op);
    }

    public void delete(long id) {
        opportunityRepo.delete(id);

    }

    public void delete(Opportunity op) {
        opportunityRepo.delete(op.getId());

    }


    public void delete(List<Opportunity> opList) {
        opportunityRepo.delete(opList);

    }

    public List<Opportunity> getAll() {

        return (List<Opportunity>) opportunityRepo.findAll();
    }
}
