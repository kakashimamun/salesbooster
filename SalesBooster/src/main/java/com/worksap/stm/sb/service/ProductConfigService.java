package com.worksap.stm.sb.service;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.ProductConfig;
import com.worksap.stm.sb.repo.ProductConfigRepo;

@Service
public class ProductConfigService
{

	@Autowired
	ProductConfigRepo productConfigRepo;

	public Iterable<ProductConfig> getAllConfig()
	{

		return productConfigRepo.findAll();
	}

	public ProductConfig update(ProductConfig pc)
	{
		// TODO Auto-generated method stub

		return productConfigRepo.save(pc);
	}

	public JSONObject getConfigurations()
	{
		return getConfigObj(this.getAllConfig());

	}

	private JSONObject getConfigObj(Iterable<ProductConfig> cfgs)
	{
		JSONObject configs = new JSONObject();
		for(ProductConfig pc: cfgs)
		{
			if(pc.getName().equals("Maximum Discount") || pc.getName().equals("Night Shift Extra")|| pc.getName().equals("Profit Margin") )
			{

				long value = Long.parseLong(pc.getValue());

				float percentage = (float) value/100;

                configs.put(pc.getName(),percentage);

			}else if(pc.getName().equals("Cleaners") || pc.getName().equals("Per Cost Cleaner")|| pc.getName().equals("Loyalty Point Threshold") )
            {
                long value = Long.parseLong(pc.getValue());

                configs.put(pc.getName(), value);

            }else
            {
                configs.put(pc.getName(), pc.getValue());

            }

		}
		
		return configs;
	}

	public Iterable<ProductConfig> update(List<ProductConfig> configs) {

		return  productConfigRepo.save(configs);
	}
}
