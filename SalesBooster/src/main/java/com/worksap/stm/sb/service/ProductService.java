package com.worksap.stm.sb.service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.plaf.basic.BasicTreeUI.TreeHomeAction;

import com.worksap.stm.sb.domain.Opportunity;
import com.worksap.stm.sb.domain.Proposal;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.Product;
import com.worksap.stm.sb.domain.Requirement;
import com.worksap.stm.sb.domain.wrapper.ProductWrapper;
import com.worksap.stm.sb.repo.ProductRepo;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class ProductService
{

	@Autowired
	ProductRepo productRepo;
	
	@Autowired
	ProductRelationService productRelationService;



	public List<String> getDistinctCategories() throws Exception
	{
		return productRepo.findDistinctSurfaces();
	}

	public List<String> getTasks(String surface) throws Exception
	{
		return productRepo.findTasksBySurface(surface);
	}

	public JSONObject createProduct(Product product)
	{
		// TODO Auto-generated method stub

		Product p = null;
		try
		{
			p = productRepo.save(product);
		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "New Service Created", p);

	}

	public JSONObject getAllServices()
	{
		List<Product> products;

		products = (List<Product>) productRepo.findAll();

		return SbLibrary.getResponseObj(true, "All services", products);
	}

	public JSONObject deleteService(long id)
	{
		// TODO Auto-generated method stub

		try
		{
			productRepo.delete(id);
		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Service Deleted");
	}

	public JSONObject editProduct(Product product)
	{
		// TODO Auto-generated method stub

		try
		{

			product = productRepo.save(product);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage(), product);
		}

		return SbLibrary.getResponseObj(true, "Service Edited", product);

	}

	public JSONObject getServiceByCatAndTask(String cat, String task)
	{
		Product product = null;

		try
		{
			product = productRepo.findBySurfaceAndTask(cat, task);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Product found", product);
	}

	private List<Product> getSingleTagMatchedService(String tag)
	{
		List<Product> matchedProducts = productRepo.findByTagsContaining(tag);

		return matchedProducts;
	}

	public static <K, V extends Comparable<V>> TreeMap<Long, ProductWrapper> sortByPriority(final TreeMap<Long, ProductWrapper> map)
	{
		Comparator<Long> valueComparator = new Comparator<Long>()
		{
			public int compare(Long k1, Long k2)
			{
				int compare = (map.get(k2)).getProduct().getPriority().compareTo((map.get(k1)).getProduct().getPriority());
				if (compare == 0)
					return 1;
				else
					return compare;
			}
		};

		TreeMap<Long, ProductWrapper> sortedByValues = new TreeMap<Long, ProductWrapper>(valueComparator);
		sortedByValues.putAll(map);

		return sortedByValues;
	}

	public JSONObject getTagMatchedServices(String tagStr, List<Requirement> reqs)
	{
		TreeMap<Long, ProductWrapper> products = new TreeMap<Long, ProductWrapper>();

		if (tagStr == null || tagStr.length() == 0)
			return SbLibrary.getResponseObj(false, "Please Set Tags first", null);

		String[] tags = tagStr.split(",");

		for (String tag : tags)
		{
			tag = tag.trim().toLowerCase();

			List<Product> matchedProducts = this.getSingleTagMatchedService(tag);

			for (Product p : matchedProducts)
			{
				if (!products.containsKey(p.getId()))
				{
					products.put(p.getId(), new ProductWrapper(tag, p));

				} else
				{
					ProductWrapper pw = products.get(p.getId());
					pw.setMsg(pw.getMsg() + ", " + tag);
				}
			}
		}

		products = this.removeAlreadyAdded(products, reqs);

		TreeMap<Long, ProductWrapper> matchedProducts = sortByPriority(products);

		return SbLibrary.getResponseObj(true, "Recommended products by tag", matchedProducts.values());

	}

	private TreeMap<Long, ProductWrapper> removeAlreadyAdded(TreeMap<Long, ProductWrapper> productList, List<Requirement> reqs)
	{
		for (Requirement req : reqs)
		{
			long id = req.getProduct().getId();

			if (productList.containsKey(id))
			{
				productList.remove(id);
			}
		}

		return productList;
	}

	public JSONObject getPurchaseMatchedServices(List<Requirement> reqs)
	{

		TreeMap<Long, ProductWrapper> products = new TreeMap<Long, ProductWrapper>();

		if (reqs.isEmpty())
			return SbLibrary.getResponseObj(false, "There is no requirement", null);

		for (Requirement req : reqs)
		{
			List<ProductWrapper> list = productRelationService.findRelatedProductsTop(req.getProduct(),10);

			for (ProductWrapper p : list)
			{
				if (!products.containsKey(p.getProduct().getId()))
				{
					products.put(p.getProduct().getId(), p);
				}
			}

		}

		 products = this.removeAlreadyAdded(products, reqs);

		TreeMap<Long, ProductWrapper> matchedProducts = sortByPriority(products);

		return SbLibrary.getResponseObj(true, "Recommended products purchase record", matchedProducts.values());

	}

	public JSONObject getSimilarServices(List<Requirement> reqs)
	{

		TreeMap<Long, ProductWrapper> products = new TreeMap<Long, ProductWrapper>();

		if (reqs.isEmpty())
			return SbLibrary.getResponseObj(false, "Please Set Tags first", null);

		for (Requirement req : reqs)
		{

			String surfaceType = req.getProduct().getSurface();
			List<Product> list = productRepo.findBySurface(surfaceType);

			for (Product p : list)
			{
				if (!products.containsKey(p.getId()))
				{
					products.put(p.getId(), new ProductWrapper(surfaceType, p));
				}
			}

		}

		 products = this.removeAlreadyAdded(products, reqs);

		TreeMap<Long, ProductWrapper> matchedProducts = sortByPriority(products);

		return SbLibrary.getResponseObj(true, "Recommended products by Same Type", matchedProducts.values());

	}


}
