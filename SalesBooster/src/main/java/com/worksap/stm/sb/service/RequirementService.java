package com.worksap.stm.sb.service;

import java.util.List;

import com.worksap.stm.sb.RequirementInfo;
import com.worksap.stm.sb.repo.RequirementInfoRepo;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.Client;
import com.worksap.stm.sb.domain.Requirement;
import com.worksap.stm.sb.repo.RequirementRepo;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class RequirementService
{

	@Autowired
	RequirementRepo requirementRepo;

	@Autowired
	RequirementInfoRepo requirementInfoRepo;

	public JSONObject create(Requirement requirement)
	{

		Requirement newRequirement = null;

		try
		{
			newRequirement = requirementRepo.save(requirement);
		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Requirement Created", newRequirement);

	}

	public JSONObject getRequirementsByClient(long clientid)
	{
		
		Client client = new Client();
		
		client.setId(clientid);
		List<Requirement> requirements = null;
		
		try
		{
			requirements =  requirementRepo.findByClient(client);

		}catch(Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}
		
		return SbLibrary.getResponseObj(true, "Requirements of client:"+clientid,requirements);

	}

	public JSONObject delete(long id)
	{
		try
		{
			requirementRepo.delete(id);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Requirement Deleted");
	}

	public JSONObject update(Requirement requirement)
	{
		// TODO Auto-generated method stub
		
		Requirement newRequirement = null;

		try
		{
			newRequirement = requirementRepo.save(requirement);
		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Requirement Updated", newRequirement);
		
	}

	public JSONObject getById(long reqid)
	{
		Requirement newRequirement = null;

		try
		{
			newRequirement = requirementRepo.findOne(reqid);
		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Fetched requirement:"+reqid, newRequirement);
		
	}


	public JSONObject createReqInfo(RequirementInfo requirementInfo) {


		requirementInfo = requirementInfoRepo.save(requirementInfo);


		return SbLibrary.getResponseObj(true,"Added",requirementInfo);

	}

	public JSONObject getRequirementsInfoByClient(Long id) {


		List<RequirementInfo> requirementInfos = requirementInfoRepo.findByClientid(id);

		return  SbLibrary.getResponseObj(true,"",requirementInfos);

	}

	public JSONObject deleteReqInfo(RequirementInfo requirementInfo) {


		requirementInfoRepo.delete(requirementInfo);

		return SbLibrary.getResponseObj(true,"Deleted",requirementInfo);


	}

	public RequirementInfo getLastRequirementInfoByClientid(long clientid) {


		RequirementInfo requirementInfo = requirementInfoRepo.findFirstByClientidOrderByCreated(clientid);

		return requirementInfo;
	}
}
