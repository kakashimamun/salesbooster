package com.worksap.stm.sb.service;

import com.worksap.stm.sb.utils.SbLibrary;
import org.joda.time.LocalDateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.SalesRecord;
import com.worksap.stm.sb.domain.embededid.SalesRecordId;
import com.worksap.stm.sb.repo.SalesRecordRepo;

import java.util.List;

@Service
public class SalesRecordService
{

	@Autowired
	SalesRecordRepo salesRecordRepo;

	@Autowired
	StatsService statsService;

	public void generateRandom()
	{

		for (;;)
		{
			int monthOfYear = 1 + (int) (Math.random() * ((12 - 1) + 1));
			int yearOfCentury = 1 + (int) (Math.random() * ((16 - 1) + 1));

			SalesRecordId id = new SalesRecordId(monthOfYear, yearOfCentury);

			long targetAmt = 10000 + (int) (Math.random() * ((10000000 - 10000) + 1));

			long diff = 1000 + (int) (Math.random() * ((10000 - 1000) + 1));
			
			float costRand = (float) ((50 +  (Math.random() * ((90 - 50) + 1)))/100);
			
			float OppRand = (float) ((10 +  (Math.random() * ((50 - 10) + 1)))/100);
			
	
			long sales;
			
			if ((int) (Math.random() * ((1 - 0) + 1)) == 0)
			{
				sales = targetAmt - diff;
				
			} else
			{
				sales = targetAmt + diff;
			}
			
			
			
			long opportunity = (long) ((long) sales + sales* OppRand);
			long profit = (long) ((long) sales - sales* costRand);
			

			SalesRecord target = new SalesRecord();

			target.setId(id);
			target.setTarget(targetAmt);
			target.setRevenue(sales);
			target.setProfit(profit);
			target.setOpportunity(opportunity);

			System.err.println(costRand+" "+OppRand);
			
			salesRecordRepo.save(target);
		}

	}




	public JSONObject setTarget(long target)
	{
		SalesRecord salesRecord;


		int month = LocalDateTime.now().getMonthOfYear();
		int year = LocalDateTime.now().getYearOfCentury();


		SalesRecordId id = new SalesRecordId(month,year);

		salesRecord = salesRecordRepo.findOne(id);


		if(salesRecord == null)
		{
			salesRecord = new SalesRecord();
			salesRecord.setId(id);

		}else
		{

		}


		salesRecord.setTarget(target);

		try
		{
			salesRecord = salesRecordRepo.save(salesRecord);
		}
		catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false,ex.getMessage());
		}


		return SbLibrary.getResponseObj(true,"Target Updated",salesRecord);


	}


	private SalesRecordId getThisMonthId()
	{

		int month = LocalDateTime.now().getMonthOfYear();
		int year = LocalDateTime.now().getYearOfCentury();


		SalesRecordId id = new SalesRecordId(month,year);

		return id;
	}

	public SalesRecord updateSalesRecordOfTheMonth()
	{

		SalesRecordId id = this.getThisMonthId();


		SalesRecord salesRecord = salesRecordRepo.findOne(id);

		if(salesRecord  == null)
		{
			salesRecord = new SalesRecord();
			salesRecord.setId(id);
		}

		salesRecord = statsService.setSaleRecordStuff(salesRecord);



		return salesRecordRepo.save(salesRecord);

	}

	public JSONArray getMorrisMonthData() {



		List<SalesRecord> records = salesRecordRepo.findSalesRecordMonthLimit(15);

		JSONArray retArray = new JSONArray();

		for(SalesRecord record:records)
		{
			JSONObject retObj = new JSONObject();

//			String monthStr = String.valueOf(record.getId().getMonthOfYear());
//
//			if(! (record.getId().getMonthOfYear() > 9))
//			{
//				monthStr = "0"+monthStr;
//			}

			retObj.put("month","20"+record.getId().getYearOfCentury()+"-"+record.getId().getMonthOfYear());
			retObj.put("opportunity",record.getOpportunity());
			retObj.put("revenue",record.getRevenue());
			retObj.put("profit",record.getProfit());

			retArray.add(retObj);
		}


		return retArray;
	}

	public JSONArray getMorrisYearData() {

		List<SalesRecord> records = salesRecordRepo.findSalesRecordYear(10);

		JSONArray retArray = new JSONArray();

		for(SalesRecord record:records)
		{
			JSONObject retObj = new JSONObject();

			String yearStr = String.valueOf(record.getId().getYearOfCentury());

			if(! (record.getId().getYearOfCentury() > 9))
			{
				yearStr = "0"+yearStr;
			}



			retObj.put("year","20"+yearStr);
			retObj.put("opportunity",record.getOpportunity());
			retObj.put("revenue",record.getRevenue());
			retObj.put("profit",record.getProfit());

			retArray.add(retObj);
		}


		return retArray;

	}


}
