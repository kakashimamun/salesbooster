package com.worksap.stm.sb.service;

import com.worksap.stm.sb.domain.*;
import com.worksap.stm.sb.domain.embedded.SalesStats;
import com.worksap.stm.sb.type.ProposalStatus;
import com.worksap.stm.sb.utils.SbLibrary;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.repo.EventStatsRepo;
import com.worksap.stm.sb.repo.SalesStatsRepo;
import com.worksap.stm.sb.type.ClientStatusType;
import com.worksap.stm.sb.type.StatsType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class StatsService
{

	
	@Autowired
	SalesStatsRepo salesStatsRepo;
	
	@Autowired
	EventStatsRepo eventStatsRepo;


	@Autowired
	ProposalService proposalService;
	
	
	public SalesStats createSalesStats(Proposal proposal)
	{
		SalesStats salesStats =  null;
		salesStats = this.salesStatsRepo.findByProposalid(proposal.getId());
		if(salesStats == null)
		{
			salesStats = new SalesStats();
		}
		salesStats.setClientid(proposal.getClientid());
		salesStats.setSalesrep(proposal.getSalesrep());
		salesStats.setRevenue(proposal.getDiscountedprice());
		salesStats.setProfit(proposal.getProfit());
		salesStats.setProposalid(proposal.getId());
		salesStats.setProposalstatus(proposal.getStatus());
		salesStats.setType(StatsType.SALES);
		
		return salesStatsRepo.save(salesStats);
	}
	
	public EventStats createEventStats(String salesrep, long clientid, StatsType type, ClientStatusType prestatus)
	{
		
		EventStats eventStats = new EventStats();
		
		eventStats.setClientid(clientid);
		eventStats.setSalesrep(salesrep);
		eventStats.setType(type);
		eventStats.setPrestatus(prestatus);
		
		return eventStatsRepo.save(eventStats);
	}
	public EventStats createEventStats(String salesrep, long clientid, StatsType type )
	{

		EventStats eventStats = new EventStats();

		eventStats.setClientid(clientid);
		eventStats.setSalesrep(salesrep);
		eventStats.setType(type);
		eventStats.setPrestatus(null);

		return eventStatsRepo.save(eventStats);
	}

	
	public void clientStatusChangeEvent(Client old, Client updated)
	{
		
		if(ClientStatusType.ACTIVE.equals(updated.getStatus()))
		{
			this.createEventStats(updated.getSrepresentative(), updated.getId(), StatsType.ACTIVE,old.getStatus());
		}
		else if(ClientStatusType.PROSPECT.equals(updated.getStatus()))
		{
			this.createEventStats(updated.getSrepresentative(), updated.getId(), StatsType.PROSPECT,old.getStatus());

			this.createEventStats(updated.getSadder(), updated.getId(), StatsType.SUSPECT);
		}
		else if(ClientStatusType.EXCLIENT.equals(updated.getStatus()))
		{
			this.createEventStats(updated.getSrepresentative(), updated.getId(), StatsType.EXCLIENT,old.getStatus());
		}
		else if(ClientStatusType.REMOVED.equals(updated.getStatus()))
		{
			if(updated.getSrepresentative() != null)
				this.createEventStats(updated.getSrepresentative(), updated.getId(), StatsType.REMOVED,old.getStatus());
		}
	}


	public void removeSuspectEvent(String srep,long clientid)
	{
		EventStats eventStats = new EventStats();

		eventStats.setClientid(clientid);
		eventStats.setSalesrep(srep);
		eventStats.setType(StatsType.SUSPECT);

		eventStatsRepo.delete(eventStats);
	}



	public SalesRecord setSaleRecordStuff(SalesRecord salesRecord) {

		salesRecord.setOpportunity(salesStatsRepo.totalOppThisMonth());
		salesRecord.setRevenue(salesStatsRepo.totalRevenueThisMonth());
		salesRecord.setProfit(salesStatsRepo.totalProfitThisMonth());

		return salesRecord;
	}

	public JSONObject getCurrentSales() {

		JSONObject curSales = new JSONObject();

		curSales.put("opportunity",salesStatsRepo.totalOppThisMonth());
		curSales.put("revenue",salesStatsRepo.totalRevenueThisMonth());
		curSales.put("profit",salesStatsRepo.totalProfitThisMonth());

		return curSales;
	}

	public List<SalesStats> getLastNWonProposal() {


		return salesStatsRepo.findNByProposalStatus(ProposalStatus.WON.toString(),10);

	}

	public List<SalesStats> getLastNLostProposal() {


		return salesStatsRepo.findNByProposalStatus(ProposalStatus.LOST.toString(),10);

	}

	public List<SalesStats> getLastNProposedProposal() {


		return salesStatsRepo.findNByProposalStatus(ProposalStatus.PROPOSED.toString(),10);

	}

	public List<SalesStats> getLatestNDeals() {

		return salesStatsRepo.findLatestNDeals(15);
	}

	public void generateSalesStats() {


		SalesStats salesStats = new SalesStats();



		long revenue = 100 + (int) (Math.random() * ((10000 - 100) + 1));

		salesStats.setRevenue(revenue);


		float costRand = (float) ((50 +  (Math.random() * ((90 - 50) + 1)))/100);

		salesStats.setProfit(revenue-revenue*costRand);

		salesStats.setType(StatsType.SALES);



		int saId = 1 + (int) (Math.random() * ((5 - 1) + 1));
		int ifid = 1 + (int) (Math.random() * ((3 - 1) + 1));

		String srep = "SA"+saId;

		salesStats.setSalesrep(srep);

		int pid = 40 + (int) (Math.random() * ((400 - 1) + 1));


		salesStats.setProposalid(pid);


		if(ifid%3 == 0)
		{
			salesStats.setProposalstatus(ProposalStatus.WON);
		}else if(ifid%3 == 1)
		{
			salesStats.setProposalstatus(ProposalStatus.LOST);
		}else if(ifid%3 == 2)
		{
			salesStats.setProposalstatus(ProposalStatus.PROPOSED);
		}


		long profit = (long) ((long) revenue - revenue* costRand);


		salesStats.setProfit(profit);

		SalesRecord target = new SalesRecord();

		System.out.println(salesStats);

		salesStatsRepo.save(salesStats);


	}

	public void generateEventStats() {


		EventStats eventStats = new EventStats();


		long revenue = 100 + (int) (Math.random() * ((10000 - 100) + 1));
		float costRand = (float) ((50 +  (Math.random() * ((90 - 50) + 1)))/100);





		int saId = 1 + (int) (Math.random() * ((5 - 1) + 1));

		String srep = "SA"+saId;

		eventStats.setSalesrep(srep);
		eventStats.setClientid(0);



		int ifid = 1 + (int) (Math.random() * ((4 - 1) + 1));


//
		if(ifid%4 == 0)
		{
			eventStats.setType(StatsType.ACTIVE);
		}else if(ifid%4 == 1)
		{
            eventStats.setType(StatsType.PROSPECT);
		}else if(ifid%4 == 2)
		{
            eventStats.setType(StatsType.REMOVED);
		}else if(ifid%4 == 3)
		{
            eventStats.setType(StatsType.EXCLIENT);
		}



        int ifid1 = 1 + (int) (Math.random() * ((5 - 1) + 1));


        if(eventStats.getType().equals(StatsType.REMOVED))
        {
            if(ifid1%5 == 0)
            {
                eventStats.setPrestatus(ClientStatusType.SUSPECT);
            }else if(ifid1%5 == 1)
            {
                eventStats.setPrestatus(ClientStatusType.PROSPECT);
            }else if(ifid1%5 == 2)
            {
                eventStats.setPrestatus(ClientStatusType.PROFILED);
            }else if(ifid1%5 == 3)
            {
                eventStats.setPrestatus(ClientStatusType.PROPOSED);
            }else if(ifid1%5 == 4)
            {
                eventStats.setPrestatus(ClientStatusType.ACTIVE);
            }
        }



		eventStatsRepo.save(eventStats);


	}


    public List<EventStats> getAllEventStats(String username) {

        return eventStatsRepo.findBySalesrep(username);
    }

    public List<EventStats> getAllEventStats( ) {


        return (List<EventStats>) eventStatsRepo.findAll();

    }

    public List<SalesStats> getSalesStats(String username) {

        return salesStatsRepo.findBySalesrep(username);
    }

    public List<SalesStats> getSalesStats( ) {

        return (List<SalesStats>) salesStatsRepo.findAll();
    }

    private JSONObject getStatsFromList(List<EventStats> eventStatses,List<SalesStats> salesStatses){

        int numberOfSuspects = 0;
        int numberOfProspect = 0;

        int clientLost = 0;
        int clientActive = 0;

        int numberOfMissedTask = 0;

        for (EventStats ev : eventStatses) {
            if (ev.getType().equals(StatsType.SUSPECT)) {
                numberOfSuspects++;
            } else if (ev.getType().equals(StatsType.PROSPECT)) {
                numberOfProspect++;
            } else if (ev.getType().equals(StatsType.REMOVED)  || ev.getType().equals(StatsType.EXCLIENT)) {
                clientLost++;
            } else if (ev.getType().equals(StatsType.ACTIVE)) {
                clientActive++;
            } else if (ev.getType().equals(StatsType.MISSED)) {
                numberOfMissedTask++;
            }
        }

        int numberOfDealsWon = 0;
        int numberOfDealsLost = 0;

        int amountWon = 0;
        int amountLost = 0;

        int profitEarned = 0;
        int profitLost = 0;

		int dealProposed = 0;

        for (SalesStats sa : salesStatses) {
            if (sa.getProposalstatus().equals(ProposalStatus.WON)) {
                numberOfDealsWon++;
                amountWon += sa.getRevenue();
                profitEarned += sa.getProfit();
            }

            if (sa.getProposalstatus().equals(ProposalStatus.LOST)) {
                numberOfDealsLost++;
                amountLost += sa.getRevenue();
                profitLost += sa.getProfit();
            }

			if (sa.getProposalstatus().equals(ProposalStatus.PROPOSED)) {
				dealProposed++;
            }
        }


        float clientConversionRatio = 0;
		if(numberOfProspect !=0)
			clientConversionRatio= (float) ((float)clientActive /(float) numberOfProspect) * 100;
        float dealWinRatio = 0 ;
		if(numberOfDealsLost+numberOfDealsWon !=0)
		dealWinRatio =  ((float) (numberOfDealsWon) / ((float)(numberOfDealsWon+numberOfDealsLost)))* 100;

		float avgDealWon = 0;
		float avgProfit = 0;

		if(numberOfDealsWon != 0)
		{
			avgDealWon = (amountWon / numberOfDealsWon);
			avgProfit = (profitEarned / numberOfDealsWon);
		}


        JSONObject retObj = new JSONObject();

        retObj.put("numberOfDealsWon", numberOfDealsWon);
        retObj.put("numberOfDealsLost", numberOfDealsLost);
        retObj.put("numberOfSuspects", numberOfSuspects);
        retObj.put("numberOfProspect", numberOfProspect);
        retObj.put("clientLost", clientLost);
        retObj.put("clientActive", clientActive);
        retObj.put("amountWon", amountWon);
        retObj.put("amountLost", amountLost);
        retObj.put("profitEarned", profitEarned);
        retObj.put("profitLost", profitLost);
        retObj.put("clientConversionRatio", clientConversionRatio);
        retObj.put("dealWinRatio", dealWinRatio);
        retObj.put("avgDealWon", avgDealWon);
        retObj.put("avgProfit", avgProfit);
        retObj.put("numberOfMissedTask", numberOfMissedTask);

        return retObj;
    }

    public JSONObject getStats(String username) {


        List<EventStats> eventStatses;
        eventStatses = this.getAllEventStats(username);

        List<SalesStats> salesStatses;
        salesStatses = this.getSalesStats(username);

        return getStatsFromList(eventStatses,salesStatses);

    }

    public JSONObject getStats( ) {


        List<EventStats> eventStatses;
        eventStatses = this.getAllEventStats();

        List<SalesStats> salesStatses;
        salesStatses = this.getSalesStats();



        return getStatsFromList(eventStatses,salesStatses);


    }

    public JSONObject getProposalStats(String username) {


        List<SalesStats> salesStatses = salesStatsRepo.findBySalesrep(username);


        JSONObject retObj = new JSONObject();

        retObj.put("salesStatses",salesStatses);
        return retObj;

    }

    public JSONObject getProposalStats( ) {


        List<SalesStats> salesStatses = (List<SalesStats>) salesStatsRepo.findAll();


        JSONObject retObj = new JSONObject();

        retObj.put("salesStatses",salesStatses);
        return retObj;

    }

	public JSONObject getRevenueByProduct() {


		List<Proposal> proposals = proposalService.getWonDeals();


		long totalCost = 0;

        JSONObject obj;

		HashMap<String,Float> proArray = new HashMap<>();

		for(Proposal p:proposals)
		{
			for(Opportunity op:p.getOpportunities())
			{
				totalCost += op.getCost();


				if(proArray.containsKey(op.getProduct().getProductName()))
				{


					float cost = proArray.get(op.getProduct().getProductName());

                    proArray.put(op.getProduct().getProductName(),cost+op.getCost());

				}else
				{

                    proArray.put(op.getProduct().getProductName(),op.getCost());

				}


			}
		}


        System.out.println(proArray);



        JSONArray array = new JSONArray();

        for(Map.Entry<String,Float> en:proArray.entrySet())
        {


            obj = new JSONObject();



            obj.put("label",en.getKey());
            obj.put("value",String.format("%.02f",en.getValue()/totalCost*100));

            array.add(obj);

        }




        return SbLibrary.getResponseObj(true,"Product Status",array);
	}
}
