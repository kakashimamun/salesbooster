package com.worksap.stm.sb.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.aspectj.weaver.ast.HasAnnotation;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.utils.SbLibrary;


@Service
public class SuspectSearchService {

	public ArrayList<HashMap<String, String>> getCompanies(String url) {

		Connection.Response response = null;
		Document doc = null;
	

		 try {
	         response = Jsoup.connect(url)
	            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
	            .timeout(10000)
	            .execute();
	      } catch (IOException e) {
	    	  e.printStackTrace();
	         System.out.println("io - "+e);
	      }
		
		 
		 if(response.statusCode() == 301 || response.statusCode() == 404) return null;
		 
	
		 try 
		 {
			 
			doc = response.parse();
			 return getCompany(doc);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Parsing Failed");
		}
		return null;
		 
		 
		
		
	}

	
	
	
	private ArrayList<HashMap<String, String>> getListOfCompanies(Elements list)
	{
		
		ArrayList<HashMap<String, String>>  companies = new ArrayList<HashMap<String,String>>();
		
		for(Element table:list)
		{
			
			HashMap<String, String> newCompany = new HashMap<String, String>();
			
			String companyName = StringEscapeUtils.unescapeHtml4(table.select(".listing_company_name").html());
//			System.out.println(companyName);
			
			
			
			if(companyName.isEmpty()) continue;
			
			newCompany.put("name", companyName);
			
			String address = StringEscapeUtils.unescapeHtml4(table.select("table tbody table tbody #tr_address .displayhide").html());
//			System.out.println(address.replaceAll("\n", ","));

			newCompany.put("address", address.replaceAll("\n", ","));
			
			String types = StringEscapeUtils.unescapeHtml4(table.select("table tbody table tbody #listing_tr_category [itemprop]").html());
//			System.out.println(types.replaceAll("<b>|</b>", "").replaceAll("\n", ","));

			newCompany.put("types", types.replaceAll("<b>|</b>", "").replaceAll("\n", ","));
			
			
			String phoneNo = StringEscapeUtils.unescapeHtml4(table.select(".tr_contact_listing  [itemprop=telephone]").html());
//			System.out.println(phoneNo);
			
			if(!phoneNo.isEmpty())
				newCompany.put("phone", phoneNo);
			
			
			String website = StringEscapeUtils.unescapeHtml4(table.select(".tr_contact_listing  [itemprop=url] a[href]").html());
//			System.out.println(website);////
			
			
			if(!website.isEmpty())
				newCompany.put("website", SbLibrary.ensureHasProtocol(website,"http://"));
			
			
			
//			System.out.println(newCompany);
			
			companies.add(newCompany);
			
		}
		
		
		return companies;
	}
	
	
	
	
	public  ArrayList<HashMap<String, String>>  getCompany(Document doc)
	{
		Elements free_listing = doc.select(".free_listing table tbody");
		Elements company_profile = doc.select(".company_profile table tbody");
		
		
		
		ArrayList<HashMap<String, String>>  companies_f = getListOfCompanies(free_listing);
		ArrayList<HashMap<String, String>>  companies_c = getListOfCompanies(company_profile);
		ArrayList<HashMap<String, String>>  companies =  new ArrayList<HashMap<String,String>>();
		
		companies.addAll(companies_c);
		companies.addAll(companies_f);
		

	
		return companies;

	}
		
	
}
