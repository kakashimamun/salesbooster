package com.worksap.stm.sb.service;

import java.util.ArrayList;
import java.util.List;

import com.worksap.stm.sb.domain.*;
import com.worksap.stm.sb.domain.embededid.SalesRecordId;
import com.worksap.stm.sb.type.*;
import org.joda.time.LocalDateTime;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.domain.wrapper.TaskEventWrapper;
import com.worksap.stm.sb.repo.TaskRepo;
import com.worksap.stm.sb.utils.SbLibrary;

@Service
public class TaskService
{

	@Autowired
	TaskRepo taskRepo;

	@Autowired
	NotificationService notificationService;

	@Autowired
	TimelineService timelineService;

	@Autowired
	ClientService clientService;

	@Autowired
	StatsService statsService;


	private Task getSuitableTaskTime(Task task)
	{
		LocalDateTime start = LocalDateTime.now().plusHours(4);
		LocalDateTime end = start.plusHours(4);

		if(start.getDayOfYear() != end.getDayOfYear())
		{
			start.plusHours(20);
            end.plusHours(20);
		}


        task.setStart(start);
        task.setEnd(end);


        return task;
	}


	private Task automatedUnscheduledTask(Client client)
	{
		Task automatedTask = new Task();
		automatedTask.setStatus(TaskStatus.DUE);
		automatedTask = this.getSuitableTaskTime(automatedTask);

		automatedTask.setSrepresentative(client.getSrepresentative());
		automatedTask.setClientid(client.getId());
		automatedTask.setClientstatus(client.getStatus());

		ClientStatusType clientstatus = client.getStatus();

		if (clientstatus.toString().equals("SUSPECT"))
		{
			automatedTask.setType(TaskType.CALL);
			automatedTask.setTitle("Collect information and Qualify " + client.getName()+" as Prospect");

		} else if (clientstatus.toString().equals("PROSPECT"))
		{
			automatedTask.setType(TaskType.MEET);
			automatedTask.setTitle("Collect Requirments of " + client.getName());

		} else if (clientstatus.toString().equals("PROFILED"))
		{
			automatedTask.setType(TaskType.PROPOSE);
			automatedTask.setTitle("Propose cleaning porposal to " + client.getName());

		} else if (clientstatus.toString().equals("PROPOSED"))
		{
			automatedTask.setType(TaskType.CLOSE);
			automatedTask.setTitle("Negotiate the price with " + client.getName());

			// } else if (clientstatus.toString().equals("ACTIVE"))
			// {
			// automatedTask.setType(TaskType.FOLLOWUP);
			// automatedTask.setTitle("Follow Up " + client.getName());

		} else if (clientstatus.toString().equals("EXCLIENT"))
		{
			automatedTask.setType(TaskType.FOLLOWUP);
			automatedTask.setTitle("Send Offer and other marketing advertisements " + client.getName());

		}
		// else
		// {
		// automatedTask.setType(TaskType.FOLLOWUP);
		// automatedTask.setTitle("Check if he needs Service Now " +
		// client.getName());
		// }

		return automatedTask;
	}

	public Task createAutomatedTask(Client client)
	{
		Task newTask = automatedUnscheduledTask(client);

		if (newTask.getType() != null)
		{
			Task createdTask = taskRepo.save(newTask);

			notificationService.create("New Task!!", "You have a new Task titled \'" + createdTask.getTitle()
					+ "\'. Please reschedule the task at you preferable time.", client.getId(), client.getSrepresentative(), "/showCalendar");

			return createdTask;
		}

		return null;
	}

	public void setTaskCompleted(long clientid, ClientStatusType clientstatus)
	{
		// taskRepo.setTaskCompleted(clientid,clientstatus);

		List<Task> tasks = taskRepo.findByClientidAndClientstatus(clientid, clientstatus);

		for (Task t : tasks)
		{
			t.setStatus(TaskStatus.COMPLETED);
		}

		taskRepo.save(tasks);
	}

	public void setTaskCompleted(long clientid, TaskType taskType)
	{
		// taskRepo.setTaskCompleted(clientid,clientstatus);

		List<Task> tasks = taskRepo.findByClientidAndType(clientid, taskType);

		for (Task t : tasks)
		{
			t.setStatus(TaskStatus.COMPLETED);
		}

		taskRepo.save(tasks);
	}

	public void changeSrepOfDueTasks(long clientid, String newsrep)
	{
		taskRepo.changeSrepOfDueTasks(newsrep, clientid, TaskStatus.DUE.toString());
	}

//	public Task create(String title, String text, long clientid, ClientStatusType clientstatus, String srepresentative, LocalDateTime start,
//			LocalDateTime end, TaskType type, TaskStatus completed)
//	{
//		Task newTask = new Task(title, clientid, clientstatus, srepresentative, start, end, type, completed,false);
//
//		Task createdTask = taskRepo.save(newTask);
//
//		timelineService.create(clientid, srepresentative, "Task Created to " + createdTask.getType(),
//				"A task was created titled \'" + createdTask.getTitle() + "\'");
//
//		return createdTask;
//	}
//	public Task create(String title, String text, long clientid, ClientStatusType clientstatus, String srepresentative, LocalDateTime start,
//			LocalDateTime end, TaskType type, TaskStatus completed,boolean fixed)
//	{
//		Task newTask = new Task(title, clientid, clientstatus, srepresentative, start, end, type, completed,fixed);
//
//		Task createdTask = taskRepo.save(newTask);
//
//		timelineService.create(clientid, srepresentative, "Task Created to " + createdTask.getType(),
//				"A task was created titled \'" + createdTask.getTitle() + "\'");
//
//		return createdTask;
//	}

	public JSONObject create(Task t)
	{
		Task task = null;
		TaskEventWrapper taskEvent = null;

		User curUser = SbLibrary.getCurrentUser().getUser();


		if(LocalDateTime.now().compareTo(t.getEnd())>0)
		{
			return SbLibrary.getResponseObj(false,"You can't create a task before time");
		}


		try
		{
			task = taskRepo.save(t);
			taskEvent = new TaskEventWrapper(task);
			if(task.isTeammeeting())
			{
				taskEvent.setEditable(t.isTeammeeting());
			}

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		if (task != null && !task.isTeammeeting())
		{
			timelineService.create(task.getClientid(), task.getSrepresentative(), "Task Created to " + task.getType(), "A task was created titled \'"
					+ task.getTitle() + "\' by " + curUser.getName());
		}else if(task.isTeammeeting())
		{
			notificationService.notifyAllSalesRep("Team Meeting","A Sales Team meeting is called by the Manager titled:"+task.getTitle(),0,"/showCalendar");
		}


		return SbLibrary.getResponseObj(true, "Task Created", taskEvent);
	}

	public JSONObject getTasksByClient(long clientid)
	{
		List<Task> tasks = null;

		try
		{
			tasks = taskRepo.findByClientid(clientid);

		} catch (Exception ex)
		{
			SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "All Tasks of client", tasks);
	}

	public List<TaskEventWrapper> getTaskByUser(String user) throws Exception
	{
		List<Task> tasks = null;
		List<TaskEventWrapper> tasksEvents = new ArrayList<TaskEventWrapper>();

		tasks = taskRepo.findAllTaskForSrepresentative(user);

		for (Task t : tasks)
		{
			TaskEventWrapper tw = new TaskEventWrapper(t);

			if(!t.isTeammeeting() && !t.getSrepresentative().equals(user)) {

				if(t.getStatus().equals(TaskStatus.DUE))
				{
					tw.setColor("#e4c343");
				}else
				{
					tw.setColor("#3333cc");
				}

				tw.setTitle("Delegate Task "+tw.getTitle());
			}
			tasksEvents.add(tw);

		}

		return tasksEvents;

	}

	public JSONObject delete(long id)
	{

		try
		{
			taskRepo.delete(id);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Task Deleted");
	}

	public JSONObject editTask(Task task) {

		Task updatedTask = null;


		try {
			updatedTask = taskRepo.save(task);

		} catch (Exception ex) {
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Edited Task", new TaskEventWrapper(updatedTask));

	}

	public JSONObject countDueTaskToday()
	{

		String user = SbLibrary.getCurrentUser().getUsername();

		int count = 0;
		try
		{
			count = taskRepo.countDueTaskToday(user);

		} catch (Exception ex)
		{
			return SbLibrary.getResponseObj(false, ex.getMessage());
		}

		return SbLibrary.getResponseObj(true, "Number of Tasks", count);

	}

	public Task createAutomatedTask(Proposal proposal)
	{

		Client client = (Client) clientService.getClientById(proposal.getClientid()).get("data");

		Task newTask = automatedUnscheduledTask(proposal, client);

		Task createdTask = taskRepo.save(newTask);

		notificationService.create("New Task!!", "You have a new Task titled \'" + createdTask.getTitle()
				+ "\'. Please reschedule the task at you preferable time.", client.getId(), client.getSrepresentative(), "/showCalendar");

		return createdTask;

	}

	private Task automatedUnscheduledTask(Proposal proposal, Client client)
	{
		Task automatedTask = new Task();
		automatedTask.setStatus(TaskStatus.DUE);

		if (ProposalStatus.WON.equals(proposal.getStatus()))
		{
			LocalDateTime start = LocalDateTime.now().plusWeeks((int) proposal.getValidfor());
			LocalDateTime end = LocalDateTime.now().plusWeeks((int) proposal.getValidfor() - 4).plusHours(4);

            if(start.getDayOfYear() != end.getDayOfYear())
            {
                start.plusHours(20);
                end.plusHours(20);
            }

            automatedTask.setStart(start);
            automatedTask.setEnd(end);

			automatedTask.setType(TaskType.FOLLOWUP);
			automatedTask.setTitle("Service is Expiring, Follow Up and Offer Another Proposal to " + client.getName());

		} else if (ProposalStatus.LOST.equals(proposal.getStatus()))
		{
            automatedTask = this.getSuitableTaskTime(automatedTask);

			automatedTask.setType(TaskType.FOLLOWUP);
			automatedTask.setTitle("Follow Up and Offer another proposal to " + client.getName());

		} else if (ProposalStatus.PROPOSED.equals(proposal.getStatus()))
		{
            automatedTask = this.getSuitableTaskTime(automatedTask);

			automatedTask.setType(TaskType.NEGOTIATE);
			automatedTask.setTitle("Negotiate the price with " + client.getName());
		}

		automatedTask.setSrepresentative(client.getSrepresentative());
		automatedTask.setClientid(client.getId());
		automatedTask.setClientstatus(client.getStatus());

		return automatedTask;
	}

	public List<TaskEventWrapper> getAllTaskForManager() {

		List<Task> tasks = null;
		List<TaskEventWrapper> tasksEvents = new ArrayList<TaskEventWrapper>();

		tasks = (List<Task>) taskRepo.findAll();

		for (Task t : tasks)
		{

			TaskEventWrapper tw = new TaskEventWrapper(t);

			tw.setEditable(t.isTeammeeting());

			if(!t.isTeammeeting())
			{
				tw.setTitle(t.getSrepresentative()+" "+tw.getTitle());
			}

			tasksEvents.add(tw);
		}

		return tasksEvents;

	}

	public void deleteCompletedtasks() {

		List<Task> tasks = taskRepo.findCompletedOldTasks(7);

		taskRepo.delete(tasks);
	}

	public void setMissedTasks() {

		List<Task> tasks = taskRepo.findMissedTasks();

		for(Task t:tasks)
		{
			t.setMissed(true);
		}

		List<Task> missedTasks = (List<Task>) taskRepo.save(tasks);


		for(Task t: missedTasks)
		{

			if(!t.isTeammeeting())
				statsService.createEventStats(t.getSrepresentative(),t.getClientid(), StatsType.MISSED);

		}
	}

	public void setTeamMeetingCompleted() {


		List<Task> tasks = taskRepo.findPassedTeamMeetings();

		System.out.println(tasks);

		for(Task t:tasks)
		{
			t.setStatus(TaskStatus.COMPLETED);
		}

		taskRepo.save(tasks);
	}


    public Task getTaskById(long id) {


        return taskRepo.findOne(id);
    }
}
