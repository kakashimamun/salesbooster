package com.worksap.stm.sb.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.worksap.stm.sb.dao.spec.UserDao;
import com.worksap.stm.sb.domain.User;

import com.worksap.stm.sb.repo.UserRepo;


@Service
public class UserService {

	
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	UserRepo userRepo;

	
	public List<User> getUsers() throws IOException
	{
		return userRepo.findAll();
	}
	
	public User getUserByUsername(String username) throws IOException
	{
		
		User user = null;
		
			user = userRepo.findByUsername(username);
		

			if(user == null)
				throw new UsernameNotFoundException("Username not found");
		
		return user;
		
	}
	
	

	public List<User> getAllSalesAssocs()
	{
		List<User> salesAssocs = null;
		
		
		salesAssocs = userRepo.findByRoles("SA");
		
		
		return salesAssocs;
	}

	
	public User createuser(User newUser)
	{
		return userRepo.save(newUser);
	}


	public List<User> getAllManagers() {

		List<User> managers = null;


		managers = userRepo.findByRoles("BM");


		return managers;
	}
}
