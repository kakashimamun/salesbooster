package com.worksap.stm.sb.type;

public enum ClientStatusType
{
	SUSPECT,
	PROSPECT,
	PROFILED,
	PROPOSED, 
	ACTIVE,
	REMOVED,
	EXCLIENT
}
