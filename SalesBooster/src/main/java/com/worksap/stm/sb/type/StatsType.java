package com.worksap.stm.sb.type;

public enum StatsType
{

	SUSPECT,PROSPECT,ACTIVE, EXCLIENT, REMOVED, MISSED, SALES
}
