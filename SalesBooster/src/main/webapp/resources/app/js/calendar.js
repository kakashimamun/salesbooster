/**
 * 
 */

var formatString = "DD/MM/YYYY-HH:mm";

function editEvent(event, delta, revertFunc)
{
	console.log(event);
	console.log(delta);

	var values =
	{};

	values['title'] = event.title;
	values['type'] = event.type;
	values['id'] = event.id;
	values['srepresentative'] = event.srepresentative;
	values['clientid'] = event.clientid;

	if (isset(event.clientstatus.$name))
		values['clientstatus'] = event.clientstatus.$name;
	else
		values['clientstatus'] = event.clientstatus;

	values['start'] = event.start.format(formatString);
	values['end'] = event.end.format(formatString);
	values['status'] = event.status;
	values['fixed'] = !event.editable;

	console.log(values);

	var url = editTaskCalUrl + event.id;

	ajaxJsonPost(url, values, function(data)
	{
		if (data.success)
		{
			showStatus(true, "Event Rescheduled");
		}

	}, function()
	{

		showStatus(false, data.msg)

		revertFunc();
	});

	var eventArray = [];

	var selectizeCleintElem;

}


var removedEvents;


/*
 * initialize the calendar
 * -----------------------------------------------------------------
 */
var calendar = $('#calendar').fullCalendar(
{
	customButtons :
	{
		onlydue :
		{
			text : 'Due Tasks!',
			click : function()
			{
				calendar.fullCalendar('removeEvents', function(event)
				{

					if (event.status !== 'DUE')
						return true;
				});

			}
		},
		all :
		{
			text : 'All Tasks!',
			click : function()
			{

				location.reload();

			}
		}
	},
	events : eventData,
	eventLimit : true, // for
	// all
	// non-agenda
	// views
	views :
	{
		agenda :
		{
			eventLimit : 20
		// adjust
		// to 6
		// only
		// for
		// agendaWeek/agendaDay
		}
	},
	height : 850,
	header :
	{
		left : 'prev,next today,onlydue,all',
		center : 'title',
		right : 'month,agendaDay',
		defaultView : 'agendaDay'
	},
	selectable : true,
	select : function(start, end, allDay, view)
	{

		// console.log(start);
		// console.log(end);
		// console.log(allDay);

		console.log(view);

		if (view.name !== 'month')
		{

			var addTaskModal = $('.modal-new-task');

			bootbox.dialog(
			{

				title : "Add New Task",
				message : addTaskModal.html(),
				onEscape : true

			});

			selectizeCleintElem = $('.bootbox-body .select-client').selectize(
			{
				plugins : [ 'remove_button' ],
				delimiter : ',',
				persist : false,
				preload : true,
				openOnFocus : true,
				optgroupValueField : 'id',
				valueField : 'id',
				labelField : 'name',
				searchField : 'name',
				create : false,
				load : function(query, callback)
				{

					$.ajax(
					{
						url : '/sb/myClientsList',
						type : 'GET',
						dataType : 'json',
						error : function()
						{
							callback();
						},
						success : function(res)
						{
							if (isset(res.data) && !empty(res.data))
								callback(res.data);
						}
					});
				}
			})[0].selectize;

			$('.task-create-form').submit(function(e)
			{

				var url = $(this).attr('action');
				var values =
				{};
				$.each($(this).serializeArray(), function(i, field)
				{
					values[field.name] = field.value;
				});

				var taskStart = moment(start).format(formatString);
				var taskEnd = moment(end).format(formatString);

				values['start'] = taskStart;
				values['end'] = taskEnd;

				console.log(values);

				ajaxJsonPost(url, values, function(data)
				{

					if (data.success)
					{
						console.log('Added!!');

						addTaskModal.find("input[type=text], textarea").val("");

						calendar.fullCalendar('renderEvent', data.data, true);

					}else
					{
						showStatus(data.success,data.msg);
					}
				}, function()
				{
				});

				bootbox.hideAll();
				e.preventDefault();
			});

		}
	},
	editable : true,
	dayClick : function(date, jsEvent, view, resourceObj)
	{

		if (view.name == 'month')
		{
			calendar.fullCalendar('changeView', 'agendaDay');
			calendar.fullCalendar('gotoDate', date);
		}

	},
	droppable : true, // this
	// allows
	// things
	// to be
	// dropped
	// onto
	// the
	// calendar !!!
	drop : function(date, allDay)
	{ // this function is
		// called when something
		// is dropped
		// retrieve the dropped
		// element's stored
		// Event Object
		var originalEventObject = $(this).data('eventObject');
		// we need to copy it,
		// so that multiple
		// events don't have a
		// reference to the same
		// object
		var copiedEventObject = $.extend(
		{}, originalEventObject);
		// assign it the date
		// that was reported
		copiedEventObject.start = date;
		copiedEventObject.allDay = allDay;
		// render the event on
		// the calendar
		// the last `true`
		// argument determines
		// if the event "sticks"
		// (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
		$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
		// is the "remove after
		// drop" checkbox
		// checked?
		if ($('#drop-remove').is(':checked'))
		{
			// if so, remove the
			// element from the
			// "Draggable
			// Events"
			// list
			$(this).remove();
		}
	},
	defaultView : 'month',
	eventRender : function(event, element)
	{

		element.attr('title', event.type);

		return element;
	},
	eventDrop : function(event, delta, revertFunc)
	{

		editEvent(event, delta, revertFunc);

	},
	eventResize : function(event, delta, revertFunc)
	{

		editEvent(event, delta, revertFunc);

	},
	eventClick : function(calEvent, jsEvent, view)
	{

	}
});