/*script for notes table*/
var notesTable;

$(document).ready(function()
{
	notesTable = $('.notes-table').DataTable(
	{
		"jQueryUI" : false,
		"lengthChange" : true,
		"autoWidth": false,
		"lengthMenu" : [ 5, 10, 15, 25, 50, 75, 100 ],
		"order" : [ [ 1, 'desc' ] ],
		"columnDefs" : [
		{
			"targets" : 'nosort',
			"orderable" : false
		},
		{
			"targets" : 'nosearch',
			"searchable" : false
		},
		{
			"targets" : 'noshow',
			"visible" : false
		},

		{
			"targets" : 'cannull',
			"defaultContent" : "<i>Not set</i>"
		} ],

		"initComplete" : function(settings, json)
		{

		},
		"rowCallback" : function(row, data, index)
		{

		},
		"dom" : 'Bfrtip',
		"buttons" : [
		{
			text : 'Create New Plan',
			className: "btn-dt",
			action : function()
			{
				var createModal = $('.create-service-modal');

				var content = $(".modal-create-note").html();
				bootbox.dialog(
				{
					title : "Add New Plan",
					message : content,
					onEscape: true
				});

				
			}
		} ],

		"renderer" : "bootstrap",
		"responsive" :
		{
			"details" :
			{
				display : $.fn.dataTable.Responsive.display.modal(
				{
					header : function(row)
					{
						var data = row.data();
						return 'Details for ' + data[0] + ' ' + data[1];
					}
				}),
				renderer : function(api, rowIdx, columns)
				{
					var data = $.map(columns, function(col, i)
					{
						return '<tr>' + '<td>' + col.title + ':' + '</td> ' + '<td>' + col.data + '</td>' + '</tr>';
					}).join('');

					return $('<table class="table"/>').append(data);
				}
			}
		},

		"scrollX" : false,

	});

	/* Formatting function for row details - modify as you need */
	function format(d)
	{

		console.log(d);

		var strVar = "";
		strVar += "<div class=\"card\">";
		strVar += "		<h4 class=\"page-header\" >Plan Details<\/h4>";
		strVar += "							<table class=\"table table-bordered\">";
		strVar += "								<tbody>";
		strVar += " <tr>";
		strVar += " <td><label>Heading<\/label><\/td>";
		strVar += " <td><label>" + d[1] + "<\/label><\/td>";
		strVar += " <\/tr> ";
		strVar += " <tr>";
		strVar += " <td><label>Client<\/label><\/td>";
		strVar += " <td><label>" + d[2] + "<\/label><\/td>";
		strVar += " <\/tr> ";
		strVar += "									<tr>";
		strVar += "										<td><label>Text<\/label><\/td>";
		strVar += "										<td><label>" + d[3] + "<\/label><\/td>";
		strVar += "									<\/tr>								";
		strVar += "									<tr>";
		strVar += "										<td><label>Created<\/label><\/td>";
		strVar += "										<td><label>" + d[4] + "<\/label><\/td>";
		strVar += "									<\/tr>							";
		strVar += "								<\/tbody>";
		strVar += "							<\/table>";
		strVar += "	<\/div>";

		return strVar;

	}

	notesTable.on('click', '.quick-details', function()
	{
		var tr = $(this).closest('tr');
		var row = notesTable.row(tr);

		var icon = $(this).children('i');

		if (row.child.isShown())
		{
			icon.removeClass('fa-eye-slash').addClass('fa-eye');

			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		} else
		{

			icon.removeClass('fa-eye').addClass('fa-eye-slash');

			// Open this row
			row.child(format(row.data())).show();
			tr.addClass('shown');
		}
	});
	notesTable.on('click', '.delete-note', function()
	{
		var tr = $(this).closest('tr');
		var row = notesTable.row(tr);

		var rowData = row.data();

		console.log(rowData);

		var url = "./deleteNote";

		var dataObj =
		{
			"id" : rowData[0]
		};

		ajaxJsonPost(url, dataObj, function(data)
		{

			showStatus(data.success, data.msg);

			if (data.success == true)
			{
				row.remove().draw();

			}

		}, function()
		{

			showStatus(false, "Note Deleted");

		});

	});

});

/* scrip for create note modal */
$(document).ready(function()
{

	var addNewNoteButton = $(".add-note-link");

	addNewNoteButton.on('click', function()
	{

		
	});

});
