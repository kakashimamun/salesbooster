/* script for create task modal */
$(document).ready(function()
{
	// var addNewTaskButton = $(".new-task-btn");

	var addTaskModal = $('.modal-new-task');

	// var taskTableElem = $('.tasks-table');

	var tasksTable;

	function addTask()
	{

		bootbox.dialog(
		{
			title : "Add New Task",
			message : addTaskModal.html(),
			onEscape : true
		});

		$('.bootbox-body .task-start').datetimepicker(
		{
			minDateTime : 0,
			dateFormat : "dd/mm/yy",
			timeFormat : "HH:mm",
			onSelect : function(dateText, inst)
			{
				console.log(dateText);

				var dtStr = dateText.split(" ");

				console.log(dtStr);

				dtStr[0] = $.datepicker.parseDate("dd/mm/yy", dtStr[0]);
				dtStr[1] = $.datepicker.parseTime("HH:mm", dtStr[1]);

				console.log(dtStr);

				$(".bootbox-body .task-end").datetimepicker("option", "minDate", dtStr[0]);
				$(".bootbox-body .task-end").datetimepicker("option", "minTime", dtStr[1]);
			}

		});

		$('.bootbox-body .task-end').datetimepicker(
		{
			dateFormat : "dd/mm/yy",
			timeFormat : "HH:mm",
			onSelect : function(dateText, inst)
			{

				console.log(dateText);

				var dtStr = dateText.split(" ");

				console.log(dtStr);

				dtStr[0] = $.datepicker.parseDate("dd/mm/yy", dtStr[0]);
				dtStr[1] = $.datepicker.parseTime("HH:mm", dtStr[1]);
				console.log(dtStr);

				$(".bootbox-body .task-start").datetimepicker("option", "maxDate", dtStr[0]);
				$(".bootbox-body .task-start").datetimepicker("option", "maxTime", dtStr[1]);
			}

		});

		$('.bootbox-body .task-create-form').submit(function(e)
		{

			var url = $(this).attr('action');
			var values =
			{};
			$.each($(this).serializeArray(), function(i, field)
			{
				values[field.name] = field.value;
			});

			console.log(values);

			ajaxJsonPost(url, values, function(data)
			{

				if (data.success)
				{
					console.log('Added!!');

					var start = Date.parse(data.data.start);
					var end = Date.parse(data.data.end);

					var taskStart = moment(start).format('DD/MM/YYYY HH:mm');
					var taskEnd = moment(end).format('DD/MM/YYYY HH:mm');

					addTaskModal.find("input[type=text], textarea").val("");

					var rowStr = "";

					rowStr += "<button class=\"btn btn-raised btn-xs quick-details\" title=\"See Details\">";
					rowStr += "												<i class=\"fa fa-eye fa-lg\"><\/i>";
					rowStr += "											<\/button>";
					rowStr += "					<button class=\"btn btn-raised btn-xs edit-task\" title=\"Edit\">";
					rowStr += "						<i class=\"fa fa-pencil fa-lg\"><\/i>";
					rowStr += "					<\/button>";
					rowStr += "					<button class=\"btn btn-raised btn-xs delete-task\" title=\"Delete\">";
					rowStr += "						<i class=\"fa fa-ban fa-lg\"><\/i>";
					rowStr += "					<\/button>";

					var newTask = [];

					newTask.push(data.data.id);
					newTask.push(data.data.title);
					newTask.push(data.data.srepresentative);
					newTask.push(data.data.type);
					newTask.push(taskStart);
					newTask.push(taskEnd);
					newTask.push(data.data.status);
					newTask.push(rowStr);

					tasksTable.row.add(newTask).draw();

				}
			}, function()
			{
			});

			bootbox.hideAll();
			e.preventDefault();
		});

	}

	tasksTable = $('.tasks-table').DataTable(
	{
		"jQueryUI" : false,
		"autoWidth" : false,
		"lengthChange" : true,
		"lengthMenu" : [ 5, 10, 15, 25, 50, 75, 100 ],
		"order" : [ [ 6, 'desc' ], [ 0, 'desc' ], ],
		"columnDefs" : [
		{
			"targets" : 'nosort',
			"orderable" : false
		},
		{
			"targets" : 'nosearch',
			"searchable" : false
		},
		{
			"targets" : 'noshow',
			"visible" : false
		},

		{
			"targets" : 'cannull',
			"defaultContent" : "<i>Not set</i>"
		} ],

		"initComplete" : function(settings, json)
		{

		},
		"rowCallback" : function(row, data, index)
		{

		},
		"dom" : 'Bfrtip',
		"buttons" : [
		{
			text : 'Create New Task',
			className : "btn-dt",
			action : function()
			{
				addTask();
			}
		} ],

		"renderer" : "bootstrap",
		"responsive" :
		{
			"details" :
			{
				display : $.fn.dataTable.Responsive.display.modal(
				{
					header : function(row)
					{
						var data = row.data();
						return 'Details for ' + data[0] + ' ' + data[1];
					}
				}),
				renderer : function(api, rowIdx, columns)
				{
					var data = $.map(columns, function(col, i)
					{
						return '<tr>' + '<td>' + col.title + ':' + '</td> ' + '<td>' + col.data + '</td>' + '</tr>';
					}).join('');

					return $('<table class="table"/>').append(data);
				}
			}
		},

		"scrollX" : false,

	});

	/* Formatting function for row details - modify as you need */
	function format(d)
	{
		console.log(d);

		var strVar = "";
		strVar += "<div class=\"card\">";
		strVar += "		<h4 class=\"page-header\" >Quick Details<\/h4>";
		strVar += "							<table class=\"table\">";
		strVar += "								<tbody>";
		strVar += "									<tr>";
		strVar += "										<td><label>ID<\/label><\/td>";
		strVar += "										<td><label>" + d[0] + "<\/label><\/td>";
		strVar += "									<\/tr>								";
		strVar += "									<tr>";
		strVar += "										<td><label>Start Date & Time<\/label><\/td>";
		strVar += "										<td><label>" + d[4] + "<\/label><\/td>";
		strVar += "									<\/tr>								";
		strVar += "									<tr>";
		strVar += "										<td><label>End Date & Time<\/label><\/td>";
		strVar += "										<td><label>" + d[5] + "<\/label><\/td>";
		strVar += "									<\/tr>								";
		strVar += "									<tr>";
		strVar += "										<td><label>Status<\/label><\/td>";
		strVar += "										<td><label>" + d[3] + "<\/label><\/td>";
		strVar += "									<\/tr>								";
		strVar += "								<\/tbody>";
		strVar += "							<\/table>";
		strVar += "	<\/div>";

		return strVar;

	}

	tasksTable.on('click', '.quick-details', function()
	{
		var tr = $(this).closest('tr');
		var row = tasksTable.row(tr);

		var icon = $(this).children('i');

		if (row.child.isShown())
		{

			icon.removeClass('fa-eye-slash').addClass('fa-eye');

			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		} else
		{

			icon.removeClass('fa-eye').addClass('fa-eye-slash');

			// Open this row
			row.child(format(row.data())).show();
			tr.addClass('shown');
		}
	});

	// Script for delete Task
	tasksTable.on('click', '.delete-task', function()
	{
		var tr = $(this).closest('tr');
		var row = tasksTable.row(tr);
		var rowData = row.data();

		var url = deleteTaskUrl;

		var dataObj =
		{
			"id" : is_numeric(rowData[0]) ? rowData[0].toString() : rowData[0]
		};

		ajaxJsonPost(url, dataObj, function(data)
		{
			showStatus(data.success, data.msg);

			if (data.success == true)
			{
				row.remove().draw();
			}
		},

		function()
		{
			showStatus(false, "Failed to Delete");

		});

	});

	tasksTable.on('click', '.edit-task', function()
	{
		var tr = $(this).closest('tr');
		var row = tasksTable.row(tr);
		var rowData = row.data();

		var editTaskModal = $('.modal-edit-task');

		bootbox.dialog(
		{
			title : "Edit Task " + rowData[0],
			message : editTaskModal.html(),
			onEscape : true
		});

		$('.bootbox-body .task-title').val(rowData[1]);
		$('.bootbox-body .task-type').val(rowData[3]);

		$('.bootbox-body .task-start').val(rowData[4]);
		$('.bootbox-body .task-end').val(rowData[5]);

		if (rowData[6] == 'DUE')
			$('.bootbox-body .task-completed').prop('checked', false);
		else
			$('.bootbox-body .task-completed').prop('checked', true);

		$('.bootbox-body .task-start').datetimepicker(
		{
			minDateTime : 0,
			dateFormat : "dd/mm/yy",
			timeFormat : "HH:mm",
			onSelect : function(dateText, inst)
			{
				console.log(dateText);

				var dtStr = dateText.split(" ");

				console.log(dtStr);

				dtStr[0] = $.datepicker.parseDate("dd/mm/yy", dtStr[0]);
				dtStr[1] = $.datepicker.parseTime("HH:mm", dtStr[1]);

				console.log(dtStr);

				$(".bootbox-body .task-end").datetimepicker("option", "minDate", dtStr[0]);
				$(".bootbox-body .task-end").datetimepicker("option", "minTime", dtStr[1]);
			}

		});

		$('.bootbox-body .task-end').datetimepicker(
		{
			dateFormat : "dd/mm/yy",
			timeFormat : "HH:mm",
			onSelect : function(dateText, inst)
			{

				console.log(dateText);

				var dtStr = dateText.split(" ");

				console.log(dtStr);

				dtStr[0] = $.datepicker.parseDate("dd/mm/yy", dtStr[0]);
				dtStr[1] = $.datepicker.parseTime("HH:mm", dtStr[1]);
				console.log(dtStr);

				$(".bootbox-body .task-start").datetimepicker("option", "maxDate", dtStr[0]);
				$(".bootbox-body .task-start").datetimepicker("option", "maxTime", dtStr[1]);
			}

		});
		$('.bootbox-body .task-edit-form').submit(function(e)
		{

			var url = editTaskUrl + '/' + rowData[0];

			var values =
			{};

			$.each($(this).serializeArray(), function(i, field)
			{
				values[field.name] = field.value;
			});

			console.log(values);
			console.log(url);

			ajaxJsonPost(url, values, function(data)
			{
				if (data.success)
				{
					console.log('Edited!!');

					row.remove().draw();

					var start = Date.parse(data.data.start);
					var end = Date.parse(data.data.end);

					var taskStart = moment(start).format('DD/MM/YYYY HH:mm');
					var taskEnd = moment(end).format('DD/MM/YYYY HH:mm');

					var rowStr = "";
					rowStr += "<button class=\"btn btn-raised btn-xs quick-details\" title=\"See Details\">";
					rowStr += "												<i class=\"fa fa-eye fa-lg\"><\/i>";
					rowStr += "											<\/button>";
					rowStr += "					<button class=\"btn btn-raised btn-xs edit-task\" title=\"Edit\">";
					rowStr += "						<i class=\"fa fa-pencil fa-lg\"><\/i>";
					rowStr += "					<\/button>";
					rowStr += "					<button class=\"btn btn-raised btn-xs delete-task\" title=\"Delete\">";
					rowStr += "						<i class=\"fa fa-ban fa-lg\"><\/i>";
					rowStr += "					<\/button>";

					var newTask = [];

					newTask.push(data.data.id);
					newTask.push(data.data.title);
					newTask.push(data.data.srepresentative);
					newTask.push(data.data.type);
					newTask.push(taskStart);
					newTask.push(taskEnd);
					newTask.push(data.data.status);
					newTask.push(rowStr);

					tasksTable.row.add(newTask).draw();

				}
			}, function()
			{
			});

			bootbox.hideAll();
			e.preventDefault();
		});

	});

});
