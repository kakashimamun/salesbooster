var directionsService;
var directionsRenderer;
var map;
var waypoints;
var dist;
var draw;


function sleep(miliseconds) {
    var currentTime = new Date().getTime();

    while (currentTime + miliseconds >= new Date().getTime());
}

function computeTotalDistance(result) {
	var totalDist = 0;
	var myroute = result.routes[0];
	var count;
	for (count = 0; count < myroute.legs.length; count++) {
		totalDist += myroute.legs[count].distance.value;
	}
	
	dist = totalDist;
}

function calcRoute(waypoints) {
	var copyWayPoints = waypoints.slice();
	var start = copyWayPoints[0];
	var end = copyWayPoints[copyWayPoints.length - 1];
	copyWayPoints.splice(0, 1);
	copyWayPoints.splice(copyWayPoints.length - 1, 1);

	var waypts = [];

	for ( var i in copyWayPoints) {
		waypts.push({
			location : copyWayPoints[i],
			stopover : true
		});
	}

	console.log(start);
	console.log(end);
	console.log(waypts);

	var request = {
		origin : start,
		destination : end,
		waypoints : waypts,
		optimizeWaypoints : true,
		travelMode : google.maps.DirectionsTravelMode.DRIVING
	};

	directionsService.route(request, function(response, status)
	{
		console.log(response);
		console.log(draw);
		
		if (status == google.maps.DirectionsStatus.OK) 
		{
			
			if(draw){
				directionsRenderer.setMap(null);
				directionsRenderer = new google.maps.DirectionsRenderer();
				directionsRenderer.setMap(map);
				directionsRenderer.setDirections(response);
			}
			
			computeTotalDistance(response);
			console.log('Distance:'+dist);

		} else {
			alert("directions response " + status);
		}
	});
}

function arrManip(waypoints, newwp) {

	var comp = 0;
	var con = 0;
	
	for (var i = 0; i <= waypoints.length; i++) {
		waypoints.splice(i, 0, newwp);

		console.log('Waypoints in Loop for ' + i + ':' + waypoints);

			calcRoute(waypoints);
			console.log(dist);
		
			if(comp > dist){ comp = dist; con = i;}
			
			sleep(3000);
	 
		newwp = waypoints[i];
		waypoints.splice(i, 1);
	}
	
	draw = true;
	waypoints.splice(con, 0, newwp);

	console.log('Selected waypts for' + con + ':' + waypoints);

		calcRoute(waypoints);
		console.log(dist);

}

function initialize() {

	waypoints = [
			new google.maps.LatLng(-8.04745931112481, -34.896676540374756),
			new google.maps.LatLng(-8.048372903858715, -34.89927291870117),
			new google.maps.LatLng(-8.046248266419221, -34.90176200866699),
			new google.maps.LatLng(-8.052728375778878, -34.9010968208313) ];
	

	var position = new google.maps.LatLng(-8.05, -34.89);

	directionsService = new google.maps.DirectionsService();
	directionsRenderer = new google.maps.DirectionsRenderer();

	map = new google.maps.Map(document.getElementById("map"), {
		zoom : 16,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		center : position
	});

	directionsRenderer.setMap(map);
	
	draw = true;
	
	calcRoute(waypoints);
	
	
	google.maps.event.addListener(map, 'click', function(event) 
	{

		newwp = event.latLng;
		console.log('new point:' + event.latLng);			
		arrManip(waypoints, newwp);
		
	});
}

/**
 * 
 */
