var markers = [];
var map;
var infowindow;
var subjectPoint;
var subjectMarker;
var service;
var selectizeElem;
var image = "resources/app/img/marker.png";

var placesTable;

var typesJSON = {};

var gmapIds = [];

function addAllClicked(event)
{
	// console.log('works');

	$('.data').each(function()
	{
		var obj = $(this);
		var objData = JSON.parse(obj.text());

		ajaxJsonPost('singleSuspectSave', objData, function(data)
		{
			console.log(data);

			if (data.success)
			{

				obj.closest('tr').fadeOut(300, function()
				{
					$(this).remove();
				});

				showStatus(true, "All Clients are added apart from the duplicate ones")

			} else
			{
				obj.closest('tr').addClass("danger");

				showStatus(false, "Client already exists.");
			}

		}, function(xhr)
		{
			console.log(xhr);
		});

	});
}

function addSingle(row)
{

	ajaxJsonPost(singleSuspectSaveUrl, row.data(), function(data)
	{
		console.log(data);

		if (data.success)
		{

			showStatus(true, "New Suspect added to the Database")
			row.remove().draw();

		} else
		{

			showStatus(false, "Client already exists. So removed from the list");
			row.remove().draw();
		}

	}, function(xhr)
	{
		console.log(xhr);
	});
}

function deleteClicked(e)
{
	$(e.target).closest('tr').fadeOut(300, function()
	{
		$(this).remove();

		showStatus(true, "Suspect Removed.");
	});
}

function newMarkerPositionAdjust(e)
{
	console.log(e);
	console.log(e.latLng);

	map.panTo(e.latLng);
	subjectMarker.setPosition(e.latLng);
	subjectPoint.point = e.latLng;
}

function initMap()
{
	$.ajax(
		{
			url : typeFetchUrl,
			type : 'GET',
			dataType : 'json',
			error : function()
			{

			},
			success : function(res)
			{
				typesJSON = res;

			}
		});

	$.ajax(
		{
			url : gmapIdUrl,
			type : 'GET',
			dataType : 'json',
			error : function()
			{

			},
			success : function(res)
			{
				gmapIds = res.data;

			}
		});




	placesTable = $('.gmap-table').DataTable(
	{
		"jQueryUI" : false,
		"lengthChange" : false,
		"autoWidth" : false,
		"order" : [ [ 12, 'asc' ],[9,'desc'] ],
		"scrollY" : "550px",
		"scrollCollapse" : true,
		"paging" : false,
		"columnDefs" : [
		{
			"targets" : 'nosort',
			"orderable" : false
		},
		{
			"targets" : 'nosearch',
			"searchable" : false
		},
		{
			"targets" : 'noshow',
			"visible" : false
		},

		{
			"targets" : 'cannull',
			"defaultContent" : "<i>Not set</i>"
		},
		{
			orderable : false,
			className : 'select-checkbox',
			targets : 0
		},
		],

		"initComplete" : function(settings, json)
		{

		},
		"rowCallback" : function(row, data, index)
		{

		},
		"dom" : 'ifrt',
		//"buttons" : [
		//{
		//	extend : 'selectAll',
		//	className : 'btn-dt'
		//},
		//{
		//	extend : 'selectNone',
		//	className : 'btn-dt'
		//},
		//{
		//	text : 'Add Selected',
		//	className : 'btn-dt',
		//	action : function()
		//	{
		//		var selectedRows = placesTable.rows(
		//		{
		//			selected : true
		//		}).data();
        //
		//		selectedRows.each(function(row)
		//		{
        //
		//			console.log(row);
        //
		//			ajaxJsonPost(singleSuspectSaveUrl, row, function(data)
		//			{
		//				console.log(data);
		//				if (data.success)
		//				{
		//					showStatus(true, "New Suspect "+row.name+" added to the Database")
		//				} else
		//				{
		//					showStatus(false, "Client "+row.name+" already exists. So removed from the list");
		//				}
        //
		//				placesTable.row('[id="' + row.placeid + '"]').remove().draw();
		//			}, function(xhr)
		//			{
		//				console.log(xhr);
		//			});
        //
		//		});
        //
		//	}
		//},
		//{
		//	text : 'Remove Selected',
		//	className : 'btn-dt',
		//	action : function()
		//	{
		//		placesTable.rows(
		//				{
		//					selected : true
		//				}).remove().draw();
		//
		//		notify("Entries Removed",'Selected entries removed','warning',1000);
		//	}
		//},
		//],
		"columns" : [
		{
			"data" : ""
		},
		{
			"data" : "name"
		},
		{
			"data" : "address"
		},
		{
			"data" : "phone"
		},
		{
			"data" : "website"
		},
		{
			"data" : "geolocation"
		},
		{
			"data" : "placeid"
		},
		{
			"data" : "gmapid"
		},
		{
			"data" : "industry"
		},
		{
			"data" : "givenleadscore"
		},
		{
			"data" : "details"
		},
		{
			"data" : "actions"
		},
		{
			"data" : "time"
		},

		],
		"rowId" : "gmapid",
		"renderer" : "bootstrap",
		"responsive" :
		{
			"details" :
			{
				display : $.fn.dataTable.Responsive.display.modal(
				{
					header : function(row)
					{
						var data = row.data();
						return 'Details for ' + data[0] + ' ' + data[1];
					}
				}),
				renderer : function(api, rowIdx, columns)
				{
					var data = $.map(columns, function(col, i)
					{
						return '<tr>' + '<td>' + col.title + ':' + '</td> ' + '<td>' + col.data + '</td>' + '</tr>';
					}).join('');

					return $('<table class="table"/>').append(data);
				}
			}
		},
		//"select" :
		//{
		//	style : 'multi',
		//	selector : 'td:first-child'
		//}

	});

	/* Formatting function for row details - modify as you need */
	function format(d)
	{

		console.log(d);

		var strVar = "";
		strVar += "<div class=\"card\">";
		strVar += "		<h4 class=\"page-header\" >Details<\/h4>";
		strVar += "							<table class=\"table table-bordered\">";
		strVar += "								<tbody>";
		strVar += " <tr>";
		strVar += " <td><label>Phone<\/label><\/td>";
		strVar += " <td><label>" + d.phone + "<\/label><\/td>";
		strVar += " <\/tr> ";
		strVar += " <tr>";
		strVar += " <td><label>Website<\/label><\/td>";
		strVar += " <td><a href='"+ d.website+" target=_blank></a><\/td>";
		strVar += " <\/tr> ";
		strVar += " <tr>";
		strVar += " <td><label>Geolocation<\/label><\/td>";
		strVar += " <td><label>" + d.geolocation + "<\/label><\/td>";
		strVar += " <\/tr> ";
		strVar += "	<tr>";
		strVar += " <td><label>Types<\/label><\/td>";
		strVar += " <td><label>" + d.industry + "<\/label><\/td>";
		strVar += "	<\/tr>								";
		strVar += "	<tr>";
		strVar += " <td><label>Actions<\/label><\/td>";
		strVar += " <td><label>" + d.actions + "<\/label><\/td>";
		strVar += "	<\/tr>								";
		strVar += "	<\/tbody>";
		strVar += "	<\/table>";
		strVar += "	<\/div>";

		return strVar;

	}

	placesTable.on('click', '.details', function()
	{
		var tr = $(this).closest('tr');
		var row = placesTable.row(tr);

		var icon = $(this).children('i');

		if (row.child.isShown())
		{
			icon.removeClass('fa-eye-slash').addClass('fa-eye');

			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');

		} else
		{

			icon.removeClass('fa-eye').addClass('fa-eye-slash');

			// Open this row
			row.child(format(row.data())).show();
			tr.addClass('shown');
		}
	});

	placesTable.on('click', '.gmap-add', function()
	{
		var tr = $(this).closest('tr');
		var row = placesTable.row(tr);
		var rowData = row.data();

		//$(".bootbox-body .suspect-form input[name*='name']").val(rowData.name);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);
		//$(".bootbox-body .suspect-form input[name*='industry']").val(rowData.industry);



		ajaxJsonPost(singleSuspectSaveUrl, rowData, function(data)
		{
			console.log(data);

			if (data.success)
			{

				showStatus(true, "New Suspect added to the Database")
				row.remove().draw();

			} else
			{

				showStatus(false, "Client already exists. So removed from the list");
				row.remove().draw();
			}

		}, function(xhr)
		{
			console.log(xhr);
		});

	});
	placesTable.on('click', '.gmap-remove', function()
	{
		var tr = $(this).closest('tr');
		var row = placesTable.row(tr);

		row.remove().draw();

		notify('Removed!!', 'Entry removed', 'warning', 1000);

	});

	var latlng = new google.maps.LatLng(1.280095, 103.850949);

	var myOptions =
	{
		zoom : 13,
		center : latlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		scaleControl : true,
		scaleControlOptions :
		{
			position : google.maps.ControlPosition.BOTTOM_LEFT
		}
	}

	map = new google.maps.Map(document.getElementById("map"), myOptions);



	//// Try HTML5 geolocation.
	//if (navigator.geolocation) {
	//	navigator.geolocation.getCurrentPosition(function(position) {
	//		var pos = {
	//			lat: position.coords.latitude,
	//			lng: position.coords.longitude
	//		};
    //
    //
	//		map.setCenter(pos);
	//	}, function() {
	//		handleLocationError(true, infoWindow, map.getCenter());
	//	});
	//} else {
	//	// Browser doesn't support Geolocation
	//	handleLocationError(false, infoWindow, map.getCenter());
	//}



	map.addListener('click', function(e)
	{
		newMarkerPositionAdjust(e);

	});

	/**
	 * Draw points and check whether those points are inside a range from a
	 * point.
	 */

	subjectPoint =
	{
		point : new google.maps.LatLng(1.280095, 103.850949),
		radius : 2.0, // default radius
		color : '#D3D9DB',
	}

	selectizeElem = $('#select_type').selectize({
		plugins : [ 'remove_button' ],
		delimiter : ',',
		persist : false,
		preload : true,
		openOnFocus : true,
		valueField : 'value',
		labelField : 'label',
		searchField : 'label',
		options : [],
		create : false,
		load : function(query, callback)
		{
			if (!query.length)
				return callback();
			$.ajax(
			{
				url : '/sb/resources/json/mapsearchtypes.json',
				type : 'GET',
				dataType : 'json',
				error : function()
				{
					callback();
				},
				success : function(res)
				{

					callback(res);


				}
			});
		}
	})[0].selectize;

	// render the range
	subjectMarker = new google.maps.Marker(
	{
		position : subjectPoint.point,
		//icon : image,
		map : map,
		draggable : true
	});

	// google.maps.event.addListener(subjectMarker,'drag',function(e) {
	// newMarkerPositionAdjust(e)
	// });

	google.maps.event.addListener(subjectMarker, 'dragend', function(e)
	{
		newMarkerPositionAdjust(e);

	});

	var subjectRange = new google.maps.Circle(
	{
		map : map,
		radius : subjectPoint.radius * 1000,
		fillColor : subjectPoint.color,
		strokeColor : '#58C227'
	});

	subjectRange.bindTo('center', subjectMarker, 'position');

	infowindow = new google.maps.InfoWindow();

	service = new google.maps.places.PlacesService(map);

	var slider_range_min = $(".slider-range-min");
	slider_range_min.slider(
	{
		range : "min",
		value : 20,
		min : 1,
		max : 300,
		slide : function(event, ui)
		{
			rad = ui.value;
			// console.log(rad);
			subjectRange.setOptions(
			{
				radius : rad * 100
			});
			map.fitBounds(subjectRange.getBounds());
		}
	});

	$("#map-search").on('click', function()
	{



		for (var i = 0; i < markers.length; i++)
		{
			markers[i].setMap(null);
		}
		markers = [];

		// Remove More button listeners
		var el = document.getElementById('more'), elClone = el.cloneNode(true);
		el.parentNode.replaceChild(elClone, el);

		console.log('Radius : ' + subjectRange.radius);




		typeStr = selectizeElem.getValue();


		service.nearbySearch(
		{
			location : subjectPoint.point,
			radius : subjectRange.radius - 1000,
			types : [ typeStr ],
		}, processResults);

	});

}

function processResults(results, status, pagination)
{
	if (status !== google.maps.places.PlacesServiceStatus.OK)
	{
		return;

	} else
	{
		searchDetails(results);

		if (pagination.hasNextPage)
		{
			var moreButton = document.getElementById('more');

			moreButton.disabled = false;

			moreButton.addEventListener('click', function()
			{
				moreButton.disabled = true;
				pagination.nextPage();
			});
		}
	}
}

function hasMatch(str)
{
	var hasMatch = false;



	for (var index = 0; index < typesJSON.length; ++index) {

		var tag = typesJSON[index].value;

		if(tag === str){
			hasMatch = true;
			break;
		}
	}

	return hasMatch;
}

function typeCheck(types)
{

	for(var i in types)
	{

		if(hasMatch(types[i]))
		{
			return types[i];
		}
	}

}

function getGivenLeadScore(rating)
{
	if(rating > 4.5)
	{
		return 2;
	}else if(rating < 4.5 && rating > 1.5)
	{
		return 1;
	}else
	{
		return 0;
	}
}

function containGmap(str)
{
	for(var i in gmapIds)
	{
		if(gmapIds[i] === str) return true;
	}

	gmapIds.push(str);


	return false;
}


function searchDetails(places)
{
	var bounds = new google.maps.LatLngBounds();
	var placesList = document.getElementById('list');

	for (var i = 0, place; place = places[i]; i++)
	{
		var icon =
		{
			url : place.icon,
			size : new google.maps.Size(71, 71),
			origin : new google.maps.Point(0, 0),
			anchor : new google.maps.Point(17, 34),
			scaledSize : new google.maps.Size(20, 20)
		};


		service
				.getDetails(
						{
							placeId : place.place_id
						},
						function(place, status)
						{
							if (status === google.maps.places.PlacesServiceStatus.OK)
							{

								if(isset(place.types)) {

									typeStr = typeCheck(place.types);



									if(!empty(typeStr) && !containGmap(place.place_id)) {

										var marker = new google.maps.Marker(
											{
												map: map,
												icon: icon,
												position: place.geometry.location
											});

										google.maps.event.addListener(marker, 'click', function () {
											infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + 'Place ID: ' + place.place_id + '<br>'
												+ place.formatted_address + '</div>');
											infowindow.open(map, this);
										});


										var givenLeadScore = 0 ;

										if(isset(place.rating))
										{
											givenLeadScore = getGivenLeadScore(place.rating);
										}



										var detailStr = "";
										detailStr += "<button class=\"btn btn-raised btn-xs details\" title=\" Details\"><i class=\"fa fa-eye fa-lg\"><\/i></a></button>";

										detailStr += '<button class="btn btn-xs btn-raised gmap-add" ><i class="fa fa-plus-square fa-lg"><div class="data" style="display:none;" >'
											+ dataObj + '</div></i></button>';

										detailStr += '<button class="delete-place btn btn-xs btn-raised gmap-remove"><i class="fa fa-trash fa-lg"></i></button>';

										var gSearchUrl = getGoogleSearchUrl(place.name, '');

										var actionsStr = '';
										actionsStr += '<button class="btn btn-xs btn-raised"><a href="' + gSearchUrl
											+ '" target=_blank><i class="fa fa-google fa-lg"></i></a></button>';

										actionsStr += '<button class="btn btn-xs btn-raised" ><a href="' + place.url
											+ '" target=_blank><i class="fa fa-map-marker fa-lg"></i></a></button>';

										var dataObj =
										{
											'': '',
											'name': place.name,
											'address': place.formatted_address,
											'phone': isset(place.international_phone_number) ? place.international_phone_number : "",
											'website': isset(place.website) ? place.website : "",
											'geolocation': place.geometry.location,
											'placeid': place.name.toLowerCase().replace(/ /g, "+"),
											'gmapid': place.place_id,
											'industry': typeStr,
											'details': detailStr,
											'actions': actionsStr,
											'time': new Date().getTime(),
											'givenleadscore': givenLeadScore
										}

										if (placesTable.rows('[id=" + dataObj.placeid + "]').any()) {
											console.log('already exist cannot be added');
											notify("Already Exists!!", "Business " + dataObj.name + " alredy in the list .", 'warning', 500);
										} else {
											placesTable.row.add(dataObj).draw();
											notify("Found New!!", "Please Scroll Down for " + dataObj.name + " .", 'info', 500);
										}
									}
								}
							
							}
						});

		bounds.extend(place.geometry.location);
	}
	// map.fitBounds(bounds);

}

function autoScroll(placeId)
{
	lastElementTop = $('#places #' + placeId).position().top;
	scrollAmount = lastElementTop - 200;

	$('#listofstuff').animate(
	{
		scrollTop : scrollAmount
	}, 1000);
}
