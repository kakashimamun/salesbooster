

function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
  }


var directionsService;
var directionsRenderer;
var map;

var waypoints;
var renderers = [];

var dist = [];
var best;
var polylines = [];
var markers = [];

var loopCount;


var dfd = new $.Deferred();


$.when(dfd).then(computeTotalDistance);



function saveDist(data,count)
{
	dist[count] = data;
	
	console.log(data);
	console.log(count);
}



function computeTotalDistance(result,count) {
  var totalDist = 0;
  var myroute = result.routes[0];
  for (i = 0; i < myroute.legs.length; i++) {
    totalDist += myroute.legs[i].distance.value;
  }
  dist[count] +=  totalDist;


  
  console.log('Internal call '+dist[count]+'\n');
}


function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function RenderDirection(map, response, display) {
	display.setMap(map);
	display.setDirections(response);
  }


function responseCallback(response,count)
{
	
//	var color = getRandomColor() ;
//	var tempRenderer = new google.maps.DirectionsRenderer({suppressMarkers: true,polylineOptions: { strokeColor: color}});
//	tempRenderer.arrayIndex = renderers.length;
//	renderers.push(tempRenderer);
//	RenderDirection(map, response, tempRenderer);
	
	
//	var polyline = new google.maps.Polyline({ strokeColor: getRandomColor()});
//	var path = response.routes[0].overview_path;
//	for (var x in path) {
//		polyline.getPath().push(path[x]);
//	}
//	polyline.setMap(map);
//	polylines.push(polyline);
	
	console.log('called for:'+count);
//	computeTotalDistance(response,count);
	
	dfd.notify(response,count);
}

function appendWayPoint(loc1,loc2,lastIndex,count) {
	
	var request = {
		origin: loc1,
		destination: loc2,
		travelMode: google.maps.DirectionsTravelMode.DRIVING
	};
	
	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) 
		{		
			
			if(lastIndex+1< waypoints.length)
				appendWayPoint(waypoints[lastIndex], waypoints[lastIndex+1], lastIndex+1, count)
			
			
			responseCallback(response,count);
		}
	});
}



function removePrevious(loopCount)
{
	 dist[loopCount] = 0;
	  
	  for(var x in polylines)
	  {
		  polylines[x].setMap(null);
	  }
	  polylines = [];
	  
	  for(var x in renderers)
	  {
		  renderers[x].setMap(null);
	  }
	  renderers = [];
	  
}


function calcRoute(waypoints,loopCount) {

	
  console.log('loop'+loopCount);
  removePrevious(loopCount);
  
//  for(var i=0;i<waypoints.length-1;i++)
//  {
//    appendWayPoint(waypoints[i],waypoints[i+1],loopCount); 
//  }
  
  appendWayPoint(waypoints[0], waypoints[1],1,loopCount)
  
  console.log('Distance for : '+loopCount+':'+dist[loopCount]);
}


function arrManip(waypoints, newwp) {

  for (loopCount = 0; loopCount <= waypoints.length; loopCount++) 
  {
    waypoints.splice(loopCount, 0, newwp);

    console.log('Waypoints in Loop for ' + loopCount + ':' + waypoints);
    calcRoute(waypoints,loopCount);
    sleep(2000);
    newwp = waypoints[loopCount];
    waypoints.splice(loopCount, 1);
  }

}


function createMarker(waypoint)
{
	
  var marker = new google.maps.Marker({
				position: waypoint, 
				map: map
			});
  markers.push(marker);
}


function initialize() 
{
	
	
  var position = new google.maps.LatLng(-8.05, -34.89);
  
  waypoints = [
               new google.maps.LatLng(-8.04745931112481, -34.896676540374756),
//               new google.maps.LatLng(-8.048372903858715, -34.89927291870117),
//               new google.maps.LatLng(-8.046248266419221, -34.90176200866699),
               new google.maps.LatLng(-8.052728375778878, -34.9010968208313)
             ];

  directionsService = new google.maps.DirectionsService();
  directionsRenderer = new google.maps.DirectionsRenderer();
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: 16,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: position
  });

  directionsRenderer.setMap(map);
  
//  appendWayPoint(waypoints[0], waypoints[1]);
//  appendWayPoint(waypoints[1], waypoints[2]);
//  appendWayPoint(waypoints[2], waypoints[3]);
  
//    calcRoute(waypoints,0);
  
//  for(var i in waypoints)
//  {
//    createMarker(waypoints[i]); 
//  }

  

  
  google.maps.event.addListener(map, 'click', function(event) {

    newwp = event.latLng;
    console.log('new point:' + event.latLng);
//    createMarker(newwp);
    arrManip(waypoints, newwp);
    
    console.log(dist);
    
  });
}



