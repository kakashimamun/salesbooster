$(document).ready(function()
{
	var table = $('.datatableCustom').DataTable(
	{
		'aoColumnDefs' : [
		{
			'bSortable' : false,
			'aTargets' : [ 'nosort' ]

		},
		{
			'bSearch' : false,
			'aTargets' : [ 'nofilter' ]

		} ],
		initComplete : function()
		{
			this.api().columns('.filter').every(function()
			{
				var column = this;

				var select = $('<select><option value=""></option></select>').appendTo($(column.footer()).empty()).on('change', function()
				{
					var val = $.fn.dataTable.util.escapeRegex($(this).val());

					column.search(val ? '^' + val + '$' : '', true, false).draw();
				});

				column.data().unique().sort().each(function(d, j)
				{
					select.append('<option value="' + d + '">' + d + '</option>')
				});
			});
		}
	});

});