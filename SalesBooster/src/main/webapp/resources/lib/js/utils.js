$.fn.serializeObject = function()
{
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};


function dateTimeStr(date)
{
	return moment(date).format('DD/MM/YYYY HH:mm');
}
function dateStr(date)
{
	return moment(date).format('DD/MM/YYYY');
}




function formatNumberToMoney(n, currency) {
	return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
			return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
		});
}



function getFullUrl(relativeUrl)
{
	var baseUrl = $('base').attr('href');
			
			console.log(baseUrl);
			
			
			return baseUrl+relativeUrl;
}


function textBoolToText(bool)
{
	if (bool == "true")
		return "Yes";
	else
		return "No";

}

function textToBool(text)
{
	if (text.toLowerCase() === "yes")
		return true;
	else
		return false;
}

function sleep(milliseconds)
{
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++)
	{
		if ((new Date().getTime() - start) > milliseconds)
		{
			break;
		}
	}
}

function bootBoxConfirmation(header, okay, cancel)
{

	bootbox.confirm(header, function(result)
	{

		if (result)
			okay();
		else
			cancel();

	});
}

function notifyBase(title, msg, type, delay, link,onShown)
{
	var icon = 'fa fa-lg fa-info'

	if (type == 'warning')
	{
		icon = 'fa fa-exclamation-triangle';
	} else if (type == 'danger')
	{

		icon = 'fa fa-exclamation-triangle';
	}

	$.notify(
	{
		icon : 'fa fa-info',
		title : '<strong>' + title + '</strong>',
		message : msg,
		url : link,
		target : '_blank'
	},
	{
		type : type,
		z_index : 2060,
		delay : delay,
		timer : 1000,
		placement :
		{
			from : "top",
			align : "left"
		},
		animate :
		{
			enter : 'animated fadeInDown',
			exit : 'animated fadeOutUp'
		},
		onShow : null,
		onShown : onShown,
		onClose : null,
		onClosed : null,
		mouse_over : null,
		offset : 20,
		spacing : 10,
		newest_on_top : true,
		icon_type : 'class',

	});
}

function notifyBaseOnClose(title, msg, type, delay, link,onClose)
{
	var icon = 'fa fa-lg fa-info'

	if (type == 'warning')
	{
		icon = 'fa fa-exclamation-triangle';
	} else if (type == 'danger')
	{

		icon = 'fa fa-exclamation-triangle';
	}

	$.notify(
	{
		icon : 'fa fa-info',
		title : '<strong>' + title + '</strong>',
		message : msg,
		url : link,
		target : '_blank'
	},
	{
		type : type,
		z_index : 2060,
		delay : delay,
		timer : 1000,
		placement :
		{
			from : "top",
			align : "left"
		},
		animate :
		{
			enter : 'animated fadeInDown',
			exit : 'animated fadeOutUp'
		},
		onShow : null,
		onShown : onClose,
		onClose : null,
		onClosed : null,
		mouse_over : null,
		offset : 20,
		spacing : 10,
		newest_on_top : true,
		icon_type : 'class',

	});
}

function notifyWithLink(title, msg, type, delay, link,onShown)
{
	notifyBase(title, msg, type, delay, link,onShown);
}

function notify(title, msg, type, delay)
{

	notifyBase(title, msg, type, delay, '',null);
}

function showStatus(success, msg)
{
	var strVar = "";
	if (success == true)
	{

		notify("", msg, 'success', 3000);

	} else
	{

		notify("Failed!!", msg, 'danger', 3000);

	}
}

// function AjaxPost(url, dataObj)
// {
//
// $.ajax(
// {
// url : url,
// type : 'POST',
// data : JSON.stringify(dataObj),
// success : function(data)
// {
// console.log(data)
//
// },
// error : function(jqXHR, textStatus, errorThrown)
// {
//
// },
// dataType : "json",
// contentType : "application/json; charset=utf-8",
// async : true
// });
//
// }

function ajaxSyncJsonGET(url, dataObj, success, error)
{
	$.ajax(
	{
		type : "GET",
		url : url,
		async:false,
		data : JSON.stringify(dataObj),
		success : success,
		error : error,
		contentType : "application/json; charset=utf-8",
	});
}
function ajaxJsonGET(url, dataObj, success, error)
{
	$.ajax(
	{
		type : "GET",
		url : url,
		data : JSON.stringify(dataObj),
		success : success,
		error : error,
		contentType : "application/json; charset=utf-8",
	});
}

function ajaxJsonPostForm(url, dataObj, success, error)
{
	$.ajax(
	{
		type : "POST",
		url : url,
		data : dataObj,
		success : success,
		error : error,
		// dataType : "json",
		contentType : "application/json; charset=utf-8",
	});
}
function ajaxJsonPost(url, dataObj, success, error)
{
	$.ajax(
	{
		type : "POST",
		url : url,
		data : JSON.stringify(dataObj),
		success : success,
		error : error,
		// dataType : "json",
		contentType : "application/json; charset=utf-8",
	});
}

function isset()
{
	// discuss at: http://phpjs.org/functions/isset/
	// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: FremyCompany
	// improved by: Onno Marsman
	// improved by: Rafał Kukawski
	// example 1: isset( undefined, true);
	// returns 1: false
	// example 2: isset( 'Kevin van Zonneveld' );
	// returns 2: true

	var a = arguments, l = a.length, i = 0, undef;

	if (l === 0)
	{
		throw new Error('Empty isset');
	}

	while (i !== l)
	{
		if (a[i] === undef || a[i] === null)
		{
			return false;
		}
		i++;
	}
	return true;
}

function empty(mixed_var)
{
	// discuss at: http://phpjs.org/functions/empty/
	// original by: Philippe Baumann
	// input by: Onno Marsman
	// input by: LH
	// input by: Stoyan Kyosev (http://www.svest.org/)
	// bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: Onno Marsman
	// improved by: Francesco
	// improved by: Marc Jansen
	// improved by: Rafal Kukawski
	// example 1: empty(null);
	// returns 1: true
	// example 2: empty(undefined);
	// returns 2: true
	// example 3: empty([]);
	// returns 3: true
	// example 4: empty({});
	// returns 4: true
	// example 5: empty({'aFunc' : function () { alert('humpty'); } });
	// returns 5: false

	var undef, key, i, len;
	var emptyValues = [ undef, null, false, 0, '', '0' ];

	for (i = 0, len = emptyValues.length; i < len; i++)
	{
		if (mixed_var === emptyValues[i])
		{
			return true;
		}
	}

	if (typeof mixed_var === 'object')
	{
		for (key in mixed_var)
		{
			// TODO: should we check for own properties only?
			// if (mixed_var.hasOwnProperty(key)) {
			return false;
			// }
		}
		return true;
	}

	return false;
}

function getGoogleSearchUrl(gSearchParam, extra)
{

	// gSearchParam = gSearchParam.replace(' ','+');
	gSearchParam = escape(gSearchParam);

	return 'https://www.google.com' + extra + '/search?q=' + gSearchParam;

}

function is_numeric(mixed_var)
{
	// discuss at: http://phpjs.org/functions/is_numeric/
	// original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// improved by: David
	// improved by: taith
	// bugfixed by: Tim de Koning
	// bugfixed by: WebDevHobo (http://webdevhobo.blogspot.com/)
	// bugfixed by: Brett Zamir (http://brett-zamir.me)
	// bugfixed by: Denis Chenu (http://shnoulle.net)
	// example 1: is_numeric(186.31);
	// returns 1: true
	// example 2: is_numeric('Kevin van Zonneveld');
	// returns 2: false
	// example 3: is_numeric(' +186.31e2');
	// returns 3: true
	// example 4: is_numeric('');
	// returns 4: false
	// example 5: is_numeric([]);
	// returns 5: false
	// example 6: is_numeric('1 ');
	// returns 6: false

	var whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
	return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -1)) && mixed_var !== ''
			&& !isNaN(mixed_var);
}

